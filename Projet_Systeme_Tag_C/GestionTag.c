#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include "GestionTag.h"
#include "ArborescenceTag.h"


#define SIZE 8192

int compteMot(char *chaine){ //N'accepte pas les espaces superflus

    if(strstr(chaine," ")==NULL){ return(1);}
    int comp=1;
    for(int k=0 ; k<strlen(chaine) ; k++)
    {
        if(chaine[k]==' '){comp++;}
    }
    return comp;
}


char** strParse(char *chaine,char tab[][50],int nm){ //on renvoie un pointeur vers un tableau contenant les nm mots de 'chaine'.
    //On espere que chaque mots fait moins que 50 char

    int taille=strlen(chaine);
    int deb=0;
    int n=0;
    for(int compte = 0 ;compte<taille;compte++)
    {
        if(chaine[compte]==' ')
        {
            strncpy(tab[n],chaine+deb,compte-deb); // tab les COPIES des mots de chaine
            strcat(tab[n],"\0");
            deb=compte+1;
            n++;
        }
    }
    strncpy(tab[n],chaine+deb,taille-deb);
    return(tab);
}


int str_rem(char *chaine, char *rem) // On supprime rem de la chaine de char chaine
{
  int n = 0;
  char *p;
 
  if (chaine && rem)
  {
    size_t len = strlen(rem);
 
    while((p = strstr(chaine, rem)) != NULL)
    {
      memmove(p, p + len, strlen(p + len) + 1);
      n++;
    }
  }
 
  return n;
}


char* tagInString(char* chaine, char* tag){ //Return un pointeur vers tag dans la chaine, ou null si il n'y est pas 
  int len=strlen(chaine);
  int lenTag=strlen(tag);
  char* ptr = NULL;
  for(int k=0;k<len;k++)
    {
      ptr=strstr(chaine+k,tag);
      if(ptr!=NULL && (*(ptr+lenTag)==' ' || *(ptr+lenTag)=='\0'))
	{
	  return ptr;
	}
    }
  return NULL;
}

int hasTags(char *valeur, char *tags){//On regarde si les mots de tags sont dans valeur. Renvoie un bool
  
  if(strlen(valeur)<strlen(tags)){return(0);} //Si il y a plus de bytes dans tags que dans valeur, valeur ne PEUT PAS contenir tous les tags.
  int res=1;
  int nm=compteMot(tags);
  char tab[nm][50];
  memset(tab,0,nm*50*sizeof(char)); //On initialise le tableau
  strParse(tags,tab,nm); // On chaque tab[k] contient un tag de tags
  for(int k=0; k<nm ;k++)
    {
      if(tagInString(valeur,tab[k])!=NULL) //Si on trouve tab[k] dans valeur
	{
	  res=res*1;
	}
      else //Sinon, c'est que tab[k] n'est pas dans valeur
	{
	  return(0);
	}
    }
  return(res);
}

int hasNotTags(char *valeur, char *tags){ //On verifie que les tags ne sont PAS dans valeur. Renvoie un bool
  
  int res=1;
  int nm=compteMot(tags);
  char tab[nm][50];
  memset(tab,0,nm*50*sizeof(char));
  strParse(tags,tab,nm);
  for(int k=0; k<nm ;k++)
    {
      if(tagInString(valeur,tab[k])==NULL) // Si le tag tab[k] n'est pas présent dans valeur
	{
	  res=res*1;
	}
      else //Sinon, il y est, et donc on renvoie faux
	{
	  return(0);
	}
    }
  return(res);
}

int search(char *path,char *tagPos, char *tagNeg){ //Affiche les fichiers qui ont les tags "TagPose" mais pas "TagNeg"
  // On cherche récursivement dans tous les sous répertoire de path
  
  DIR *dirActu = opendir(path);
  if(dirActu == NULL) //Si Mauvais chemin
     { 
    perror("Erreur Fichier "); 
    exit(-1);
  }
   struct dirent* dir;
   while((dir=readdir(dirActu))!=NULL) // On itère sur les éléments du répertoire
     {
       char inspect[200]; // Ces lignes pour se souvenir du chemin parcouru
       memset(inspect,0,200); // Et réussir l'appel récursif
       strcpy(inspect,path);
       strcat(inspect,"/");
       strcat(inspect,dir->d_name);

       int tValeur = getxattr(inspect,"user.tag",NULL,0);
       char valeur[tValeur+1];
       memset(valeur,0,tValeur+1);
       getxattr(inspect,"user.tag",valeur,tValeur+1);
       if(hasTags(valeur,tagPos) && hasNotTags(valeur,tagNeg)) // Si l'élément correspond à notre recherche
	 {
	   switch(dir->d_type){ // On distingue en fonction de si l'élément est un repertoire ou un fichier
	   case 4 : // DT_DIR =4 = element est repertoire. DT_DIR n'est pas reconnu par la commande make, à améliorer.
	     {
	       if((dir->d_name)[0] =='.'){} // On evite les répertoires parents, et cachés
	       else
		 {
		   search(inspect,tagPos,tagNeg);
		 }
	     }
	   default : //Notre élément n'est pas un Répertoire
	     {
	        if((dir->d_name)[0] =='.'){} // On evite les répertoires parents, et cachés
		else{
		  printf("%s \n",inspect);
		}
	     }
	   }
	 }
     }
   closedir(dirActu);
   return(0);
}

int remove_tag(char *path, char *tag){ //Supprime tag des tags de path
  
  int fd = open(path,O_RDONLY);
  if(fd <= 0){//si on trouve pas le fichier, erreur
    perror("Erreur fichier ");
    exit(-1);
  }
   int sizevalue = fgetxattr(fd,"user.tag",NULL,0);//On prend la taille actuelle de l'attribut user.tag
   if(sizevalue == -1 || sizevalue == 0){printf("Rien à enlever\n");close(fd);exit(-1);} // si il n'y pas encore de valeur pour user.tag
   char ntag[strlen(tag)+2];
   memset(ntag,0,strlen(tag)+2);
   strcat(ntag," "); // On ajoute aussi le char " " dans ce qu'il faut suppr
   strcat(ntag,tag);
   char value[sizevalue];
   memset(value,0,sizevalue);
   fgetxattr(fd,"user.tag",value,sizevalue);//On stocke la valeur dans value
   if (tagInString(value,tag)==NULL)
     {
       printf("%s n'est pas tagé par %s \n ",path,tag);
       
       close(fd);
       return(0);
     }
   str_rem(value,ntag);//on supprime tag de value 
   fsetxattr(fd,"user.tag",value,strlen(value),0);
   close(fd);
   return(0);
  
}

int init(char *path){ //Supprime tous les tags de path
  
  int fd = open(path,O_RDONLY);
  if(fd <= 0){//si on trouve pas le fichier, erreur
    perror("Erreur fichier ");
    exit(-1);
  }
  fsetxattr(fd,"user.tag","",strlen("")+1,0);
  return(0);
}

int add_tag(char *path,char *tag){ // ajoute un tag à path

  int fd = open(path,O_RDONLY);
  if(fd <= 0){//si on trouve pas le fichier, erreur
    perror("Erreur fichier ");
    exit(-1);
  }
  else{
    
    FILE* fichier=fopen("Arborescence.txt","r+");
    if(TagIsPresent(tag,fichier)==-1) // Si le mot n'est pas encore dans l'arborescence
      {
	ajouterTag(tag);//On l'ajoute
      }
    fclose(fichier);
    
    int sizevalue = fgetxattr(fd,"user.tag",NULL,0);//On prend la taille actuelle de l'attribut user.tag
    if(sizevalue == -1){sizevalue=0;} // si il n'y pas encore de valeur pour user.tag
    char value[sizevalue+1];
    fgetxattr(fd,"user.tag",value,sizevalue+1);//On stocke la valeur dans value
    if(tagInString(value,tag)!=NULL) // On ne peut pas donner deux fois le meme tag
      {
	return(0);
      }

    char nvalue[sizevalue + strlen(tag)];
    memset(nvalue,0,sizevalue + strlen(tag));
    
    if(sizevalue != 0 && sizevalue != 1) // Si il y avait déjà une valeur, alors il faut la mettre avant tag et insérer un espace
      {
	strcat(nvalue,value);
	strcat(nvalue," ");
      }
    strcat(nvalue,tag); //On concatene tag et l'ancienne valeur
    
    fsetxattr(fd,"user.tag",nvalue,strlen(nvalue)+1,0); // On met à jour les tags de l'élément
    
    close(fd);
    return(0);
    
  }

}

int add_tag2(char* path,char* tag){ //Prends en compte l'arborescence
  add_tag(path,tag);
  char* surtags= getSurTag(tag);
  if(surtags != NULL) 
    {
      int nm=compteMot(surtags);
      char tab[nm][50];
      memset(tab,0,nm*50*sizeof(char)); //On initialise le tableau
      strParse(surtags,tab,nm); //  chaque tab[k] contient un tag de surtags
      for(int k=0;k<nm;k++)
	{
	  add_tag(path,tab[k]);
	}
      return(0);
    }
  free(surtags);
  return(0);
}

int see_tag(char *path){ //On affiche les tags de  path
  
  int fd = open(path,O_RDONLY);
  if(fd <= 0){//si on trouve pas le fichier, erreur
    perror("Erreur fichier ");
    exit(-1);
  }
  else{
    int sizevalue = fgetxattr(fd,"user.tag",NULL,0); ////On prend la taille actuelle de l'attribut user.tag
    if(sizevalue==-1){//si pas de valeur pour user.tag
      printf("Tags de %s : \n",path); //On affiche quand meme le vide
      return(0);
    }
  
    char value[sizevalue+1];
    memset(value,0,sizevalue+1);
    fgetxattr(fd,"user.tag",value,sizevalue+1);
    printf("Tags de %s : %s\n",path,value);
    close(fd);
    return(0);
      }
    
}

int maj_dir(char *path){ // On s'assure que les répertoires portent les tags de leurs éléments
  DIR *dirActu = opendir(path);
  if(dirActu == NULL)
    {
      perror("Erreur a l'ouverture du Repertoire ");
      exit(-1);
    }
  init(path); // On réinitialise la valeur des tags du dir
  struct dirent* dir;
  while((dir=readdir(dirActu))!=NULL)
    //On ne regarde ni le repertoire ni son pere
    // On va comparer la valeur des tags du Dir avec celles des tags de  ses éléments
    {
      if((dir->d_name)[0] =='.')
	{
	  //On n'inspecte pas les fichiers commençant par .
	  //Donc on passe au dir suivant
	}
      else
	{
      
	  char inspect[200];
	  memset(inspect,0,200);
      
	  strcpy(inspect,path); // Ces lignes pour garder en mémoire le chemin parcouru
	  strcat(inspect,"/"); // et réussir les appels récursifs
	  strcat(inspect,dir->d_name);
	  
	  int tValDir=getxattr(path,"user.tag",NULL,0); // Taille des tags du répertoire
	  char valeurDir[tValDir+1];
	  memset(valeurDir,0,tValDir+1);
	  getxattr(path,"user.tag",valeurDir,tValDir+1);
	  int tValeurs = getxattr(inspect,"user.tag",NULL,0); // Taille des tags de inspect
       
	  if(tValeurs >1 && (dir->d_name)[0] !='.') //S'il n'y PAS qu'un espace dans les tags de inspect
	    {
	      char valeurs[tValeurs+1]; // On y stocke les tags de l'élément inspecté
	      memset(valeurs,0,tValeurs+1);
	      getxattr(inspect,"user.tag",valeurs,tValeurs+1);
	      int nm=compteMot(valeurs);
	      char tab[nm][50];
	      memset(tab,0,nm*50*sizeof(char));
	      strParse(valeurs,tab,nm);
	      for(int k=0;k<nm;k++)
		{
		  if(tagInString(valeurDir,tab[k])==NULL) //Si on ne trouve pas la valeur tab[k] dans valeurDir
		    {
		      //alors il faut la rajouter
		      add_tag(path,tab[k]);
		    }
		}
	     
	    }
	 
	  if(dir->d_type == 4 && (dir->d_name)[0] !='.') //Si nous inspections un Repertoire, qui n'est ni parent ni caché
	    {
	      //alors on met aussi à jour ses tags
	      maj_dir(inspect);
	    }
	  
	}
    }
  closedir(dirActu);
  return(0);
}

void ErrorOnArg(int argc,int seuil,char* text){ //En cas d'erreur nombre d'argument, affiche le readme a partir de text
  if(argc < seuil) {
    int fd=open("readme.md",O_RDONLY);
    printf("\nPas assez d'arguments, veuillez relire le readme : \n\n");
    if(fd<=0)
      {
	perror("Erreur readme \n");
	exit(-1);
      }
    char buff[SIZE];
    memset(buff,0,SIZE);
    while(read(fd,buff,SIZE)>0)
      {
	
	char* ptr =strstr(buff,text);
	if(strcmp(text,"## Les commandes")==0)
	  {
	    printf("%s\n\n",ptr);
	    exit(-1);
	  }
	else
	  {
	    char* fin =strstr(ptr+1,"*");
	    char out[1000];
	    memset(out,0,1000);
	    strncpy(out,ptr,fin-ptr);
	    printf("%s\n\n",out);
	    exit(-1);
	  }
      }
    exit(-1);
  }
}

