#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "GestionTag.h"
#include "ArborescenceTag.h"


int main(int argc, char *argv[]){
  maj_dir(".");
  ErrorOnArg(argc,3,"## Les commandes");// Si il n'y a que un argument après mytag, c'est que la commande est incomplète. On affiche les commandes.

   if (strcmp(argv[1],"-help")==0){ //Si on appelle à l'aide
     ErrorOnArg(0,1,"## Les commandes");//On force l'apparition des commandes
     return(0);
   }
  
  if (strcmp(argv[1],"-init")==0){ //On initialise les tags des fichiers argv[k]
    for(int k=2;k < argc; k++){
     init(argv[k]);
    }
    
    return(0);
  }


  
  if (strcmp(argv[1],"-rem")==0){ // On va supprimer les tags  argv[k] de argv[2]
    ErrorOnArg(argc,4,"* 2");
    for(int k=3;k < argc ; k++){
      remove_tag(argv[2],argv[k]);
    }
    
    return(0);
  }


  
  if (strcmp(argv[1],"-list")==0){ // On affiche les tags de argv[1]
    for(int k=2;k<argc;k++){
    see_tag(argv[k]);
    }
     
    return(0);
  }


  
  if (strcmp(argv[1],"-search")==0){ // On veut faire une recherche dans les repertoires
    switch(argc)
      {
      case(3) : // Dans le cas où on ne fournit pas de tag interdit
	{
	  search(".",argv[2],"TagCompletementImpossible"); // On fait comme si l'interdit était "TagCompl.." lol
	  return(0);
	}
      default :
	{
	  search(".",argv[2],argv[3]);
	   return(0);
	}
      }
  }

  

  if (strcmp(argv[1],"-addW")==0) // On ajoute les argv[k] mots à l'arbor
    {
      ErrorOnArg(argc,3,"* 6");
      for(int k=2;k<argc;k++)
	{
	  ajouterTag(argv[k]);
	}
      return(0);
    }


  
  if (strcmp(argv[1],"-setLk")==0) //On veut rajouter les argv[k] sousTag à argv[2]
    {
      ErrorOnArg(argc,4,"* 7");
      for(int k=3;k<argc;k++)
	{
	   ajouterSoustag(argv[2],argv[k]);
	}
      return(0);
    }


  if (strcmp(argv[1],"-setLkInv")==0) //On veut rajouter les argv[k] surTag à argv[2]
    {
      ErrorOnArg(argc,4,"* 8");
      for(int k=3;k<argc;k++)
	{
	  ajouterSurtag(argv[2],argv[k]);
	}
      return(0);
    }
  

  
  if(strcmp(argv[1],"-remLk")==0)//On veut supprimer le lien de argv[2] et argv[3]
    {
      ErrorOnArg(argc,4,"* 9");
      supprimerSurtag(argv[2],argv[3]);
    }

  if(strcmp(argv[1],"-sTags")==0) // On veut voir les SURtags
    {
      for(int k=2;k<argc;k++)
	{
	  char* buff=getSurTag(argv[k]);
	  if(buff != NULL){
	    printf("%s\n",buff);
	    free(buff);
	  }
	}
    }

  if(strcmp(argv[1],"-ssTags")==0) // On veut voir les SOUS-tags
    {
      for(int k=2;k<argc;k++)
	{
	  char* buff=getSousTag(argv[k]);
	  if(buff != NULL){
	    printf("%s\n",buff);
	    free(buff);
	  }
	}
    }

  if(strcmp(argv[1],"-supprMot")==0) // On veut suppr des mots de l'arbore.
    {
      for(int k=2;k<argc;k++)
	{
	  supprimerTag(argv[k]);
	}
    }

  
  else{ // On ajoute les tags argv[k] sur le fichier argv[1]
    for(int k=2;k < argc; k++)
      { 
	add_tag2(argv[1],argv[k]);
      }
    return(0);
  }
  
}
