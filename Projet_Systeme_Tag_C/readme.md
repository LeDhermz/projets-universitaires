PROJET DE PROGRAMMATION SYSTEME 2020
============================================

**MyTag : Un gestionnaire de tags**

Vous lisez le readme relatif au programme de gestionnaire de tag programmé par nos soins.
Vous y trouverez dans une première partie la méthode suivie pour le réaliser,
dans une seconde la méthode d'installation, et enfin les commandes implémentées.
Le repertoire doit comprendre les fichiers :

* Presentation.md
* AUTHORS.txt
* Arborescence.txt
* readme.md
* ARCHITECTURE.md
* main.c
* GestionTag.c et headers
* Arborescence.c et headers
* Makefile
* install.sh
* uninstall.sh

En quelques mots, MyTag implémente le projet avec des Tags LOCAUX, qui utlisent les Extendeds Attributs, et dont la hiérachie
est construite et entretenue dans un fichier.txt. Une particularité du projet est de permettre le tag des répertoires, pour faciliter
la recherche de tag.
De  plus, un petit script procède à une 'installation' facilite l'utilisation du programme.


## Méthode suivie

Pour ajouter un tag à un élément du systeme de fichier, nous avons décidé
d'utiliser les "Extended Attributs", dits xattr, présents sur la majorité des distributions linux.
Un même fichier peut avoir plusieurs xattr, de la forme "name:value".
Pour tager un fichier, il suffit donc de créer un attribut "user.tag",
puis d'y stocker dans un string les différents tags du fichier.
Le préfixe 'user' stipule que l'attribut sera accessible seulement pour l'utilisateur,
car nous pensions que c'était l'usage plus pratique. A noter qu'un autre préfixe aurait eu
pour effet de modifier la portée de l'attribut.
Tous les éléments d'un répertoire peuvent porter des xattr, en particulier d'autres répertoires.
Ainsi, notre gestionnaire laisse la possibilité de tager des répertoires, et transmet
aux répertoires les tags des fichiers qu'ils contiennent.
Les xattr étant modifiables à loisir, il est tout à fait possible de rajouter un tag,
d'en supprimer, et il est possible de les comparer.
Les xattr étant  directement liés aus fichiers, ils survivent à la majorité des commandes linux,
parfois en utlisant des options de commande.
En particulier, dans le cas d'un 'cp', il faut activer l'option preserve xattr,
nous conseillons donc à l'utlisateur de mettre un alias sur cp pour le faire à chaque fois.
D'ailleurs, l'intallation propose à l'utilisateur de rentrer -ou non- cet alias dans le bashrc.

Avec mytag, le char ' ' signe la fin du tag. Ainsi, le tag "bleu ciel" devra être écrit
"bleu-ciel" pour être compris comme un seul tag, et non pas comme la concaténation de bleu et de ciel.

Plus de détails sont fournis dans le fichier ARCHITECTURE.md.


## L'installation

Il suffit de donner les droits d'execution au install.sh, avec la commande :

$ chmod +x install.sh

Puis, executer le script avec la commande :

$ ./install.sh

Le script compilera main.c, créera un alias vers l'executable main  de telle sorte
qu'il suffise de rentrer la commande 'mytag' pour executer main dans le repertoire courant.
Enfin le script vous demandera si vous souhaitez créér un alias cp='cp --preserve=xattr',
de manière à conserver les tags des fichiers que vous copiez. Il le créera le cas échéant.
Il est normal de voir quelques warnings s'afficher, nous n'avons malheureusement pas
réussi à la supprimer sans tricher.(en enlevant -Wall à gcc par exemple)

Pour **désinstaller**, donner les droits d'excution avec la commande :

$ chmod +x uninstall.sh

Puis, executer le script avec la commande :

$ ./uninstall.sh

## Les commandes


* 1 Tager un fichier : mytag fichier tag1 tag2 tag3

* 2 Effacer un (ou plusieurs) tag d'un fichier : mytag -rem fichier tag1 tag2 ..

* 3 Effacer TOUS les tags d'un (ou plusieurs) fichier : mytag -init fichier1 fichier2 ..

* 4 Lister les tags d'un (ou plusieurs) fichier : mytag -list fichier1 fichier 2 ..

* 5 Lister les fichiers ayant les "tag1 tag2 .." mais les pas "tag3 tag4 .." : mytag -search "tag1 tag2 .." "tag3 tag4 .."
  Le dernier argument est optionnel.

* 6 Ajouter un (ou plusieurs) tag dans l'arborescence : mytag -addW tag1 tag2 ..

* 7 Créer un lien entre un tag et des sous-tags : mytag -setLk tag sstag1 sstag2 ..

* 8 Créer un lien entre un tag et des sur-tags : mytag -setLkInv tag srtag1 srtag2..

* 9 Supprimer un lien entre deux tags : mytag -remLk tag1 tag2

* 10 Voir les SUR-tags d'un (ou plusieurs) tag : mytag -sTags tag1 tag2 ..

* 11 Voir les SOUS-tags d'un (ou plusieurs) tag : mytag -ssTags tag1 tag2 ..

* 12 Supprimer un (ou plusieurs) mot de l'arborescence : mytag -supprMot tag1 tag2 ..

* 13 Faire apparaître cette liste de commande : mytag -help
