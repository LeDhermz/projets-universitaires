#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <ctype.h>
#include "ArborescenceTag.h"

#define TAILLE_MAX 10000


int max(int a,int b){// Renvoie le max entre A et B
    if(a>=b) {
        return a;
    }
    else {
        return b;
    }
}

char* MytagInString( char* chaine, char* tag){ //Return un pointeur vers tag dans la chaine, ou null si il n'y est pas
    int len=strlen(chaine);
    int lenTag=strlen(tag);
    char* ptr = NULL;
    for(int k=0;k<len;k++)
    {
        ptr=strstr(chaine+k,tag);
        if(ptr!=NULL && (*(ptr+lenTag)==' ' || *(ptr+lenTag)=='\0' || *(ptr+lenTag)=='\n'))// On vérifie que c'est un mot et non pas une partie d'un mot
        {
            return ptr; // On return le pointer vers Tag
        }
    }
    return NULL; // Sinon on return NULL
}

void SuppTaginStr(char *chaine, char  *tag){ // Permet de Supprimer Tag dans Chaine si celui ci appartient a Chaine
    char *p=MytagInString(chaine,tag);
    if(p==NULL){ // On s'assure que Le tag appartient a la chaine
        return ;
    }
    memmove(p,p+ strlen(tag),strlen(p + strlen(tag)) + 1); // On déplace les octets en écrasant ceux correspondant a Tag
    return;
}


int Next_Space(char *chaine,int pos){// Fonction qui renvoie la position du prochain espace dans Chaine
    if(isblank(chaine[pos])){
        return -1;
    }
    while(chaine[pos]!='\0'){ // On parcourt la chaine
        if(isspace(chaine[pos])){ // si c'est un espace on renvoie sa position
            return pos;
        }
        pos++;
    }
    return strlen(chaine); // Sinon on retourne la longueur de la Chaine
}

int TagIsPresent(char *tag,FILE* fichier){// Cherche si un Tag est présent dans l'arborescence
    // Renvoie 1 en cas de succès et -1 en cas d'echec
    fseek(fichier, 0, SEEK_SET); // On se positionne au début du fichier
    char chaine[TAILLE_MAX];
    while (fgets(chaine,TAILLE_MAX, fichier)!= NULL){// On parcourt le fichier ligne par ligne
        int pos=Next_Space(chaine,0);
        if(memcmp(chaine,tag,pos)==0 && pos==strlen(tag) && strlen(tag)!=0 ){ //On regarde si le Tag appartient au Fichier
            return 1; // Si oui on return 1
        }

    }
    return -1; // Sinon on return -1
}


int ajouterTag(char *tag){// Ajoute un Tag dans l'arborescence
    // Renvoie 1 en cas de succès et -1 en cas d'echec
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){ // On vérifie que le fichier a bien était ouvert
        return -1;
    }
    if(TagIsPresent(tag,fichier)==1){ // Si le tag est deja présent pas besoin de l'ajouter
        printf("Le tag est deja present\n");
        fclose(fichier);
        return -1;
    }
    fseek(fichier, 0, SEEK_END); // On se positionne a la fin  du fichier pour ajouter le Tag
    if (ftell(fichier)==0){
        fprintf(fichier,"%s : /",tag); // On regarde si on est au début du fichier,on écrit sur la premiere ligne
        fclose(fichier);
        return 1;
    }
    fprintf(fichier,"\n%s : /",tag);// Sinon on écrit sur la ligne suivante
    fclose(fichier);
    return 1;
}



int supprimerTag(char *tag){// Supprime un Tag dans l'arborescence
    // Renvoie 1 en cas de succès et -1 en cas d'echec
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return -1;
    }
    if(TagIsPresent(tag,fichier)==-1){// Pour supprimer des elements dans un fichier on copie les elements que l'on shouaite garder
        printf("Le tag n'est pas présent on ne peut le supprimer\n");// Dans un fichier que l'on crée
        fclose(fichier);// Puis on remove le fichier original
        return -1;// Et on rename la copie
    }
    FILE* fichier2;
    fichier2 = fopen("Arborescence2.txt", "a+");
    if(fichier==NULL){
        return -1;
    }
    fseek(fichier, 0, SEEK_SET);
    char chaine[TAILLE_MAX];
    while (fgets(chaine,TAILLE_MAX, fichier)!= NULL){
        int pos=Next_Space(chaine,0);
        if ((memcmp(chaine, tag, pos) == 0 && pos == strlen(tag) && strlen(tag) != 0) || pos == 0) {// Si on est sur la ligne du tag a supprimer on ne l'ajoute pas

        }
        else{
            SuppTaginStr(chaine,tag); // Sinon on ajoute la ligne en faisant attention a bien supprimer les éventuelles sous tag et sur tag
            fprintf(fichier2, "%s", chaine);
        }

    }

    remove("Arborescence.txt");// On supprime l'ancien fichier
    rename("Arborescence2.txt", "Arborescence.txt"); // On rename le nouveau fichier
    fclose(fichier2);
    return 1;
}


char* stringBetween2Char( char *chaine,char *first,char *last) { // Renvoie un pointeur vers un string contenant les éléments entre deux charactére d'une chaine
    char *i1 = strstr(chaine, first); // Renvoie NULL si les caractéres n'appartiennent pas a la chaine
    if (i1 != NULL) { // On cherche le premier caractère
        int len_first = strlen(first);
        char *i2 = strstr(i1 + len_first, last); // On cherche le second caractère
        if (last != NULL) {
            int len = i2 - (i1 + len_first);
            char *rep=malloc(len+1); // On alloue de la mémoire pour la nouvelle chaine
            if (rep != NULL) { // Le malloc sera free dans la fonctions isSurtag ou isSoustag
                memcpy(rep, i1 + len_first, len); // On recopie la partie entre les deux caractère dans le pointeur
                rep[len] = '\0';
                return rep;
            }
        }
    }
    return NULL;
}


char* getSurTag(char *tag){// Renvoie les sur tags de un  Tag dans l'arborescence. Le pointeur renvoyé, issu de malloc, devra être free ulterieurement
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return NULL;
    }
    if(TagIsPresent(tag,fichier)==-1){ // Si le tag n'est pas présent on ne peut renvoyer ses surtag
        printf("Le tag n'est pas présent on ne peut renvoyer ses surTags\n");
        return NULL;
    }
    fseek(fichier, 0, SEEK_SET); // On se positionne au début du fichier
    char chaine[TAILLE_MAX];
    while (fgets(chaine,TAILLE_MAX, fichier)!= NULL) {
        int pos = Next_Space(chaine, 0);
        if(memcmp(chaine,tag,pos)==0 && pos==strlen(tag) && strlen(tag)!=0 ){
            char *ptr; // Sera free dans isSurtag
            ptr=stringBetween2Char(chaine,":","/"); // On récupere les surtag
            if(ptr==NULL || strlen(ptr)==1){ // Si il existe on les renvoient sinon on renvoie NULL
                return NULL;
            }
            return ptr;
        }
    }
    return NULL;
}

char *getSousTag(char *tag){// Renvoie les sous tags de un  Tag dans l'arborescence  Le pointeur renvoyé, issu de malloc, devra être free ulterieurement
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return NULL;
    }
    if(TagIsPresent(tag,fichier)==-1){// Si le tag n'est pas présent on ne peut renvoyer ses soustag
        printf("Le tag n'est pas présent on ne peut renvoyer ses sousTags\n");
        return NULL;
    }

    fseek(fichier, 0, SEEK_SET); // On se positionne au début du fichier
    char chaine[TAILLE_MAX];
    while (fgets(chaine,TAILLE_MAX, fichier)!= NULL) {
        int pos = Next_Space(chaine, 0);
        if(memcmp(chaine,tag,pos)==0 && pos==strlen(tag) && strlen(tag)!=0 ){
            char *ptr; // Sera free dans isSoustag
            if(chaine[strlen(chaine)-1]!='\n'){ // On vérfie si c'est la dernière ligne du fichier ou non
                ptr = stringBetween2Char(chaine, "/", "\0"); // On récupere les soustag
            }
            else{
                ptr = stringBetween2Char(chaine, "/", "\n"); // On récupere les soustag
            }
            if(ptr==NULL ){  // Si il existe on les renvoient sinon on renvoie NULL
                return NULL;
            }
            return ptr;
        }
    }

    return NULL;
}




int isSurtag(char * tag,char *surtag){
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return -1;
    }
    if(TagIsPresent(tag,fichier)==-1){ // On vérifie que le tag est présent dans l'arbo
        printf("Le tag %s n'est pas présent dans l'arboresence\n",tag);
        return -1;
    }

    if(TagIsPresent(surtag,fichier)==-1){ // On vérifie que le surtag est présent dans l'arbo
        printf("Le tag %s n'est pas présent dans l'arboresence\n",surtag);
        return -1;
    }
    int len=max(strlen(tag),strlen(surtag)); // Pour ne pas lire dans des espace de mémoire que l'on ne connait pas
    if(memcmp(tag,surtag,len)==0 && strlen(tag)==strlen(surtag)){
        printf("Les deux tags sont égaux,ils ne sont pas donc surtag et tag\n"); // On vérifie que les deux arguments sont différents
        return -1;
    }

    char *ptr=getSurTag(tag);// On recupere les surtags de tag
    if(ptr==NULL){
        return -1;
    }
    char *res;
    res=MytagInString(ptr,surtag); // On regarde si surtag  appartient au surtag de tag
    if(res!=NULL){
        free(ptr); // tres important de free le malloc
        return 1;
    }
    free(ptr); // tres important de free le malloc
    return -1;
}


int isSoustag(char * tag,char *soustag){
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return -1;
    }
    if(TagIsPresent(tag,fichier)==-1){ // On vérifie que le tag est présent dans l'arbo
        printf("Le tag %s n'est pas présent dans l'arboresence\n",tag);
        return -1;
    }

    if(TagIsPresent(soustag,fichier)==-1){// On vérifie que le soustag est présent dans l'arbo
        printf("Le tag %s n'est pas présent dans l'arboresence\n",soustag);
        return -1;
    }
    int len=max(strlen(tag),strlen(soustag));// Pour ne pas lire dans des espace de mémoire que l'on ne connait pas
    if(memcmp(tag,soustag,len) ==0 && strlen(tag)==strlen(soustag)){
        printf("Les deux tags sont égaux,ils ne sont pas donc soustag et tag\n"); // On vérifie que les deux arguments sont différents
        return -1;
    }
    char *ptr=getSousTag(tag); // On recupere les soustags de tag
    if(ptr==NULL){
        return -1;
    }
    char *res;
    res=MytagInString(ptr,soustag);// On regarde si soustag  appartient au soustag de tag
    if(res!=NULL){
        free(ptr); // tres important de free le malloc
        return 1;
    }
    free(ptr);  // tres important de free le malloc
    return -1;
}


int appSurtag(char *tag,char *surtag){// Ajoute un SurTag dans le fichier (pas dans l'Arbo)
    // Renvoie 1 en cas de succès et -1 en cas d'echec
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return -1;
    }
    if(TagIsPresent(tag,fichier)==-1){ // On regarde si tag est dans l'Arbo
        printf("Le tag %s n'est pas présent dans l'arboresence on l'ajoute donc\n",tag);
        ajouterTag(tag);

    }
    if(TagIsPresent(surtag,fichier)==-1){// On regarde si surtag est dans l'Arbo
        printf("Le tag %s n'est pas présent dans l'arboresence on l'ajoute donc\n",surtag);
        ajouterTag(surtag);
    }

    if(isSurtag(tag,surtag)==1){ // On vérifie que la relation n'existe pas deja
        return -1;
    }

    if(isSurtag(surtag,tag)==1){ // On vérifie que la relation inverse n'existe pas
        return -1;
    }


    FILE* fichier2;
    fichier2 = fopen("Arborescence2.txt", "a+");// On crée un nouveau fichier pour y ajouter le surtag
    if(fichier==NULL){
        return -1;
    }
    fseek(fichier, 0, SEEK_SET);// On se positionne au début du fichier
    char chaine[TAILLE_MAX];
    while (fgets(chaine,TAILLE_MAX, fichier)!= NULL) {
        int pos = Next_Space(chaine, 0);
        if ((memcmp(chaine, tag, pos) == 0 && pos == strlen(tag) && strlen(tag) != 0) || pos == 0) { // On regarde si on est sur la ligne de tag
            char old[strlen(chaine)];
            strcpy(old,chaine);
            chaine[pos+2]='\0'; // On manipule Pour pouvoir écrire entre : et / dans les surtags de tag
            strcat(chaine," ");
            strcat(chaine,surtag);
            strcat(chaine," ");
            strcat(chaine,old+pos+3);
            fprintf(fichier2, "%s", chaine);
        } else {
            fprintf(fichier2, "%s", chaine); // Sinon on copie normalement la ligne dans le nouveau fichier
        }

    }
    remove("Arborescence.txt");
    rename("Arborescence2.txt", "Arborescence.txt"); // Puis on rename pour recup notre fichier update.
    fclose(fichier2);
    return 1;
}



int appSoustag(char *tag,char *soustag){// Ajoute un SousTag dans le fichier (pas dans l'Arbo)
    // Renvoie 1 en cas de succès et -1 en cas d'echec
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return -1;
    }
    if(TagIsPresent(tag,fichier)==-1){ // On regarde si tag est dans l'Arbo
        printf("Le tag %s n'est pas présent dans l'arboresence on l'ajoute donc\n",tag);
        ajouterTag(tag);

    }
    if(TagIsPresent(soustag,fichier)==-1){ // On regarde si soustag est dans l'Arbo
        printf("Le tag %s n'est pas présent dans l'arboresence on l'ajoute donc\n",soustag);
        ajouterTag(soustag);
    }
    if(isSoustag(tag,soustag)==1){ // On vérifie que la relation n'existe pas deja
        return -1;
    }
    if(isSoustag(soustag,tag)==1){ // On vérifie que la relation inverse n'existe pas
        return -1;
    }
    FILE* fichier2;
    fichier2 = fopen("Arborescence2.txt", "a+");// On crée un nouveau fichier pour y ajouter le soustag
    if(fichier==NULL){
        return -1;
    }
    fseek(fichier, 0, SEEK_SET); // On se positionne au début du fichier
    char chaine[TAILLE_MAX];
    while (fgets(chaine,TAILLE_MAX, fichier)!= NULL) {
        int pos = Next_Space(chaine, 0);
        if ((memcmp(chaine, tag, pos) == 0 && pos == strlen(tag) && strlen(tag) != 0) || pos == 0) { // On regarde si on est sur la ligne de tag
            if(chaine[strlen(chaine)-1]=='\n'){ // On vérifie si on est dans la dernière ligne du fichier
                chaine[strlen(chaine)-1]='\0';
            }
            strcat(chaine," ");
            strcat(chaine ,soustag);
            strcat(chaine,"\n"); // On manipule Pour pouvoir écrire entre / et la fin de la ligne (dans les soustags de tag)
            fprintf(fichier2, "%s", chaine);
        } else {
            fprintf(fichier2, "%s", chaine);// Sinon on copie normalement la ligne dans le nouveau fichier
        }

    }
    remove("Arborescence.txt");
    rename("Arborescence2.txt", "Arborescence.txt"); // Puis on rename pour recup notre fichier update.
    fclose(fichier2);
    return 1;
}

void appSurtag2(char * tag,char *surtag){ // Ajoute  le surtag dans le fichier de façon asynchrone (des deux sens de la relation)
    appSurtag(tag,surtag);
    appSoustag(surtag,tag);
}

void ajouterSurtag(char *tag,char * surtag){ // Ajoute dans l'arborescence un Surtag en utilisant la récursion
    int len=max(strlen(tag),strlen(surtag));// Pour ne pas manipuler la mémoire dont on a pas accès
    if(memcmp(tag,surtag,len)==0 && strlen(tag)==strlen(surtag)){ // On vérifie que le tag et le surtag ne soient pas égaux
        return;
    }
    appSurtag2(tag,surtag); // On ajoute le surtag
    char *p;
    p=getSurTag(surtag);// on récupere les surtag
    if(p==NULL){
        return;
    }
    const char delimiters[] = " ";
    char *token = strtok(p,delimiters);// On découpe les surtag par mot
    ajouterSurtag(tag,token);
    while (token != NULL)
    {
        ajouterSurtag(tag,token);// Puis on appel la fonction sur ses surtag pour pouvoir ajouter de facon correcte dans l'Arbo
        token = strtok(NULL, delimiters);
    }
    free(p);
    return ;
}

void ajouterSoustag(char *tag,char *soustag){ // Ajoute  le soustag dans le fichier et l'arbo  de façon asynchrone (des deux sens de la relation)
    appSoustag(tag,soustag);
    ajouterSurtag(soustag,tag);
}

int supprimerSurtag(char *tag,char *surtag){// Supprime un SurTag dans l'arborescence
  // Renvoie 1 en cas de succès et -1 en cas d'echec
  if(isSurtag(tag,surtag)!=1){ // On vérifie que l'élément a supprimer existe
    printf("Impossible de supprimer car cette relation n'existe pas\n");
  }
  FILE* fichier = NULL;
  fichier = fopen("Arborescence.txt", "r+");
  if(fichier==NULL){
    return -1;
  }
  if(TagIsPresent(tag,fichier)==-1)
    {
      printf("Le tag n'est pas présent on ne peut le supprimer\n");
      fclose(fichier);
      return -1;// Et on rename la copie
    }
  FILE* fichier2;// Pour supprimer des elements dans un fichier on copie les elements que l'on shouaite garder
  fichier2 = fopen("Arborescence2.txt", "a+");// Dans un fichier que l'on crée
  if(fichier==NULL){// Puis on remove le fichier original
    return -1; // Et on rename la copie
  }
  fseek(fichier, 0, SEEK_SET);// On se positionne au début du fichier
  char chaine[TAILLE_MAX];
  memset(chaine,0,TAILLE_MAX);
  while (fgets(chaine,TAILLE_MAX, fichier)!= NULL){
    int pos = Next_Space(chaine, 0);
    if ((memcmp(chaine, tag, pos) == 0 && pos == strlen(tag) && strlen(tag) != 0) || pos == 0) {
        char newchaine[strlen(surtag)+1];
         memset(newchaine,0,strlen(surtag)+1);
         strcpy(newchaine," ");
         strcat(newchaine,surtag);
         SuppTaginStr(chaine,newchaine);// On suprime le surtag sur la ligne de tag
        fprintf(fichier2,"%s",chaine);//Puis on écrit la ligne modifié dans le nouveau fichier
    }
    else{
      fprintf(fichier2,"%s",chaine);// Sinon on copie la ligne normalement
    }
  }

  remove("Arborescence.txt");
  rename("Arborescence2.txt", "Arborescence.txt");
  fclose(fichier2);
  return 1;

}


int supprimerSoustag(char *tag,char *soustag){// Supprime un SousTag dans l'arborescence
    // Renvoie 1 en cas de succès et -1 en cas d'echec
    if(isSoustag(tag,soustag)!=1){
        printf("Impossible de supprimer car cette relation n'existe pas\n");
        return -1;
    }
    FILE* fichier = NULL;
    fichier = fopen("Arborescence.txt", "r+");
    if(fichier==NULL){
        return -1;
    }
    if(TagIsPresent(tag,fichier)==-1){// On vérifie que l'élément a supprimer existe
        printf("Le tag n'est pas présent on ne peut le supprimer\n");
        fclose(fichier);
        return -1;
    }
    FILE* fichier2;// Pour supprimer des elements dans un fichier on copie les elements que l'on shouaite garder
    fichier2 = fopen("Arborescence2.txt", "a+");// Dans un fichier que l'on crée
    if(fichier==NULL){// Puis on remove le fichier original
        return -1;// Et on rename la copie
    }
    fseek(fichier, 0, SEEK_SET);
    char chaine[TAILLE_MAX];
    while (fgets(chaine,TAILLE_MAX, fichier)!= NULL){
        int pos = Next_Space(chaine, 0);
        if ((memcmp(chaine, tag, pos) == 0 && pos == strlen(tag) && strlen(tag) != 0) || pos == 0) {
            char newchaine[strlen(soustag)+1];
            memset(newchaine,0,strlen(soustag)+1);
            strcpy(newchaine," ");
            strcat(newchaine,soustag);
            SuppTaginStr(chaine,newchaine);// On suprime le soustag sur la ligne de tag
            fprintf(fichier2,"%s",chaine);//Puis on écrit la ligne modifié dans le nouveau fichier

        }
        else{
            fprintf(fichier2,"%s",chaine);// Sinon on copie la ligne normalement
        }
    }
    remove("Arborescence.txt");
    rename("Arborescence2.txt", "Arborescence.txt");
    fclose(fichier2);
    return 1;
}

