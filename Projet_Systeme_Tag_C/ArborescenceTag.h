int max(int a,int b); // Renvoie le max entre deux nombres
char* MytagInString(char* chaine, char* tag); //Return un pointeur vers tag dans la chaine, ou null si il n'y est pas
void SuppTaginStr(char *chaine, char  *tag);// Supprime le string tag dans le string chaine si il existe
int Next_Space(char *chaine,int pos);// Fonction qui renvoie le prochain espace
int TagIsPresent(char *tag,FILE* fichier);// Cherche si un Tag est présent dans l'arborescence
// Renvoie 1 si le tag est dans l'arbo et -1 sinon
int ajouterTag(char *tag);// Ajoute un Tag dans l'arborescence
// Renvoie 1 en cas de succès et -1 en cas d'echec
int supprimerTag(char *tag);// Supprime un Tag dans l'arborescence
// Renvoie 1 en cas de succès et -1 en cas d'echec
char * stringBetween2Char( char *chaine,char *first,char *last);// Renvoie un pointeur vers la partie de chaine qui se trouve en first et last, renvoie null sinon
char * getSurTag(char *tag);// Renvoie les sur tags de un  Tag dans l'arborescence (par un pointeur) , renvoie null sinon
char *getSousTag(char *tag);// Renvoie les sous tags de un  Tag dans l'arborescence (par un pointeur) , renvoie null sinon
int isSurtag(char * tag,char *surtag); // Renvoie 1 si surtag est un surtag de tag,sinon renvoie -1
int isSoustag(char * tag,char *soustag);// Renvoie 1 si soustag est un soustag de tag,sinon renvoie -1
int appSurtag(char *tag,char *surtag);// Ajoute un SurTag dans le fichier
int appSoustag(char *tag,char *soustag);// Ajoute un SousTag dans le fichier
void appSurtag2(char * tag,char *surtag); // Ajoute un surtag dans le fichier de facon asynchrone
void ajouterSurtag(char *tag,char *surtag);// Ajoute un SurTag dans l'arborescence.
void ajouterSoustag(char *tag,char *soustag);// Ajoute un SousTag dans l'arborescence.
int supprimerSurtag(char *tag,char *surtag);// Supprime un SurTag dans l'arborescence. Renvoie 1 en cas de succès et -1 en cas d'echec
int supprimerSoustag(char *tag,char *soustag);// Supprime un SousTag dans l'arborescence. Renvoie 1 en cas de succès et -1 en cas d'echec