ARCHITECTURE DE MYTAG
============================

**Comment MyTag a-t-il été conçu ?**

Le projet s'articule autour de 3 fichiers .c :

- Le main.c qui traite les demandes de l'utilisateur, en faisant appel aux deux autres .c

- Le GestionTag.c  (GT.c) qui s'occupe de l'entretien des xattr dans le SGF, et de leur recherche.

- Le Arborescence.c (A.c) qui gère la hiérarchie des tags, à travers la mise à jour d'un fichier
  arborescence.txt.

## Le main.c

Le main analyse la commande de l'utilisateur, traduit les commandes qu'il rentre en appels
de fonctions de GT.c ou A.c, et renvoie vers une portion adéquate du readme en cas de
commande incohérente. Ces portions sont balisées dans le readme par '* k' avec k le
'numéro' de la commande. 

## Le GestionTag.c

Ce fichier concentre l'essentiel de la reflexion fournie pour ce projet : c'est ici que sont
programmées les fonctions permettant d'ajouter, ou de supprimer un tag à un fichier, de lister
les tags d'un fichier ou du répertoire courant, de faire une recherche récursive d'une combinaison
de tags à partir du répertoire courant, et de mettre à jour les tags des répertoires.
Ce fichiers a été pensé pour être le plus indépendant possible de A.c, aussi il ne fait appel à
ses fonctions que lorsqu'on add un tag à un fichier. En effet, la fonction add_tag(fichier,tag)
s'occupe d'attribuer tag à fichier, et de rajouter tag dans l'arborescence, s'il n'y est pas encore.
add_tag() est ensuite appelée par la fonction add_tag2(fichier, tag), qui se charge d'inspecter les
surtags de de tag, et de les ajouter à fichier si besoin. A elles seules, ces fonctions suffisent à
assurer que tous les tags entrés par l'utilisateur apparaissent dans l'arborescence, et que ces tags
sont accompagnés de leurs sur-tags, le cas échéant.
Une autre partie intéressante de ce fichier se trouve dans l'implémentation de l'algorithme de recherche.
Pour minimiser le nombre de répertoire à fouiller, nous avons décidé qu'ils porteraient les tags de leurs
éléments, et ceux des répertoires de leurs éléments. Ainsi, en lançant une recherche à la racine, il suffit
 d'inspecter les repertoires qui portent la combinaison de tag recherchée, et de n'ouvrir ainsi que les
répertoires qui sont susceptibles de contenir un fichier qui porte la combinaison voulue.
Attention, il est possible qu'un répertoire soit porteur de la combinaison, mais que ce ne soit le cas d'aucun
de ses fichiers. Néanmoins, ce répertoire sera retourné à l'utilisateur, car on le suppose tout de même
susceptible d'être interessant.
Une telle implémentation doit se faire en parrallèle d'une mise à jour régulière des tags des répertoires,
et il faut a contrario que cette maj inspecte TOUS les répertoires, de manière à ne pas manquer un fichier
qui aurait été déplacé à un autre bout du disque, par exemple, ou à ne pas garder des tags qui n'existent plus.
Cette mise à jour est faite par la fonction maj_dir(), qui est systèmatiquement la première appelée dans le main.
Enfin, la fonction ErrorOnArg(), appelée uniquement dans le main,  permet de généraliser  la gestion des erreurs
de saisie de commandes, en affichant une sous partie adéquates du readme.

## Le ArborescenceTag.c

Arborescence.c permet de tenir à jour le fichier Arborescence.txt, qui liste tous les tags utlisés par l'utilisateur,
et permet aussi de tenir leur hiérachie. Ce systeme, simple, nous a semblé le plus efficace pour répondre à notre
problèmatique.Le fichier Arborescence.txt est divisé par ligne , en effet chaque ligne correspond à un tag.Chaque
ligne est divisée en 3 parties qui sont les suivantes : le tag,ses surtags et ses soustags.La partie du tag correspond 
au premier mot de la ligne , la partie des surtags correspond aux mots compris entre le caractère ':' et '/' et la
dernière partie des soustags correspond aux mots compris entre le caractère '/' et la fin de la ligne.
Pour pouvoir ajouter un tag dans la hiérachie maintenue par Arborescence.txt, on appelle la fonction ajouterTag(tag)
qui ajoute ce tag si il n'est pas déjà présent dans le fichier . Pour supprimer un tag complètement de l'Arborescence , on appelle
la fonction  supprimerTag(tag) qui supprime la ligne de tag et les éventuelles relations de ce tag (soustag ou surtag).
Les fonctions getSurtag(tag) et getSoustag(tag) renvoient un pointeur vers les mots compris entre  ':' et '/' pour getSurtag
(donc les surtags du tag) et '/' et la fin de la ligne pour getSoustag (donc les soustags du tag).
Les fonctions isSurtag(tag,surtag) et isSoustag(tag,soustag) renvoient 1 si les relations existent dans l'Arborescence
et -1 dans le cas contraire.
Les fonctions ajouterSurtag(tag,surtag) et ajouterSoustag(tag,soustag) ajoutent les relations dans l'Arborescence.Elles ajoutent 
les relations en profondeur dans l'Arborescence, c'est à dire que l'on conserve la hiérachie déjà existante. Par exemple en ajoutant une
relation de surtag entre tag1 et tag2, le tag2 héritera aussi des surtags du tag1.
Les deux dernières fonctions  supprimerSurtag(tag,surtag) et supprimerSoustag(tag,surtag) permettent de supprimer une relation entre
deux tags si celle-ci existe.Contrairement à supprimerTag(tag), ces fonctions suppriment seulement la relation demandé.