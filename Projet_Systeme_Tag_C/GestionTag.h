int compteMot(char *chaine); //N'accepte pas les espaces superflus
char** strParse(char *chaine,char tab[][50],int nm); //on renvoie un pointeur vers un tableau contenant les nm mots de 'chaine'.
int str_rem(char *s, char *rem);//Fct trouvee sur developpez.net
int compteMot(char *chaine); //N'accepte pas les espaces superflus
char* tagInString(char* chaine, char* tag); //Return un pointeur vers tag dans la chaine, ou null si il n'y est pas
char** strParse(char *chaine,char tab[][50],int nm); //on renvoie un pointeur vers un tableau contenant les nm mots de 'chaine'. //On espere que chaque mots fait moins que 50 char
int hasTags(char *valeur, char *tags);//On regarde si les mots de tags sont dans valeur. Renvoie un bool
int hasNotTags(char *valeur, char *tags); //On verifie que les tags ne sont PAS dans valeur. Renvoie un bool
int search(char *path,char *tagPos, char *tagNeg); //Affiche les fichiers qui ont les tags "TagPose" mais pas "TagNeg"
  // On cherche récursivement dans tous les sous répertoire de path
int remove_tag(char *path, char *tag); //Supprime tag des tags de path
int add_tag(char *path,char *tag); // ajoute un tag à path
int see_tag(char *path); //On affiche les tags de  path
int maj_dir(char *path); // On s'assure que les répertoires portent les tags de leurs éléments
int add_tag2(char* path,char* tag); //Ajoute un tag et ses surtags
void ErrorOnArg(int argc,int seuil,char* text); //En cas d'erreur nombre d'argument, affiche le readme a partir de text
int init(char *path); //Supprime tous les tags de path
