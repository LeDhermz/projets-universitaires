#!/bin/bash

make
echo "L'executable sera appelé avec la commande 'mytag' "
var=`realpath main`
var2="alias mytag='$var'"
echo $var2 >> ~/.bashrc
echo -e "\n"
echo -e "Souhaitez vous creer un alias cp = cp --preserve xattr,\nde manière a faire suivre vos tags dans vos copies ? [Y/n]"
read rep2

if [ $rep2 = "Y" ] || [ $rep2 = "y" ] || [ -z $rep2 ]
then
    echo "alias cp='cp --preserve=xattr'" >> ~/.bashrc
    echo -e "L'alias a été créé"
else
    echo -e "Nous n'avons pas créé d'alias"
fi
source ~/.bashrc
echo "L'installation est terminée"

