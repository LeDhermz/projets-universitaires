
public class Chemin extends Saboteur{

	public Chemin(int gauche, int droite, int haut, int bas) {
		super(gauche, droite, haut, bas);
		// TODO Auto-generated constructor stub
	}
	public Chemin() {
		
	}
	//           _____
	//          |     |
	//gauche    |     |  droite
	//          |_____|
	public boolean estCompatible(Chemin test) {
		if (gauche==test.gauche || droite==test.gauche || gauche==test.droite || droite==test.droite || couleurgauche==test.couleurgauche || couleurgauche==test.couleurdroite || couleurdroite==test.couleurgauche || couleurdroite==test.couleurdroite) {
			return true;
		}else {
			return false;
		}
	}
	public boolean estCompatibleT(Chemin test) {
		if (gauche==test.gauche || droite==test.gauche || gauche==test.droite || droite==test.droite) {
			return true;
		}
		else { return false;}
	}
	public boolean estCompatibleF(Chemin test) {
		if (couleurgauche==test.couleurgauche || couleurgauche==test.couleurdroite || couleurdroite==test.couleurgauche || couleurdroite==test.couleurdroite) {
			return true;
		}else { return false;}
	}
	//Si boolean true nous juxtaposons la piece avec un bord gauche/droite si false bord couleurgauche/couleurdroite
	public void regularisation(Chemin test) {
			if (droite==test.droite || gauche==test.gauche || couleurgauche==test.couleurgauche || couleurdroite==test.couleurdroite) {
				int cont=test.droite;
				test.droite=test.gauche;
				test.gauche=cont;	
				cont=test.couleurdroite;
				test.couleurdroite=test.couleurgauche;
				test.couleurgauche=cont;	}
				
	}
	public String toString() {
		if (gauche==1 && droite==0 && couleurgauche==1 && couleurdroite==0) {
			return "A   ";
		}
		if (gauche==1 && droite==0 && couleurgauche==0 && couleurdroite==1) {
			return "B   ";
		}
		if (gauche==1 && droite==1 && couleurgauche==0 && couleurdroite==0) {
			return "C   ";		
				}
		if (gauche==0 && droite==0 && couleurgauche==1 && couleurdroite==1) {
			return "D   ";
		}
		if (gauche==0 && droite==1 && couleurgauche==1 && couleurdroite==0) {
			return "E   ";
		}
		if (gauche==0 && droite==1 && couleurgauche==0 && couleurdroite==1) {
			return "F   ";
		}
		if (gauche==1 && droite==1 && couleurgauche==1 && couleurdroite==0) {
			return "G   ";
		}
		if (gauche==1 && droite==0 && couleurgauche==1 && couleurdroite==1) {
			return "H   ";
		}
		if (gauche==1 && droite==1 && couleurgauche==0 && couleurdroite==1) {
			return "I   ";
		}
		if (gauche==0 && droite==1 && couleurgauche==1 && couleurdroite==1) {
			return "J   ";
		}
		if (gauche==1 && droite==1 && couleurgauche==1 && couleurdroite==1) {
			return "X   ";
		}
		return "    ";
	}
	//Fait le tableau de compatibilite pour chaque piece
	public String [][] compatiseur(){
		Chemin A=new Chemin(1,0,1,0);
		Chemin B=new Chemin(1,0,0,1);
		Chemin C=new Chemin(1,1,0,0);
		Chemin D=new Chemin(0,0,1,1);
		Chemin E=new Chemin(0,1,1,0);
		Chemin F=new Chemin(0,1,0,1);
		Chemin G=new Chemin(1,1,1,0);
		Chemin H=new Chemin(1,0,1,1);
		Chemin I=new Chemin(1,1,0,1);
		Chemin J=new Chemin(0,1,1,1);
		Chemin X=new Chemin(1,1,1,1);
		String[][] ret=new String [12][11];
            ret[0][0]="A   "; 
			ret[0][1]="B   "; 
			ret[0][2]="C   "; 
			ret[0][3]="D   "; 
			ret[0][4]="E   "; 
			ret[0][5]="F   "; 
			ret[0][6]="G   "; 
			ret[0][7]="H   ";
			ret[0][8]="I   "; 
			ret[0][9]="J   "; 
			ret[0][10]="X   "; 
		for (int j=1;j<12;j++) {
			Chemin Z=new Chemin();
			if(j==1) { Z=A; }
			if(j==2) { Z=B; }
			if(j==3) { Z=C; }
			if(j==4) { Z=D; }
			if(j==5) { Z=E; }
			if(j==6) { Z=F; }
			if(j==7) { Z=G; }
			if(j==8) { Z=H; }
			if(j==9) { Z=I; }
			if(j==10) { Z=J; }
			if(j==11) {Z=X; }
			if (A.estCompatible(Z)) {
				ret[j][0]=Z.toString();
			}
			if (B.estCompatible(Z)) {
				ret[j][1]=Z.toString();
			}
			if (C.estCompatible(Z)) {
				ret[j][2]=Z.toString();
			}
			if (D.estCompatible(Z)) {
				ret[j][3]=Z.toString();
			}
			if (E.estCompatible(Z)) {
				ret[j][4]=Z.toString();
			}
			if (F.estCompatible(Z)) {
				ret[j][5]=Z.toString();
			}
			if (G.estCompatible(Z)) {
				ret[j][6]=Z.toString();
			}
			if (H.estCompatible(Z)) {
				ret[j][7]=Z.toString();
			}
			if (I.estCompatible(Z)) {
				ret[j][8]=Z.toString();
			}
			if (J.estCompatible(Z)) {
				ret[j][9]=Z.toString();
			}
			if (X.estCompatible(Z)) {
				ret[j][10]=Z.toString();
			}
		}
		
		return ret;
}
	public String[] tabCompa(Domin test) {
		if (test instanceof Chemin) {
			String [][] temp=compatiseur();
			for (int i=0;i<11;i++) {
			if (test.toString().equals(temp[0][i].toString())){
				return temp[i];
			}
			}
			return new String[0];
		}
		return new String[0];
	}
}
