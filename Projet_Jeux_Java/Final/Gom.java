
public class Gom extends Domin{
		public int couleurgauche,couleurdroite;
		//4 formes donc gauche/droite prennent comme valeurs 0 a 3
		//4 couleurs donc entre 0 et 3
		//0:Bleu ; 1:Rouge ; 2:Jaune ; 3:Vert
		public Gom(int g, int cg, int d, int cd) {
		super(g, d);
		couleurgauche=cg;
		couleurdroite=cd;
		// TODO Auto-generated constructor stub
	}
		public int getCouleurGauche() {
			return couleurgauche;
			
		}
		public int getCouleurDroite() {
			return couleurdroite;
		}
			public String toString() {
				return "|"+this.gauche+" "+couleurGauche()+" ; "+this.droite+" "+couleurDroite()+"|";
			}
			public String couleurGauche() {
				if (couleurgauche==0) {
					return "Bleu";
				}
				if (couleurgauche==1) {
					return "Rouge";
				}
				if (couleurgauche==2) {
					return "Jaune";
				}		
				if (couleurgauche==3) {
					return "Vert";
				}
				return "";
			}
			public String couleurDroite() {
				if (couleurdroite==0) {
					return "Bleu";
				}
				if (couleurdroite==1) {
					return "Rouge";
				}
				if (couleurdroite==2) {
					return "Jaune";
				}		
				if (couleurdroite==3) {
					return "Vert";
				}
				return "";
			}
			boolean estCompatiblePremier(Domin test) {
			if (test instanceof Gom) { 
				return this.estCompatible(test);
			}
			return false;
			}
		boolean estCompatible(Domin test) {
			 if (test.gauche==this.gauche || test.droite==this.gauche || test.couleurdroite==this.couleurgauche || test.couleurgauche==this.couleurgauche){
		            return true;
		        }else{ return false;
		        }
		}
		boolean estCompatibleDernier(Domin test) {
			if (test instanceof Gom) {
			 if (test.gauche==this.droite || test.droite==this.droite || test.couleurdroite==this.couleurdroite || test.couleurgauche==this.couleurdroite){
		            return true;
		        }else{ return false;
		        }
			}
			return false;
		}
		@Override
		void regularisationDebut(Domin test) {
			if (test.gauche==this.gauche || test.couleurgauche==this.couleurgauche){
	            int cont=test.gauche;
	            test.gauche=test.droite;
	            test.droite=cont;
	            int cont2=test.couleurgauche;
	            test.couleurgauche=test.couleurdroite;
	            test.couleurdroite=cont2;
	        }
		}
		@Override
		void regularisationFin(Domin test) {
			if (test.droite==this.droite || test.couleurdroite==this.couleurdroite){
	            int cont=test.droite;
	            test.droite=test.gauche;
	            test.gauche=cont;
	            int cont2=test.couleurdroite;
	            test.couleurdroite=test.couleurgauche;
	            test.couleurgauche=cont2;
	        }
			
		}
	
}
