
public class Domi extends Domin{
public Domi(int g, int d) {
	super(g,d);	
}
	public int getGauche() {
		return gauche;
		
	}
	public int getDroite() {
		return droite;
	}
	public String toString(){
        return "|"+this.gauche+" ; "+this.droite+"|";
    }
	@Override
	boolean estCompatiblePremier(Domin test) {
		 if (test.gauche==this.gauche || test.droite==this.gauche){
	            return true;
	        }else{ return false;
	        }
	}
	@Override
	boolean estCompatibleDernier(Domin test) {
		 if (test.gauche==this.droite || test.droite==this.droite){
	            return true;
	        }else{ return false;
	        }
	}
	@Override
	void regularisationDebut(Domin test) {
		if (test.gauche==this.gauche){
            int cont=test.gauche;
            test.gauche=test.droite;
            test.droite=cont;
        }
		
	}
	@Override
	void regularisationFin(Domin test) {
		if (test.droite==this.droite){
            int cont=test.droite;
            test.droite=test.gauche;
            test.gauche=cont;
        }
		
	}
	
}
