import java.util.ArrayList;
public class ConstructeurJeu {
	    public ArrayList<Domin> jeu;
	    public ConstructeurJeu(int choix){
	        jeu=new ArrayList<Domin>();
	        if (choix==1){
	        for (int i=0;i<7;i++){
	            for (int j=i;j<7;j++){
	            jeu.add(new Domi(i,j));
	            }
	        }
	        }
	        if (choix==2) {
	        	//La solution pour creer un jeu aleatoire n'a pas ete trouvee, on cree donc ici un jeu totalement aleatoire avec chaque piece unique pour pouvoir jouer
	    		jeu.add(new Gom(0,0,0,1));
	    		jeu.add(new Gom(0,0,1,1));
	    		jeu.add(new Gom(0,1,1,2));
	    		jeu.add(new Gom(0,3,2,1));
	    		jeu.add(new Gom(0,0,2,2));
	    		jeu.add(new Gom(0,1,3,2));
	    		jeu.add(new Gom(0,2,3,3));
	    		jeu.add(new Gom(1,2,0,3));
	    		jeu.add(new Gom(1,2,0,2));
	    		jeu.add(new Gom(1,2,1,3));
	    		jeu.add(new Gom(1,3,2,0));
	    		jeu.add(new Gom(1,0,2,1));
	    		jeu.add(new Gom(1,1,3,0));
	    		jeu.add(new Gom(1,2,3,2));
	    		jeu.add(new Gom(2,0,0,2));
	    		jeu.add(new Gom(2,1,0,2));
	    		jeu.add(new Gom(2,2,1,2));
	    		jeu.add(new Gom(2,3,1,0));
	    		jeu.add(new Gom(2,0,2,2));
	    		jeu.add(new Gom(2,1,3,2));
	    		jeu.add(new Gom(2,2,3,3));
	    		jeu.add(new Gom(3,0,0,1));
	    		jeu.add(new Gom(3,1,0,0));
	    		jeu.add(new Gom(3,2,1,3));
	    		jeu.add(new Gom(3,3,1,0));
	    		jeu.add(new Gom(3,0,2,1));
	    		jeu.add(new Gom(3,1,2,3));
	    		jeu.add(new Gom(3,2,3,0));
	        }
	        if (choix==3) {
	        	//Creation d'un puzzle 3x3 avec 0 comme attibut si la piece est sur le bord et 13 attributs differents pour rendre les 9 pieces uniques
	        	jeu.add(new Puzzle(0,1,0,2));
	        	jeu.add(new Puzzle(1,3,0,4));
	        	jeu.add(new Puzzle(3,0,0,5));
	        	jeu.add(new Puzzle(0,6,2,7));
	        	jeu.add(new Puzzle(6,8,4,9));
	        	jeu.add(new Puzzle(8,0,5,11));
	        	jeu.add(new Puzzle(0,12,7,0));
	        	jeu.add(new Puzzle(12,13,9,0));
	        	jeu.add(new Puzzle(13,0,11,0));
	        }
	        if (choix==4) {
	    		//Il y a 2 parmi 4 facon de creer une piece avec un seul chemin entre deux cotes, 3 parmi 4 pour des pieces avec deux chemins entrecroises et 4 parmi 4 pour 3 chemins.
	    		//Donc 6 pieces , 4 pieces, 1 piece
	    		//On cree 4xles 6 pieces, les 4 pieces et la 1 piece
	    	for (int i=0;i<4;i++) {
	    		jeu.add(new Chemin(1,0,1,0));
	    		jeu.add(new Chemin(1,0,0,1));
	    		jeu.add(new Chemin(1,1,0,0));
	    		jeu.add(new Chemin(0,0,1,1));
	    		jeu.add(new Chemin(0,1,1,0));
	    		jeu.add(new Chemin(0,1,0,1));
	    		jeu.add(new Chemin(1,1,1,0));
	    		jeu.add(new Chemin(1,0,1,1));
	    		jeu.add(new Chemin(1,1,0,1));
	    		jeu.add(new Chemin(0,1,1,1));
	    		jeu.add(new Chemin(1,1,1,1));
	    		}
	    	//Il y a 3 cartes de sabotage differentes et 3 cartes de reparation differentes 
	    	//Comme il en faut 4 de chaque type
	    	//Representation : Action 1:Transport / Action 2:Outils / Action 3: Eclairage / Action 4:Rep Transport / Action 5:Rep Outils / Action 6:Rep Eclairage
	    	for (int i=0;i<4;i++) {
	    		jeu.add(new Action(2));
	    		jeu.add(new Action(3));
	    		jeu.add(new Action(4));
	    		jeu.add(new Action(5));
	    		jeu.add(new Action(6));
	    		jeu.add(new Action(7));}
	    	}
	    	//Creation des Cartes de Depart et d'Arrivees
	    	//Carte de depart= carte de chemin 4 chemins
	    	//Carte d'arrivees x3
	    	jeu.add(new Chemin(1,1,1,1));
	    	jeu.add(new Arrivee());
	    	jeu.add(new Arrivee());
	    	jeu.add(new Arrivee());
	    	
	    }	    
	    
	    public String toString() {
	    	String ret="";
	    	for (int i=0;i<jeu.size();i++) {
	    		ret=ret+jeu.get(i).toString()+"\n \n";
	    	}
	    	return ret;
	    }
}


