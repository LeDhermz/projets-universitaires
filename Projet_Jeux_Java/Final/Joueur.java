import java.util.ArrayList;

public class Joueur{
    public int age;
    public String nom;
    public Hand hand;
    private ArrayList<Action> restriction=new ArrayList<Action>();
    private int gain=0;
    public Joueur() {
    this.hand=new Hand();
    }
    public Joueur(int age){
        this.age=age;
        this.hand=new Hand();
    }
    public String toString(){
        return "Joueur : "+nom+" Age : "+age+" "+hand.toString();
    }
    public void Restriction() {
    	if (!restriction.isEmpty()) {
    		System.out.println("Vous avez ces sabotages : ");
    		for (int i=0;i<restriction.size();i++) {
    		System.out.println(restriction.get(i)+"  ");
    	}
    		System.out.println("Utiliser les reparations correspondantes pour recommencer a jouer");
    }else {
    	System.out.println("Vous n'avez aucuns sabotages");
    }  
}
    public boolean Restriction2() {
    	return !(restriction.isEmpty());
    }
    public ArrayList<Action> getRestriction(){
    	return restriction;
    }
    public void setRestriction(Domin rest) {
    	if (rest instanceof Action) {
    	restriction.add((Action)rest);
    	}
    }
    public void setGain(int g) {
    	gain=gain+g;
    }
    public int getGain() {
    	return gain;
    }
}
