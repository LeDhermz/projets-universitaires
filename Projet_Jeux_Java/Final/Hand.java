import java.util.ArrayList;
public class Hand{
    public ArrayList<Domin> enMain;
    public Hand(){
        enMain=new ArrayList<Domin>();
    }
    public void add(Domin a){
        enMain.add(a);
}
    public String toString(){
        String s="Main du joueur : ";
        if (enMain.isEmpty()){
            s=s+" Aucune piece a jouer";
        }
        
        for (int i=0;i<enMain.size();i++){
            s=s+"\n"+(i+1)+" : "+enMain.get(i).toString();
        }
        return s;
    }
}
