import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;
import java.util.LinkedList;
// Cette classe permet de reproduire le plateau de jeu ainsi que les mains
class Dom extends JPanel {
    public Vieww v;
    private BufferedImage[] is;
    private Domin[][] ttmains;
    private BufferedImage image;
    private LinkedList<Domin> plateau;
    private int nb;
    private Joueur[] joueurs;
    public JTextArea Text;

    public Dom(Domin[][] t,LinkedList<Domin> l,int n,Joueur[] j) {

	plateau=l;
	nb=n;
	joueurs=j;
	    ttmains=t;
	    is= new BufferedImage[7];
        try {
            image = ImageIO.read(new File("../Image/Dice0.png"));
	    is[0]=image;
	    image = ImageIO.read(new File("../Image/Dice1.png"));
	    is[1]=image;
	    image = ImageIO.read(new File("../Image/Dice2.png"));
	    is[2]=image;
	    image = ImageIO.read(new File("../Image/Dice3.png"));
	    is[3]=image;
	    image = ImageIO.read(new File("../Image/Dice4.png"));
	    is[4]=image;
	    image = ImageIO.read(new File("../Image/Dice5.png"));
	    is[5]=image;
	    image = ImageIO.read(new File("../Image/Dice6.png"));
	    is[6]=image;
        } catch (IOException e) {
            e.printStackTrace();
        }
	
	    
    }

    public void paintComponent(Graphics g) {
	
        super.paintComponent(g);
	g.setColor(Color.YELLOW);
	g.fillRect(0,0,230,250);
	g.setColor(Color.MAGENTA);
	g.fillRect(0,250,230,250);

	//this.setLayout(null);

	//JLabel j1 = new JLabel(joueurs[0].nom);
	//JLabel j2 = new JLabel(joueurs[1].nom);
	//j1.setLocation(300,200);
	//this.add(j1);

	

	//Text= new JTextArea(joueurs[1].nom);
	//Text.setLocation(500,500);
	//this.add(Text);
	
	// Ci dessous on place les zone colorees pour l'espace des joueurs
	    
	if(joueurs.length>=3){
	g.setColor(Color.GRAY);
	g.fillRect(0,500,230,250);
	}
	if(joueurs.length==4){
	g.setColor(Color.RED);
	g.fillRect(0,750,230,250);
	}


	// on dessine les mains des joueurs

	for(int p=0;p<joueurs.length; p++){
	
	    int c=0;                        //compteursDomino;
	    
	    
	    for(int j=0;j<joueurs[p].hand.enMain.size();j++){
		               
		    c=c+1;
		    int h=62*(c-1)+5;                
		    int v=20+250*p; 
		    int a= ttmains[p][j].getGauche();
		    int b= ttmains[p][j].getDroite();
		    if(c<4){
			g.drawImage(is[a],h,v,this);
			g.drawImage(is[b],h,v+30,this);
		    }
		    else{
		    
			v=85+250*p;
			h=62*(c-4)+5;
			g.drawImage(is[a],h,v,this);
			g.drawImage(is[b],h,v+30,this);
		    }
		
		
	       	
	    }
	}

	//on dessine le plateau
	
	int hauteur =400;
	int largeur =100;
        for(int k=0;k<plateau.size();k++){
	    Domin doi = plateau.get(k);
	    hauteur=hauteur+60;

	    int a= doi.getGauche();
	    int b= doi.getDroite();
	    g.drawImage(is[a],hauteur,largeur,this);
	    g.drawImage(is[b],hauteur+30,largeur,this);
	}    	   


    }
	    
}



//	ImageIcon i=new ImageIcon("/home/antoine/infop7/Projet/Image/Dice1.png");
//	i.paintIcon(this,g,30,0);
