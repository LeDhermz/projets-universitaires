import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.LinkedList;
public class Jeu{
    private Joueur[] joueurs;
    private ArrayList<Domin> pioche;
    public ConstructeurJeu jeudedominos;
    public SuiteDominos plateaudejeu;
    public boolean findePartie;
    public Plat plateaupuzzle;
    public PlatS plateauSabo;
	private Scanner sc;
	private Scanner scc;
	private Scanner sccc;
	private Scanner scr;
	private Scanner choixdejeu;
	private int nombrej;//Indique 1 pour Puzzle et 4 pour Saboteur
	private Scanner scp1;
	private Scanner scp2;
	private Scanner sca;
	private Scanner scan;
	private Scanner scan2;
	private Domin[][] ttmains;
	private Scanner scan3;
        private Vieww view;
    public Jeu(int InterChoix){
        findePartie=false;
        //Cas Particulier Puzzle:
        if (InterChoix==3) {
        	//Initialisation du plateau Puzzle
        	plateaupuzzle=new Plat();
        	nombrej=1;
        	joueurs=new Joueur[1];
        	joueurs[0]=new Joueur();
        	System.out.println("Vous avez choisi de jouer au puzzle qui se joue a 1 joueur. Ce puzzle possede 9 pieces differentes les unes des autres. Ce jeu consiste a replacer les 9 pieces pour former un motif uniforme de forme carre de 3 pieces de cote. L'attribut 0 correspond aux cotes des pieces de la bordure du motif. Il s'agira ensuite de bien placer les pieces pour coller les cotes des pieces avec les memes attributs. Bonne chance, voici les pieces: ");
        	//Initialisation des pieces
        	jeudedominos=new ConstructeurJeu(3);
        	//Distribution aleatoire des pieces
        	distribution(1);
        	System.out.println("Voici le plateau de jeu du puzzle et voici tes pieces : Choisis la piece de ton jeu que tu veux jouer et entre ensuite la place sur le plateau de jeu entre 1 et 9 ou tu veux la placer");
        	System.out.println(plateaupuzzle);
        	boolean finpuzzle=false;
        	//Tour de puzzle
	        	while(!finpuzzle) {
	        		for (int i=0;i<joueurs[0].hand.enMain.size();i++) {
	            		System.out.println((i+1)+" : \n"+joueurs[0].hand.enMain.get(i)+"\n ");   
	            		}
	        		System.out.println("Quelle piece veux tu poser?");
	        		scp1 = new Scanner(System.in);
	        		int scp=scp1.nextInt();
	        		while(scp>joueurs[0].hand.enMain.size()) {
	        			scp--;
	        		}
	        		System.out.println(joueurs[0].hand.enMain.get(scp-1));
	            	System.out.println("Ou veux tu poser cette piece");
	            	scp2 = new Scanner(System.in);
	            	int scpp=scp2.nextInt();
	            	if (scp<10 && scp>0 && scpp>0 && scpp<10) {
	            		if(posagedepuzzle(scp,scpp)) {
	            			if(plateaupuzzle.plateau[scpp-1] instanceof Puzzle) {
	        			plateaupuzzle.plateau[scpp-1]=joueurs[0].hand.enMain.get(scp-1);
	        			joueurs[0].hand.enMain.remove(scp-1);
	        			System.out.println("Bravo vous avez trouve la bonne position pour cette piece");
	        			finpuzzle=finPuzzle();
	            			}	
	            			System.out.println(plateaupuzzle);
	            		}
	            	}else {
	            		System.out.println("Piece ou lieu de pose errone(e)(s)");
	            	}
	        	}
	        	System.out.println("Felicitation vous avez complete le jeu Puzzle");     	
        }else {
        	
        	
        //Intialisation de tour et classement du tour des joueurs selon l'age / Si meme age, l'ordre d'enregistrement determinera l'ordre de jeu
        System.out.println("Combien y a t'il de joueurs?");
        sc = new Scanner(System.in);
        int i = sc.nextInt();
        while (i<2 || i>4) { System.out.println("Le nombre de joueurs doit ete compris entre 2 et 4, veuillez rentrez un nombre de joueurs correct :");
        i=sc.nextInt();
        }
        joueurs=new Joueur[i];
        for (int j=0;j<i;j++){
            System.out.println("Age du joueur?");
            scc = new Scanner(System.in);
            joueurs[j]=new Joueur(scc.nextInt());
            System.out.println("Nom du joueur?");
            sccc = new Scanner(System.in);
            joueurs[j].nom=sccc.nextLine();
            
        }
        //Appel a la fonction de tri
        triBulleCroissant();
        ttmains=new Domin[i][7];
        //Affichage de l'ordre des joueurs
        getTour();
        //Distribution des dominos : 28 dominos : 7 par personne le reste a la pioche ou 72 pieces pour le saboteur
        //Initialisation de la poche
        pioche=new ArrayList<Domin>();
        //Creation du jeu de dominos de 28 dominos ou 72 pieces pour Saboteur
        jeudedominos=new ConstructeurJeu(InterChoix); 
        int cont=jeudedominos.jeu.size();
        //On retire les 4 dernieres pieces du jeu Saboteur car places automatiquement sur le plateau Saboteur
        if (InterChoix==4) {
        	nombrej=4;
        	int cptcpt=0;
        	while(cptcpt<4) {
        	jeudedominos.jeu.remove(cont-1);
        	cptcpt++;
        	cont--;
        	}
        }
        //Initialise la main des joueurs et la pioche aleatoirement en fonction du nombre de joueurs
        distribution(i);
        for (int k=0;k<joueurs.length;k++) {
        	for (int j=0;j<joueurs[k].hand.enMain.size();j++){
        		ttmains[k][j]=joueurs[k].hand.enMain.get(j);
        		
        	}
        }
         
        
        if (InterChoix==4) {
        	//tour de saboteur + affichage des indications sur les pieces
        	tableaudeC();
        	plateauSabo=new PlatS();
        	while(this.findePartie!=true) {
             System.out.println(joueurs[0].nom+" : c'est a votre tour !");
             if(presenceSabo()) {
            	 if (presenceRep(joueurs[0].getRestriction())) {
            		 System.out.println("Voulez vous poser votre reparation ? Entrez oui ou non");
            		 sca = new Scanner(System.in);
            		 String test=sca.nextLine();
            		 if (test.contentEquals("oui")) {
            			 joueurs[0].hand.enMain.remove(trouveRep(joueurs[0].getRestriction().get(0)));
            			 System.out.println("Vous n'etes plus sabote");
            		 }else {
            			 System.out.println("Vous passez donc votre tour, une de vos pieces va aleatoirement etre defaussee");
            			 joueurs[0].hand.enMain.remove((int)Math.round(Math.random() * 5));
            		 }
            	 }else { 
            		 System.out.println("Vous passez donc votre tour car vous avez ete sabote et vous n'avez pas la piece de reparation correspondante, une de vos pieces va aleatoirement etre defaussee");
            		 joueurs[0].hand.enMain.remove((int)Math.round(Math.random() * 5));
            	 }
            	 }else {
            		
            		 poseurSaboteur();
            		 
            	 }
             	piocheurSabo();
            	decalageJoueur();
            	findePartie=finSabo();
        	}
        	//Tri et classement de fin de partie de saboteur
        	triDeFin();
        	classement();
        	  //Affiche les mains de tous les joueurs dans l'ordre du tour et la pioche pour verifier que tout fonctionne
            //afficheMain();
            //Tour du premier joueur et initialisation du plateau de jeu	
        }else {
	    
         
        } if (InterChoix==1 || InterChoix==2){
		
		if(InterChoix==1){
		    plateaudejeu=new SuiteDominos();
		    view=new Vieww(nombrej,this);}
            System.out.println(joueurs[0].hand);
            System.out.println("Pose la premiere piece "+joueurs[0].nom+ " !");
        System.out.println("C'est facile, tes dominos sont affiches devant toi, indique le numero du premier domino que tu veux poser :");
	if(InterChoix==2){
	scr = new Scanner(System.in);
        int aposer=scr.nextInt();
	 plateaudejeu=new SuiteDominos(joueurs[0].hand.enMain.get(aposer-1));
        joueurs[0].hand.enMain.remove(aposer-1);
	
	}
	if (InterChoix==1){
	while(view.getAction()==false){
	    System.out.print("");
	}
	view.setActionB();
	
	
        plateaudejeu=new SuiteDominos(joueurs[0].hand.enMain.get(view.getBouton()-1));
        joueurs[0].hand.enMain.remove(view.getBouton()-1);
	}

	   
	
        for (int k=0;k<joueurs.length;k++) {
        	for (int j=0;j<joueurs[k].hand.enMain.size();j++){
        		ttmains[k][j]=joueurs[k].hand.enMain.get(j);
        		
        	}
        }
        }
	if(InterChoix==1){
	    view.refresh();
	}


 if (InterChoix==1){
        while(this.findePartie!=true){
            System.out.println(plateaudejeu);
            decalageJoueur();
            System.out.println(joueurs[0].nom+" : Il vous reste "+joueurs[0].hand.enMain.size()+" pieces a jouer avant de gagner");
            Hand compatible=peutjouer();
            System.out.println(compatible);
            int choix;
	    
	    while(view.getAction()==false){
	        System.out.print("");
	    }
	    view.setActionB();
	    choix=view.getBouton();
	
	//choixdejeu = new Scanner(System.in);
	//  choix=choixdejeu.nextInt();
	    
            if (choix==0){
                joueurs[0].hand.enMain.add(pioche.get(pioche.size()-1));
                pioche.remove(pioche.size()-1);
            }
            if ((choix>=1) && (choix<=compatible.enMain.size())){
                poseurdepiece(compatible.enMain.get(choix-1));
            }
            if (choix>compatible.enMain.size()){
                System.out.println("Votre piece n'est pas repertorie, vous allez passer votre tour");
            }
            for (int k=0;k<joueurs.length;k++) {
            	for (int j=0;j<joueurs[k].hand.enMain.size();j++){
            		ttmains[k][j]=joueurs[k].hand.enMain.get(j);
            		
            	}
            }
	    if(InterChoix==1){
	    view.refresh();
	}
            this.findePartie=findepartie();
        }
	}
        //Tour de jeu Dominos
	if(InterChoix==2){
        while(this.findePartie!=true){
            System.out.println(plateaudejeu);
            decalageJoueur();
            System.out.println(joueurs[0].nom+" : Il vous reste "+joueurs[0].hand.enMain.size()+" pieces a jouer avant de gagner");
            Hand compatible=peutjouer();
            System.out.println(compatible);
            int choix;              
            choixdejeu = new Scanner(System.in);
            choix=choixdejeu.nextInt();
            if (choix==0){
                joueurs[0].hand.enMain.add(pioche.get(pioche.size()-1));
                pioche.remove(pioche.size()-1);
            }
            if ((choix>=1) && (choix<=compatible.enMain.size())){
                poseurdepiece(compatible.enMain.get(choix-1));
            }
            if (choix>compatible.enMain.size()){
                System.out.println("Votre piece n'est pas repertorie, vous allez passer votre tour");
            }
            for (int k=0;k<joueurs.length;k++) {
            	for (int j=0;j<joueurs[k].hand.enMain.size();j++){
            		ttmains[k][j]=joueurs[k].hand.enMain.get(j);
            		
            	}
            }
            this.findePartie=findepartie();
	}
	}
        //La partie est terminee, impression des classements des joueurs
        triDeFin();
        classement();
        }
       
	
    
    }
    //FONCTIONS ANNEXES ET COLLABORATIVES
    public Joueur[] getJoueurs(){
	return joueurs;
    }
    public boolean testvoisin(String[] t) {
	       int b=0;
	       for (int i=0;i<t.length;i++) {
	    	   if (t[i].equals("    ")) {
	    		   b++;
	    	   }
	       }
	       if (b==0 || b==4) { return false;}
	       return true;
	}
	public int nombrevoisin(String[] t) {
		int cpt=4;
		for (int i=0;i<t.length;i++) {
			if(t[i].equals(null)) {
				System.out.println("estVide");
				cpt--;
			}
			
		}
		return cpt;
}
	public boolean present(String[]t1, String[] t2) {
		boolean etat=false;
		for (int i=0;i<t1.length;i++) {
			etat=false;
			for (int j=0;j<t2.length;j++) {
				if (t1[i]==t2[j]) {
					etat=true;
				}
			}
			if (etat!=true) { return false;}
			
		}
		return etat;
		}
    public void triBulleCroissant() {
        int longueur = joueurs.length;
        Joueur a=null;
        boolean permut;
        do {
            // hypothese : le tableau est trie
            permut = false;
            for (int i = 0; i < longueur - 1; i++) {
                // Teste si 2 elements successifs sont dans le bon ordre ou non
                if (joueurs[i].age > joueurs[i + 1].age) {
                    // s'ils ne le sont pas, on echange leurs positions
                    a=joueurs[i];
                    joueurs[i] = joueurs[i + 1];
                    joueurs[i + 1] = a;
                    permut = true;
                }
                if (joueurs[i].age==joueurs[i+1].age){
                    int test=(int) Math.random() * (2);
                    if (test==1){
                        a=joueurs[i];
                        joueurs[i] = joueurs[i + 1];
                        joueurs[i + 1] = a;
                        permut = true;}
                }
                        
            }
        } while (permut);
    }
    //Fonction d'affichage des informations de joueur
    public void getTour(){
        String affiche="";
        for (int i=0;i<joueurs.length;i++){affiche=affiche+" Joueur "+i+" :"+this.joueurs[i].nom+" :"+this.joueurs[i].age+" ans"+"\n";}
        System.out.println(affiche);}
    //Tri classement fin de partie 
    public void triDeFin(){
   	 int longueur = joueurs.length;
        Joueur a=null;
        boolean permut;
   	if(nombrej==4) {
   		// hypothese : le tableau est trie
           permut = false;
           for (int i = 0; i < longueur - 1; i++) {
               // Teste si 2 elements successifs sont dans le bon ordre ou non
               if ((joueurs[i].getGain()) > (joueurs[i + 1].getGain())) {
                   // s'ils ne le sont pas, on echange leurs positions
                   a=joueurs[i];
                   joueurs[i] = joueurs[i + 1];
                   joueurs[i + 1] = a;
                   permut = true;
               }
               if (joueurs[i].hand.enMain.size()==joueurs[i+1].hand.enMain.size()){
                   int test=(int) Math.random() * (2);
                   if (test==1){
                       a=joueurs[i];
                       joueurs[i] = joueurs[i + 1];
                       joueurs[i + 1] = a;
                       permut = true;}
               }
                       
           }
        while (permut);
   		
   	}else {
       
       do {
           // hypothese : le tableau est trie
           permut = false;
           for (int i = 0; i < longueur - 1; i++) {
               // Teste si 2 elements successifs sont dans le bon ordre ou non
               if ((joueurs[i].hand.enMain.size()) > (joueurs[i + 1].hand.enMain.size())) {
                   // s'ils ne le sont pas, on echange leurs positions
                   a=joueurs[i];
                   joueurs[i] = joueurs[i + 1];
                   joueurs[i + 1] = a;
                   permut = true;
               }
               if (joueurs[i].hand.enMain.size()==joueurs[i+1].hand.enMain.size()){
                   int test=(int) Math.random() * (2);
                   if (test==1){
                       a=joueurs[i];
                       joueurs[i] = joueurs[i + 1];
                       joueurs[i + 1] = a;
                       permut = true;}
               }
                       
           }
       } while (permut);}
   }
    //Fonction decalant les joueurs a chaque tour
   public void decalageJoueur(){
       Joueur cont=joueurs[0];
       for (int i=0;i<joueurs.length-1;i++){
           joueurs[i]=joueurs[i+1];
       }
       joueurs[joueurs.length-1]=cont;
   }
   //Fonction permettant de savoir si la partie est terminee
   public boolean findepartie(){
       if (joueurs[0].hand.enMain.isEmpty() || pioche.isEmpty() ){
           return true;
       }else{
           return false;
       }
   }
   //Fonction interne Exception
   class NbJoueursIncorrect extends Exception{
       /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public NbJoueursIncorrect(){
           System.out.println("Nombre de joueurs doit etre entre 2 et 4"); } }
   //Fonction distribuant les pieces dans chaque jeu
   public void distribution(int nbj){
       int Domi[] = {0, 1, 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27};
       int Domi2[]= {0,1,2,3,4,5,6,7,8};
       int Domi3[]=new int[68];
       for (int cpt=0;cpt<68;cpt++) {
       	Domi3[cpt]=cpt;
       }
       Shuffle.shuffleArray(Domi);
       Shuffle.shuffleArray(Domi2);
       Shuffle.shuffleArray(Domi3);
       if (nombrej==1) {
       if (nbj==1) {
       	for (int cpt=0;cpt<9;cpt++) {
       		
       		  joueurs[0].hand.enMain.add(jeudedominos.jeu.get(Domi2[cpt]));
       	}
       	return;
       }
       }
       if (nombrej==4) {
       for (int b=0;b<5;b++) {
       	joueurs[0].hand.enMain.add(jeudedominos.jeu.get(Domi3[b]));
           if (nbj==2){joueurs[1].hand.enMain.add(jeudedominos.jeu.get(Domi3[b+5]));}
           if (nbj==3){
               joueurs[1].hand.enMain.add(jeudedominos.jeu.get(Domi3[b+5]));
               joueurs[2].hand.enMain.add(jeudedominos.jeu.get(Domi3[b+10]));}
           if (nbj==4){
               joueurs[1].hand.enMain.add(jeudedominos.jeu.get(Domi3[b+5]));
               joueurs[2].hand.enMain.add(jeudedominos.jeu.get(Domi3[b+10]));
               joueurs[3].hand.enMain.add(jeudedominos.jeu.get(Domi3[b+15]));}
       }
       for (int j=20;j<68;j++){
           pioche.add(jeudedominos.jeu.get(Domi3[j]));}  
       return;}
       // if (nbj>4 || nbj<2){ throw new NbJoueursIncorrect();}
       for (int j=0;j<7;j++){
           joueurs[0].hand.enMain.add(jeudedominos.jeu.get(Domi[j]));
           if (nbj==2){joueurs[1].hand.enMain.add(jeudedominos.jeu.get(Domi[j+7]));}
           if (nbj==3){
               joueurs[1].hand.enMain.add(jeudedominos.jeu.get(Domi[j+7]));
               joueurs[2].hand.enMain.add(jeudedominos.jeu.get(Domi[j+14]));}
           if (nbj==4){
               joueurs[1].hand.enMain.add(jeudedominos.jeu.get(Domi[j+7]));
               joueurs[2].hand.enMain.add(jeudedominos.jeu.get(Domi[j+14]));
               joueurs[3].hand.enMain.add(jeudedominos.jeu.get(Domi[j+21]));}
       }
       if (nbj==2){for (int j=14;j<28;j++){
               pioche.add(jeudedominos.jeu.get(Domi[j])); } }
       if (nbj==3){for (int j=21;j<28;j++){
               pioche.add(jeudedominos.jeu.get(Domi[j]));} } } 
   //Affiche les mains de chaque joueur
   public void afficheMain(){
       System.out.print("Pioche :");
       if (pioche.isEmpty()){ System.out.print(" Vide");
       }else{
           for (int ite=0;ite<pioche.size();ite++){
               System.out.print(pioche.get(ite));
               System.out.print(" ");
           }
       }
       System.out.println();
       for (int ite=0;ite<joueurs.length;ite++){
           System.out.print(ite+" : ");
           System.out.print(joueurs[ite]);
           System.out.println();
       }
       
           
   }    
   public void classement(){
       System.out.println("FELICITATIONS "+joueurs[0].nom);
       for (int i=0;i<joueurs.length;i++){
           System.out.println((i+1)+" : "+joueurs[i].nom);
       }
   }
    //Fonctions interface graphique
   public Domin[][] getTtmains(){
	   return ttmains;
   }
    
   
 
    //Fonctions dominos / dominos gomettes
    public void poseurdepiece(Domin choisi){
        for (int i=0;i<joueurs[0].hand.enMain.size();i++){
            if (joueurs[0].hand.enMain.get(i).equals(choisi)){
                if(plateaudejeu.plateau.getFirst().estCompatiblePremier(joueurs[0].hand.enMain.get(i))){
                    plateaudejeu.plateau.getFirst().regularisationDebut(joueurs[0].hand.enMain.get(i));
                    plateaudejeu.plateau.addFirst(joueurs[0].hand.enMain.get(i));
                    joueurs[0].hand.enMain.remove(i);
                    return;
                }
                if(plateaudejeu.plateau.getLast().estCompatibleDernier(joueurs[0].hand.enMain.get(i))){
                    plateaudejeu.plateau.getLast().regularisationFin(joueurs[0].hand.enMain.get(i));
                    plateaudejeu.plateau.addLast(joueurs[0].hand.enMain.get(i));
                    joueurs[0].hand.enMain.remove(i);
                    return;
                }
            }
        }
    
    }
    


                
    public Hand peutjouer(){
        System.out.println("C'est a votre tour "+joueurs[0].nom);
        int cpt=0;
        Hand provisoire=new Hand();
        for (int i=0;i<joueurs[0].hand.enMain.size();i++){
            if ( ((plateaudejeu.plateau.getFirst().estCompatiblePremier(joueurs[0].hand.enMain.get(i)))==true) || ((plateaudejeu.getDernier().estCompatibleDernier(joueurs[0].hand.enMain.get(i)))==true)){
                cpt++;
                provisoire.add(joueurs[0].hand.enMain.get(i));
            }
        }
        if (cpt==0){
            System.out.println("Vous ne pouvez pas jouer, entrez 0 pour piocher");
        }
        if (cpt>0){
            System.out.println("Voici les pieces que vous pouvez jouer, entrez le numero de la piece que vous voulez posez sur le plateau :");
        }
                   
                
        return provisoire;
    }
    //Fonctions puzzle
    public boolean posagedepuzzle(int pie, int lieu) {
    	if (lieu==1) {
    		if (joueurs[0].hand.enMain.get(pie-1).gauche==0 && joueurs[0].hand.enMain.get(pie-1).couleurgauche==0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==2) {
    		if (joueurs[0].hand.enMain.get(pie-1).couleurgauche==0 && joueurs[0].hand.enMain.get(pie-1).gauche!=0 && joueurs[0].hand.enMain.get(pie-1).droite!=0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==3) {
    		if (joueurs[0].hand.enMain.get(pie-1).droite==0 && joueurs[0].hand.enMain.get(pie-1).couleurgauche==0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==4) {
    		if (joueurs[0].hand.enMain.get(pie-1).gauche==0 && joueurs[0].hand.enMain.get(pie-1).couleurgauche!=0 && joueurs[0].hand.enMain.get(pie-1).couleurdroite!=0 ) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==5) {
    		if (joueurs[0].hand.enMain.get(pie-1).gauche!=0 && joueurs[0].hand.enMain.get(pie-1).couleurgauche!=0 && joueurs[0].hand.enMain.get(pie-1).droite!=0 && joueurs[0].hand.enMain.get(pie-1).couleurdroite!=0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==6) {
    		if (joueurs[0].hand.enMain.get(pie-1).droite==0 && joueurs[0].hand.enMain.get(pie-1).couleurgauche!=0 && joueurs[0].hand.enMain.get(pie-1).couleurdroite!=0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==7) {
    		if (joueurs[0].hand.enMain.get(pie-1).gauche==0 && joueurs[0].hand.enMain.get(pie-1).couleurdroite==0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==8) {
    		if (joueurs[0].hand.enMain.get(pie-1).gauche!=0 && joueurs[0].hand.enMain.get(pie-1).couleurdroite==0 && joueurs[0].hand.enMain.get(pie-1).droite!=0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	if (lieu==9) {
    		if (joueurs[0].hand.enMain.get(pie-1).droite==0 && joueurs[0].hand.enMain.get(pie-1).couleurdroite==0) {
    			return true;
    		}else {
    			return false;
    		}
    	}
    	return false;
	}                    
    public boolean finPuzzle() {
		for (int i=0;i<9;i++) {
			if (plateaupuzzle.plateau[i].gauche==i+1 && plateaupuzzle.plateau[i].droite==i+1 && plateaupuzzle.plateau[i].couleurgauche==i+1 && plateaupuzzle.plateau[i].couleurdroite==i+1) {
				return false;
			}
		}
		return true;
	}
    //FONCTIONS SABOTEUR
    private boolean finSabo() {
		if (plateauSabo.plateauS[5][11].equals("R   ") && plateauSabo.plateauS[7][11].contentEquals("R   ") && plateauSabo.plateauS[9][11].contentEquals("R   ")) {
			return true;
		}
		return false;
	}




	private void piocheurSabo() {
		if(!pioche.isEmpty())
		joueurs[0].hand.enMain.add(pioche.get(0));
		pioche.remove(0);
	}



//Fonction appelee a chaque tour pour chaque joueur
	private void poseurSaboteur() {
		System.out.println(plateauSabo);
		 System.out.println(joueurs[0].hand.enMain);
		System.out.println("Indiquez le nombre de la piece que vous voulez poser ou indiquer 0 pour vous defausser d'un chemin");
		scan = new Scanner(System.in);
		int choixS=scan.nextInt();
		if (choixS<0 && choixS>5) { 
			System.out.println("Nombre incorrect, veuillez choisir une autre piece :");
			choixS=scan.nextInt();
		}
		if(choixS==0) { 
			for (int i=0;i<joueurs[0].hand.enMain.size();i++) {
				if(joueurs[0].hand.enMain.get(i) instanceof Chemin) {
				System.out.println(i+" : "+joueurs[0].hand.enMain.get(i));
			}
			}
				scan3 = new Scanner(System.in);
				int choixS3=scan3.nextInt();	
				joueurs[0].hand.enMain.remove(choixS3);
				choixS=100;
		}	
			choixS--;
			if ( joueurs[0].hand.enMain.get(choixS).gauche>4) {
				System.out.println("Vous ne pouvez pas jouer de Reparation, veuillez choisir une autre piece :");
				poseurSaboteur();
			}
			if (joueurs[0].hand.enMain.get(choixS).gauche<5 && joueurs[0].hand.enMain.get(choixS).gauche>1) {
				for (int i=1;i<joueurs.length;i++) {
					System.out.println(i+" : "+joueurs[i].nom+"  ");
				}
			
				System.out.println("Choisissez le joueur sur lequel vous voulez envoyer ce sabotage :");
				scan2 = new Scanner(System.in);
				int choixS2=scan2.nextInt();
				 while(choixS2>joueurs.length) {
					 System.out.println("Nombre incorrect, veuillez choisir un autre nombre :");
					choixS2=scan2.nextInt();
				     }
					joueurs[choixS2].setRestriction(joueurs[0].hand.enMain.get(choixS));
					System.out.println("Vous avez correctement saboter "+joueurs[choixS2].nom);
					joueurs[0].hand.enMain.remove(choixS);
					choixS=100;
				}
			
		
			if(choixS<100) {	
			String[] temp=tabCompa(joueurs[0].hand.enMain.get(choixS));
			System.out.println("Veuillez rentrez la coordonnee du plateau ou vous voulez placer la piece : (Voici un modele:'H1')");
			Scanner scann=new Scanner(System.in);
			String pos=scann.nextLine();
			
			while (pos.length()!=2 && pos.length()!=3) {
				System.out.println("Recommencez:");
				pos=scann.nextLine();
			}
			String posY="";
			String posX="";
			if(pos.length()==2) {
			char poY=pos.charAt(0);
			char poX=pos.charAt(1);
			posY=Character.toString(poY);
			posX=Character.toString(poX);
			}
			if (pos.length()==3) {
			char poY=pos.charAt(0);
			char poX=pos.charAt(1);
			char poX1=pos.charAt(2);
			posY=Character.toString(poY);
			posX=Character.toString(poX)+Character.toString(poX1);
			}
			String[] verif=verifS(posX,posY);
			System.out.println("verif supprime");
			//if (testvoisin(verif)) {
				int nbrVoisin=nombrevoisin(verif);
				System.out.println("verife supprime");
				//if (verif2S(verif,nbrVoisin,(Chemin)joueurs[0].hand.enMain.get(choixS))) {
					System.out.println("verif2 passe");
					int[]poss=conversion(posX,posY);
					System.out.println(poss[0]);
					System.out.println(poss[1]);
					Tresor(poss[0],poss[1]);
						System.out.println("Vous remportez un tresor aleatoire!");
						joueurs[0].setGain((int)Math.round(Math.random() * 5));
					plateauSabo.plateauS[poss[0]][poss[1]]=joueurs[0].hand.enMain.get(choixS).toString();
					joueurs[0].hand.enMain.remove(choixS);
				//}else {
					//System.out.println("Votre piece ne peut etre poser ici, reessayer une autre localisation au prochain tour");
				//	joueurs[0].hand.enMain.remove((int)Math.round(Math.random() * 5));
				//}
			//}
			}	
	}
	public void Tresor(int X,int Y) {
		if (X!=1 && Y!=1) {								
								if((plateauSabo.plateauS[X-1][Y]+"   ").equals("T   ")){
									plateauSabo.plateauS[X-1][Y]="R   ";
									System.out.println("Vous remportez un tresor aleatoire!");
									joueurs[0].setGain((int)Math.round(Math.random() * 5));
									}
								if((plateauSabo.plateauS[X+1][Y]+"   ").equals("T   ")){ 
									plateauSabo.plateauS[X+1][Y]="R   ";
									System.out.println("Vous remportez un tresor aleatoire!");
									joueurs[0].setGain((int)Math.round(Math.random() * 5));
									}
								if((plateauSabo.plateauS[X][Y+1]+"   ").equals("T   ")){ 
									plateauSabo.plateauS[X][Y+1]="R   ";
									System.out.println("Vous remportez un tresor aleatoire!");
									joueurs[0].setGain((int)Math.round(Math.random() * 5));
									}
								if((plateauSabo.plateauS[X][Y-1]+"   ").equals("T   ")){ 
									plateauSabo.plateauS[X][Y+1]="R   ";
									System.out.println("Vous remportez un tresor aleatoire!");
									joueurs[0].setGain((int)Math.round(Math.random() * 5));
									}
							}
	}
	//Fonction pour les caracteristiques des pieces en debut de partie		
	public void tableaudeC() {
		afficheur(new Chemin(0,0,0,0).compatiseur());
		System.out.println("Les sabotages sont representes par les pieces Ae,At,Ao et les reperations par Re,Rt,Ro");
	}
	public void afficheur(String[][] t) {
		System.out.println("Voici le tableau des compatibilites des pieces en colonne: ");
		System.out.println("\n");
		for (int i=0;i<t[0].length;i++) { 
			System.out.print(t[0][i]+" ");
		}
		System.out.println("\n\n");
		for (int i=1;i<t.length;i++) {
			for (int j=0;j<t[i].length;j++) {
				System.out.print(t[i][j]+" ");
			}
			System.out.println("\n");
		}
	}
	//2 String en 2 int pour la position dans le tableau
	public int[] conversion(String X,String Y) {
		int[] ret=new int[2];
		ret[1]=Integer.valueOf(X);
		if (Y.equals("K")) { ret[0]=1;}
		if (Y.equals("L")) { ret[0]=2;}
		if (Y.equals("M")) { ret[0]=3;}
		if (Y.equals("N")) { ret[0]=4;}
		if (Y.equals("O")) { ret[0]=5;}
		if (Y.equals("P")) { ret[0]=6;}
		if (Y.equals("Q")) { ret[0]=7;}
		if (Y.equals("R")) { ret[0]=8;}
		if (Y.equals("S")) { ret[0]=9;}
		if (Y.equals("U")) { ret[0]=10;}
		if (Y.equals("V")) { ret[0]=11;}
		if (Y.equals("W")) { ret[0]=12;}
		if (Y.equals("Y")) { ret[0]=13;}
		return ret;
	}
	//Fonction verifiant si la piece peut se poser avec de 1 a 3 voisins 
	private boolean verif2S(String[] test,int nbr,Chemin aposer) {
		LinkedList<String> temp1=new LinkedList<String>();
		String[] temp=new String[nbr];
		Chemin[]temp2=new Chemin[nbr];
		for (int i=0;i<test.length;i++) {
			if (test[i]!=null) {
				temp1.add(test[i]);
			}
		}
		for(int i=0;i<temp1.size();i++) {
			temp[i]=temp1.get(i);
		}
		for (int i=0;i<temp.length;i++) {
			System.out.println("Temp "+i+" : "+temp[i]);
		}
		//Creation des objets chemins pour pouvoir faire les tests et regularisation
		for (int k=0;k<temp.length;k++) {
			if (temp[k].equals("A   ")) {
				temp2[k]=new Chemin(1,0,1,0);
			}
			if (temp[k].equals("B   ")) {
				temp2[k]=new Chemin(1,0,0,1);
			}
			if (temp[k].equals("C   ")) {
				temp2[k]=new Chemin(1,1,0,0);
			}
			if (temp[k].equals("D   ")) {
				temp2[k]=new Chemin(0,0,1,1);
			}
			if (temp[k].equals("E   ")) {
				temp2[k]=new Chemin(0,1,1,0);
			}
			if (temp[k].equals("F   ")) {
				temp2[k]=new Chemin(0,1,0,1);
			}
			if (temp[k].equals("G   ")) {
				temp2[k]=new Chemin(1,1,1,0);
			}
			if (temp[k].equals("H   ")) {
				temp2[k]=new Chemin(1,0,1,1);
			}
			if (temp[k].equals("I   ")) {
				temp2[k]=new Chemin(1,1,0,1);
			}
			if (temp[k].equals("J   ")) {
				temp2[k]=new Chemin(0,1,1,1);
			}
			if (temp[k].equals("X   ")) {
				temp2[k]=new Chemin(1,1,1,1);
			}
		}
		//--------------------------------------------------------------------------
		
		if (nbr==1) {
			if (temp2[0].estCompatible(aposer)) { 
				temp2[0].regularisation(aposer);
				return true;
			}
			return false;
		}
		if(nbr==2) {
			//En partie faux si compatible sur le meme cote
			if (temp2[0].estCompatible(aposer)) { 
				temp2[0].regularisation(aposer);
				if(temp2[1].estCompatible(aposer)) {
					return true;
				}
				return false;
		}
			return false;
		}
		if(nbr==3) {
			//Solution non trouvee
			return false;
		}
			return false;
		}


//Renvois un tableau de String avec tous les voisins de la pos on veut poser la piece
	public String[] verifS(String X,String Y) {
		int [] temp=conversion(X,Y);
		String[] ret;
		System.out.println(temp[0]);
		System.out.println(temp[1]);
		
						if (temp[1]!=1 && temp[0]!=1){ 
							ret=new String[4];
							ret[0]=plateauSabo.plateauS[temp[0]-1][temp[1]]+"   ";
							ret[1]=plateauSabo.plateauS[temp[0]+1][temp[1]]+"   ";
						    ret[2]=plateauSabo.plateauS[temp[0]][temp[1]+1]+"   ";
						    ret[3]=plateauSabo.plateauS[temp[0]][temp[1]-1]+"   ";
						    return ret;
						}
						if (temp[0]==1) { 
							ret=new String[3];
							ret[0]=plateauSabo.plateauS[temp[0]][temp[1]-1]+"   ";
							ret[1]=plateauSabo.plateauS[temp[0]][temp[1]+1]+"   ";
						    ret[2]=plateauSabo.plateauS[temp[0]+1][temp[1]]+"   ";
						    return ret;
							}
						if (temp[1]==1) {
							ret=new String[3];
							ret[0]=plateauSabo.plateauS[temp[0]-1][temp[1]]+"   ";
							ret[1]=plateauSabo.plateauS[temp[0]][temp[1]+1]+"   ";
						    ret[2]=plateauSabo.plateauS[temp[0]+1][temp[1]]+"   ";
						    return ret;
							}
						if (temp[1]==1 && temp[0]==1) { 
							ret=new String[2];
							ret[0]=plateauSabo.plateauS[temp[0]+1][temp[1]]+"   ";
							ret[1]=plateauSabo.plateauS[temp[0]][temp[1]+1]+"   ";
							return ret;
							}

					return new String[0];
				}
//trouve quelle reparation pour un sabotage donne
	private Domin trouveRep(Action rest) {
		for (int i=0;i<joueurs[0].hand.enMain.size();i++) {
			if (SaboRep(rest, joueurs[0].hand.enMain.get(i))) { 
				return joueurs[0].hand.enMain.get(i);
			}
	}
		return null;
	}

	public String[] tabCompa(Domin test) {
		if (test instanceof Chemin) {
			Chemin temp=(Chemin)test;
			return temp.tabCompa(temp);
		}
		return new String[0];
	}

//regarde si possede la reparation pour un sabotage donne
	private boolean presenceRep(ArrayList<Action> rest) {
		if (rest.size()==1) {
			for (int i=0;i<joueurs[0].hand.enMain.size();i++) {
				if (SaboRep(rest.get(0), joueurs[0].hand.enMain.get(i))) { 
					return true;
				}
			}
		}
		if (rest.size()==2) {
			for (int i=0;i<joueurs[0].hand.enMain.size();i++) {
				if (SaboRep(rest.get(0), joueurs[0].hand.enMain.get(i))) { 
					for (int j=0;j<joueurs[0].hand.enMain.size();j++) {
						if (SaboRep(rest.get(1), joueurs[0].hand.enMain.get(i))) {
							return true;
						}
					}
				}
			}
		}
		if (rest.size()==3) {
			for (int i=0;i<joueurs[0].hand.enMain.size();i++) {
				if (SaboRep(rest.get(0), joueurs[0].hand.enMain.get(i))) { 
					for (int j=0;j<joueurs[0].hand.enMain.size();j++) {
						if (SaboRep(rest.get(1), joueurs[0].hand.enMain.get(i))) {
							for (int w=0;w<joueurs[0].hand.enMain.size();w++) {
								if (SaboRep(rest.get(2), joueurs[0].hand.enMain.get(i))) {
									return true;
								}
						}
					}
				}
			}
		}
		}
		return false;
	}
	//Trouve si le couple Sabotage/reparation est le bon
	public boolean SaboRep(Action sabotage, Domin piece) {
		if (sabotage.gauche>0 && sabotage.gauche<4 && (piece instanceof Action)) {
			if (sabotage.gauche==1) {
				if (piece.gauche==4) { return true;}
			}
			if (sabotage.gauche ==2) {
				if (piece.gauche==5) { return true;}
			}
			if (sabotage.gauche==3) {
				if (piece.gauche==6) { return true;}
			}
			return false;	
			}
		return false;
		}
	


	
	public boolean presenceSabo() {
    	return joueurs[0].Restriction2();
    }
    
    

    //CLASSE INTERNE PLATEAU DE JEU
    public class SuiteDominos{
    public LinkedList<Domin> plateau;
	public SuiteDominos(){plateau= new LinkedList<Domin>();}
    public SuiteDominos(Domin premier){
        plateau=new LinkedList<Domin>();
        plateau.add(premier);
    }
    public Domin getPremier(){
        return plateau.getFirst();
    }
    public Domin getDernier(){
        return plateau.getLast();
    }

    public void AjoutDebut(Domin ajout){
        if (plateau.getFirst().estCompatiblePremier(ajout)){
            plateau.getFirst().regularisationDebut(ajout);
            plateau.addFirst(ajout);
        }
    }
    public void AjoutFin(Domin ajout){
        if (plateau.getLast().estCompatibleDernier(ajout)){
            plateau.getLast().regularisationFin(ajout);
        plateau.addLast(ajout);
        }
    }
    public String toString(){
        String s="";
        for (int i=0;i<plateau.size();i++){
            s=s+" "+plateau.get(i).toString();
        }
        return s;
    }
            
            
    
}
    //Classe interne plateau de puzzle

	public class Plat {
	public Domin[]plateau;
	public Plat() {
		plateau=new Domin[9];
		for (int i=0;i<9;i++) {
			plateau[i]=new Puzzle(i+1,i+1,i+1,i+1);
		}
	}
		public String toString() {
			String ret=" ___  ___  ___\n";
			ret=ret+"| "+plateau[0].couleurgauche+" || "+plateau[1].couleurgauche+" || "+plateau[2].couleurgauche+" |\n";
			ret=ret+"|"+plateau[0].gauche+" "+plateau[0].droite+"|"+"|"+plateau[1].gauche+" "+plateau[1].droite+"|"+"|"+plateau[2].gauche+" "+plateau[2].droite+"|\n";
			ret=ret+"| "+plateau[0].couleurdroite+" || "+plateau[1].couleurdroite+" || "+plateau[2].couleurdroite+" |\n";
			ret=ret+" ___  ___  ___\n";
			ret=ret+" ___  ___  ___\n";
			ret=ret+"| "+plateau[3].couleurgauche+" || "+plateau[4].couleurgauche+" || "+plateau[5].couleurgauche+" |\n";
			ret=ret+"|"+plateau[3].gauche+" "+plateau[3].droite+"|"+"|"+plateau[4].gauche+" "+plateau[4].droite+"|"+"|"+plateau[5].gauche+" "+plateau[5].droite+"|\n";
			ret=ret+"| "+plateau[3].couleurdroite+" || "+plateau[4].couleurdroite+" || "+plateau[5].couleurdroite+" |\n";
			ret=ret+" ___  ___  ___\n";
			ret=ret+" ___  ___  ___\n";
			ret=ret+"| "+plateau[6].couleurgauche+" || "+plateau[7].couleurgauche+" || "+plateau[8].couleurgauche+" |\n";
			ret=ret+"|"+plateau[6].gauche+" "+plateau[6].droite+"|"+"|"+plateau[7].gauche+" "+plateau[7].droite+"|"+"|"+plateau[8].gauche+" "+plateau[8].droite+"|\n";
			ret=ret+"| "+plateau[6].couleurdroite+" || "+plateau[7].couleurdroite+" || "+plateau[8].couleurdroite+" |\n";
			ret=ret+" ___  ___  ___\n";
			return ret;
			
		}
	}
	//Classe interne Saboteur
	public class PlatS extends Shuffle{
		public String[][] plateauS;
		public PlatS() {
			plateauS=new String[14][14];
			plateauS[0][0]="0   ";
			plateauS[7][3]=new Chemin(1,1,1,1).toString();
			plateauS[5][11]=new Arrivee().toString();
			plateauS[7][11]=new Arrivee().toString();
			plateauS[9][11]=new Arrivee().toString();
			for (int i=1;i<14;i++) {
				String z="";
				if(i==1) {z="K:  ";}
				if(i==2) {z="L:  ";}
				if(i==3) {z="M:  ";}
				if(i==4) {z="N:  ";}
				if(i==5) {z="O:  ";}
				if(i==6) {z="P:  ";}
				if(i==7) {z="Q:  ";}
				if(i==8) {z="R:  ";}
				if(i==9) {z="S:  ";}
				if(i==10) {z="U:  ";}
				if(i==11) {z="V:  ";}
				if(i==12) {z="W:  ";}
				if(i==13) {z="Y:  ";}
			plateauS[0][i]=String.valueOf(i)+"   ";
			plateauS[i][0]=z;
			}
		}
		public String toString() {
			String ret="";
			for (int i=0;i<13;i++) {
				for (int j=0;j<14;j++) {
					ret=ret+"  "+plateauS[i][j];
				}
				ret=ret+"\n";
			}
			return ret;
		}
		
	}
	
	
    //Classe interne pour melanger le jeu dominos
    static class Shuffle{
        public static void shuffleArray(int[] array){
        int index;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            if (index != i)
            {
                array[index] ^= array[i];
                array[i] ^= array[index];
                array[index] ^= array[i];
            }
        }
    }
    }
}
