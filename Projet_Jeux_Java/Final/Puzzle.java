public class Puzzle extends Domin{
	//Le Puzzle 3x3 est une arraylist de telle sorte que si l'on a le puzzle
	// 1  2  3
	// 4  5  6
	// 7  8  9
	//Les pieces seront stockees de telle sorte que a l'indice 0 de l'array list on aura la piece 1 , a l'indice 3 on aura 4 et a l'indice 8 on aura 9
	public Puzzle(int gauche,int droit,int haut, int bas) {
		super(gauche,droit);
		couleurgauche=haut;
		couleurdroite=bas;
	}
	public String toString() {
	return "|  "+couleurgauche+"  |\n|"+gauche+"   "+droite+"|\n|  "+couleurdroite+"  |";
	}
	
	@Override
	boolean estCompatiblePremier(Domin test) {
		
		return false;
	}

	@Override
	boolean estCompatibleDernier(Domin test) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	void regularisationDebut(Domin test) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void regularisationFin(Domin test) {
		// TODO Auto-generated method stub
		
	}
	
}
