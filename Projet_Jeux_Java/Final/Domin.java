
abstract class Domin {
	public int gauche;
	public int droite;
	public int couleurgauche;
	public int couleurdroite;
	public Domin() {}
	public Domin(int g, int d) {
		gauche=g;
		droite=d;
	}
	
	public int getGauche() {
		return gauche;
		
	}
	public int getDroite() {
		return droite;
	}
	abstract boolean estCompatiblePremier(Domin test);
	abstract boolean estCompatibleDernier(Domin test);
	abstract void regularisationDebut(Domin test);
	abstract void regularisationFin(Domin test);


}
