import java.awt.*;
import javax.swing.*;
import javax.swing.JSlider.*;
import java.awt.event.*;
import java.util.LinkedList;

public class Vieww extends JFrame implements ActionListener{
    
    private Dom Plateau;
    private JPanel Actions;
    private Container P;
    private JButton[]TB;
    private int h;
    private int w;
    private Jeu Jeu;
    private Domin[][]ttmains;    //tableau possedant les mains des joueurs
    private int nb;             // nombre de joueurs
    private int bouton;         //valeur du bouton actionné
    private LinkedList<Domin> plateau; //liste de dominos a afficher
    private boolean action;    //informe si l'on actionne un bouton

    public int getBouton(){ 
	return bouton;
    }

    public boolean getAction(){
	return action;
    }
    public void setActionB(){ // 
	action=false;
    }
    
    public Vieww(int nbJoueurs,Jeu j){
	Jeu=j;
	action=false;
	nb=nbJoueurs;
	ttmains=Jeu.getTtmains();
	plateau=Jeu.plateaudejeu.plateau;
	setTitle("Domino");
	Plateau= setPlateau();
	Actions= setActions();
	
	P=getContentPane();
	
	Rectangle r = this.getBounds();
	 h = r.height;
	 w = r.width;
	
	P.setLayout(new BorderLayout());
	P.add(Plateau,BorderLayout.CENTER);
	P.add(Actions,BorderLayout.EAST);
   
    Plateau.setBackground(new Color(0,50,5));
   
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
    setSize(800,800);
    
    }
    public void actionPerformed(ActionEvent t){}
    public JPanel setActions(){
	this.Actions= new JPanel();
	TB= new JButton[7];
	JButton Poser1= new JButton("Poser 1");
	JButton Poser2= new JButton("Poser 2");
	JButton Poser3= new JButton("Poser 4");
	JButton Poser4= new JButton("Poser 5");
	JButton Poser5= new JButton("Poser 6");
	JButton Poser6= new JButton("Poser 6");
	JButton Poser7= new JButton("Poser 7");
	
	TB[0]=Poser1;
	TB[1]=Poser2;
	TB[2]=Poser3;
	TB[3]=Poser4;
	TB[4]=Poser5;
	TB[5]=Poser6;
	TB[6]=Poser7;

	JButton Piocher= new JButton("Piocher");

	for(int k=0;k<7;k++){
	    TB[k].setBorder( new RoundedBorder(5));
	    TB[k].setForeground(Color.BLUE);
	    Actions.add(TB[k]);
	}
	TB[0].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=1;
			action=true;
		    }});
	TB[1].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=2;
			action=true;
		    }});
	TB[2].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=3;
			action=true;
		    }});
	TB[3].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=4;
			action=true;
		    }});
	TB[4].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=5;
			action=true;
		    }});
	TB[5].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=6;
			action=true;
		    }});
	TB[6].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=7;
			action=true;
		    }});
	Piocher.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent args0){
			bouton=0;
			action=true;
		    }});

	
	Piocher.setPreferredSize(new Dimension(100,200));
	Piocher.setBorder( new RoundedBorder(5));
	Piocher.setForeground(Color.RED);

	
	Actions.add(Piocher);
	Actions.setLayout(new GridLayout(8,1));

	return this.Actions;
    }

    public Dom setPlateau(){
	return new Dom(ttmains,plateau,nb,this.Jeu.getJoueurs());
    }
    public void refresh(){ 

	Dom np = new Dom(ttmains,Jeu.plateaudejeu.plateau,nb,this.Jeu.getJoueurs());
	np.setBackground(new Color(0,50,5));
	P.remove(Plateau);
	P.add(np);
      
	validate();
    }
	    

    public int getH(){
	return h;
    }

    public int getW(){
	return w;
    }
     public Domin[][]getTtmains(){
	return ttmains;
    }
    public int getNb(){
	return nb;
    }

    
	
}
