import java.util.Scanner;

public class InterDeJeu{
    public boolean enJeu=false;
	private Scanner sc;
    @SuppressWarnings("resource")
	public InterDeJeu(){
            sc = new Scanner(System.in);
            int choix;
            while(!enJeu){
            	System.out.println("Bienvenue dans l'interface des jeux Dominos \n Voici la liste des jeux, entrez le numero associe au jeu pour y jouer :");
                System.out.println(1+" : Dominos");
                System.out.println(2+" : Dominos gommettes");
                System.out.println(3+" : Puzzle");
                System.out.println(4+" : Saboteur");
                sc = new Scanner(System.in);
            	choix=sc.nextInt();
            	while (!(choix>0 && choix<5)){
                    System.out.println("Numero incorrect, rentrez un numero correct s'il vous plait");
                    choix=sc.nextInt();
                }
                new Jeu(choix);
            }
    }       
}
