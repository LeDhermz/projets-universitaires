#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <time.h>
#include "projet2019.h"
#include "projet2019.c"
#include "listes_generiques.c"

#define TailleMemoire  100000
#define SeuilCompact 10000 // nombre de cases libres inutiles entrainant une compaction au lieu d'un ajout de mémoire
#define SRAND 20 
#define RATIO 5 //modulo de supression
#define NB_OPS 500000









int main(){

  int ratio = RATIO;
  int k, oper, pos, val, j, taille_liste;
   
 

  
  clock_t startProjet = clock();
  
  srand(SRAND);
  void * aux;
  taille_liste = 0;
  void * listeP = ld_create(TailleMemoire);
  if (listeP==NULL){
    printf("Echec du malloc initial");
    exit(0);
  }
  for (k=0; k<=NB_OPS; k++){
    oper = rand();
    pos = 0;
    if (taille_liste != 0){
      pos = (rand()%10)%taille_liste;
    }
    val = rand();

    //Suppression
    if (oper % ratio == 0){
      aux = ld_first(listeP);
      if (taille_liste != 0){
	for(int j=1;j<pos;j++){
	  aux = ld_next(listeP, aux);
	}
	ld_delete_node(listeP, aux);
      taille_liste -=1;
      }
    }



    //insertion
    else{
      if (taille_liste == 0 || pos == 0){
	ld_insert_first(listeP, sizeof(val), &val);
      }
      else{
	aux = ld_first(listeP);
	for(j=1;j<pos;j++){
	  aux = ld_next(listeP, aux);
	}
	if(ld_insert_after(listeP, aux, sizeof(val), &val) == NULL){
	  if ((ld_total_free_memory(listeP)- ld_total_useful_memory(listeP)) >= SeuilCompact){
	    listeP = ld_compactify(listeP);
	  }
	  else{
	    ld_add_memory(listeP, TailleMemoire);
	  }
	  ld_insert_after(listeP, aux, sizeof(val), &val);
	}
	taille_liste+=1;
      }
    }
  }
  ld_destroy(listeP);
  clock_t endProjet = clock();
  printf("Le temps d'execution de l'implementation du projet est de %zu \n",(endProjet - startProjet));




  /*


  clock_t startClassique = clock();
  
  srand(SRAND);
  taille_liste = 0;
  liste auxC;
  liste listeClassique = creer_liste();
  if (listeClassique == NULL){
    printf("Echec  malloc");
    exit(1);
  }
  for (k=0; k<=NB_OPS; k++){
    oper = rand();
    pos = 0;
    if (taille_liste != 0){
      pos = (rand()%10)%taille_liste;
    }
    val = rand();

    //Suppression
    if (oper % ratio == 0){
      auxC = listeClassique;
      if (!est_vide(listeClassique)){
	for(int j=1;j<pos;j++){
	  auxC = suivant(auxC);
	}
	supprimer_element(auxC);
      taille_liste -=1;
      }
    }
  

    //insertion
    else{
      if (taille_liste == 0 || pos == 0){
	inserer_apres(listeClassique, &val);
      }
      else{
	auxC = suivant(listeClassique);
	for(j=1;j<pos;j++){
	  auxC = suivant(auxC);
	}
	inserer_apres(auxC, &val);
	}
	taille_liste+=1;
    }
    
  }
  detruire_liste(listeClassique);



  
  clock_t endClassique = clock();
  printf("Le temps d'execution de l'implementation classique est de %zu secondes \n",(endClassique - startClassique));

  */
  return (0);


  
}

