# Projet Chaines Génériques doublement chainées 2019
============================================

Ce Projet consiste à

Le fichier projet2019.c contient toutes les fonctions et structures demandées dans le sujet du projet.

Le fichier main.c contient l'implémentation de test d'insertion et de suppression répétés. Les valeurs definies aux debut permettent de moduler les tests.

Le fichier listes_generiques.c contient votre implémentation de liste doublement chainées pour pouvoir comparer l'efficacité d'un malloc unique.

Le fichier testProjet2019.c contient quand à lui differentes fonctions permettant le test des differentes donctions demandées. L'affichage textuel dans le terminal risque cependant de vous paraître abscond mais il sera plus clair lorsque nous nous appuyerons dessus lors de la présentation.

Nous vous souhaitons une bonne année.
Bien à vous,

