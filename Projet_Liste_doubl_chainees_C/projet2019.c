#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "projet2019.h"


typedef union{
  intmax_t a;
  void *adr;
  long double c;
}align_data;

typedef struct node{
  ptrdiff_t next;
  ptrdiff_t previous;
  size_t len;
  align_data data[];
}node;
  
typedef struct entete_tranche{
  ptrdiff_t suivant;
  size_t nb_blocs;
} entete_tranche;



typedef struct head{
  void* memory; //pointeur vers la memoire
  ptrdiff_t first; //ptrdiff_t
  ptrdiff_t last; //ptrdiff_t
  entete_tranche * libre; //ptrdiff_t ou pointeur si la liste de tranches
  size_t len;
  void * ajout;
} head;



static size_t nb_blocs(size_t o){
  int acc=0;
  while ( o > acc * sizeof(align_data)){
    acc++;
  }
  return acc;
}




//creation de base
void* ld_create( size_t nboctets ){
  int aux = nb_blocs(nboctets);
  int k = nb_blocs(sizeof(head));
  void *temp = malloc ( sizeof(align_data[aux+k]));
  if (temp==NULL){
    return NULL;
  }
  entete_tranche t = {.suivant = 0 , .nb_blocs = aux};
  head h= {(void*)((align_data *)temp + k), 0, 0, (entete_tranche *)(void*)((align_data*)temp + k), (size_t)aux,NULL};
  entete_tranche *n =(entete_tranche *) (void *)((align_data *)temp + k);
  *n = t;
  head *m = temp;
  *m = h;
  return temp;
}



//fonctions de deplacement
void* ld_first (void*liste){
  if (liste==NULL){
    return NULL;
  }
  head * h = (head *)liste ;
  if (h->first==0){
    return NULL;
  }else{
    return (void *)((align_data*)liste+(h->first));;
  }
}

void* ld_last(void*liste){
  if (liste==NULL){
    return NULL;
  }
  head * h = (head *)liste ;
  if (h->last==0){
    return NULL;
  }else{
    return (void *)((align_data*)liste+(h->last));
  }
}

void* ld_next(void*liste,void*current){
  if (liste==NULL || current==NULL){
    return NULL;
  }
  if (liste==current) {
    return ld_first(liste);
  }
  if (ld_last(liste) == current){
    return NULL;
  }
  node *n = (node *)current;
  ptrdiff_t k = (n->next);
  return (void *)((align_data *) n + k);
}

void* ld_previous(void*liste,void*current){
 if (liste==NULL || current==NULL){
    return NULL;
  }
  if (liste==current) {
    return ld_last(liste);
  }
  if (ld_first(liste) == current){
    return NULL;
  }
  node *n = (node *)current;
  ptrdiff_t k = (n->previous);
  return (void *)((align_data *) n + k);
}



//destroy
void ld_destroy(void*liste){
  head * h = (head*) liste;
  if ((h->ajout)!=NULL)
    free(h->ajout);
  free(liste);
}



//copie
size_t ld_get(void*liste,void*current, size_t len,void*val){
  if (liste == NULL || current == NULL){
    return 0;
  }
  node * aux = (node *)current;
  int k= ((aux->len) - nb_blocs(sizeof(node))) * (int)sizeof(align_data);
  if ((int)len > k) {
    len=k;
  }
  memmove(val, ((node*)current)->data, len);
  return len; 
}


//Fonctions d'insertion
static void* place_node(void*liste, size_t len){
  size_t aux = nb_blocs(len);
  aux += nb_blocs(sizeof(node)); 
  //printf("debut place_node \n liste = %p `n len = %zu \n ", liste, aux);
  head *h = (head *) liste;
  entete_tranche * t1=(h-> libre);
  entete_tranche * t2=(h-> libre);
  size_t temp;
  if ((t1->nb_blocs) >= aux){
    temp = t2->nb_blocs - aux;
    if (temp >= nb_blocs(sizeof(entete_tranche))){
      int next=0;
      if ((t1->suivant)!=0){
	next = (t1->suivant) - aux;
      }
      entete_tranche x = {next, (t1->nb_blocs) - aux};
      (h->libre)=(entete_tranche *)(void *)((align_data*)(void*)(h->libre)+aux);
      entete_tranche *k =(entete_tranche*)(void*)((align_data*)(void *)t1 + aux);
      *k=x;
    }
    else{
      (h->libre)=(entete_tranche *)(void *)((align_data*)(void*)(h->libre)+(t1->suivant));
    }
    return (void *) t1;
  }
  //printf("\n placenode \n len = %zu \n aux = %zu \n nbblocs =%zu \n", len, aux, t2->nb_blocs); 
  while(t2->nb_blocs < aux){
    if (t2->suivant==0){
      return NULL;
    }
    t1=t2;
    t2 =(entete_tranche *)(void *)((align_data*)(void*)t2 + (t2->suivant));;
  }
  temp = t2->nb_blocs - aux;
  if (temp >= nb_blocs(sizeof(entete_tranche))){
    entete_tranche x2 = {.suivant = (t2->suivant) - aux, .nb_blocs= (t2->nb_blocs) - aux};
    (t1->suivant)+=aux;
    entete_tranche *k2 = (void*)((align_data*)(void *)t2 + aux);
    *k2=x2;
  }
  else{
    (t1->suivant)=t2->suivant ;
  }
  return (void*)t2;
}


void* ld_insert_first(void*liste, size_t len,void*p_data){
   
  head *h = (head *) liste;
  size_t aux = nb_blocs(len);
  aux += nb_blocs(sizeof(node));
  if ( (h->first)== 0){
    void *place = place_node( liste, len);
    if (place==NULL){
      return NULL;
    } 
    (h->first)= (place - liste)/sizeof(align_data);
    (h->last) = (h->first);
    node k ={.previous =0 , .next=0, .len=aux};
    node *noeud = (node*)place;
    *noeud = k;
    memmove(((node*)place)->data, p_data, len);
    return place;
  }
  return ld_insert_before(liste, (void*)((align_data *)liste+ (h->first)), len, p_data);
}
							  



void* ld_insert_last(void*liste, size_t len,void*p_data){
  head *h = (head *) liste;
  size_t aux = nb_blocs(len);
  aux += nb_blocs(sizeof(node));
  if ( (h->first)== 0){
    void *place = place_node( liste, len);
    if (place==NULL){
      return NULL;
    }
    (h->first)= (place - liste)/sizeof(align_data);
    (h->last) = (h->first);
    node k ={.previous =0 , .next=0, .len=aux};
    node *noeud = (node*)place;
    *noeud = k;
    memmove((noeud->data), p_data, len);
    return place;
  }
  return ld_insert_after(liste, (void*)((align_data *)liste+ (h->last)), len, p_data);
}

// si after est en derniere position a gerer
void* ld_insert_after(void*liste,void*current, size_t len,void*p_data){
  head *h = (head *) liste;
  size_t aux = nb_blocs(len);
  aux += nb_blocs(sizeof(node));
  if (h->first == 0 ) {
    return ld_insert_last(liste, len, p_data);
  }
  void *place = place_node( liste, len);
  if (place==NULL){
    return NULL;
  }
  node * pre = (node *)current;
  node k ={.previous =0 , .next=0, .len=aux};
  node *noeud = (node*)place;
  *noeud = k;
  memmove((noeud->data), p_data, len);
  if ((pre->next)==0){
    (noeud->next) = 0;
    (noeud->previous)=((char*)pre - (char*)noeud)/(ptrdiff_t)(int)sizeof(align_data);
    (pre->next)=(-1)*(noeud->previous);
    (h->last)+=(pre->next);
  }
  else{
    node * sui = (node *)(void *)((align_data *)current + (pre->next));
    (noeud->next)=((char*)sui - (char*)noeud)/(ptrdiff_t)(int)sizeof(align_data);
    (noeud->previous)=((char*)pre - (char*)noeud)/(ptrdiff_t)(int)sizeof(align_data);
    (pre->next)=(-1)*(noeud->previous);
    (sui->previous)=(-1)*(noeud->next);
  }
  return (void *)noeud;
}

void* ld_insert_before(void*liste,void*current, size_t len,void*p_data){
  head *h = (head *) liste;
  size_t aux = nb_blocs(len);
  aux += nb_blocs(sizeof(node));
  if (h->first == 0) {
    return ld_insert_first(liste, len, p_data);
  }
  node * sui = (node *)current;
  void *place = place_node( liste, len);
  if (place==NULL){
    return NULL;
  }
  node k ={.previous =0 , .next=0, .len=aux};
  node *noeud = (node*)place;
  *noeud = k;
  memmove((noeud->data), p_data, len);
  if((sui->previous)==0){
    (noeud->previous)=0;
    (noeud->next)=((char*)sui-(char*)noeud)/(ptrdiff_t)(int)sizeof(align_data);
    (sui->previous)=(-1)*(noeud->next);
    (h->first)+=(sui->previous);
  }
  else{
    
    node * pre = (node *)(void *)((align_data *)current + (sui->previous));
    (noeud->previous)=((char*)pre - (char*)noeud)/(ptrdiff_t)(int)sizeof(align_data);
    (noeud->next)=((char *)sui - (char *)noeud)/(ptrdiff_t)(int)sizeof(align_data);

    (pre->next)=(-1)*(noeud->previous);
    (sui->previous)=(-1)*(noeud->next);
    
  }
  return (void *)noeud;
}



//suppression
void* ld_delete_node(void*liste,void*n){
  head *h = (head *)liste;
  node * current = (node*) n;
  void * temp = ld_next(liste,n);
  void * temp2 = ld_previous(liste, n);
  if (temp != NULL){
    node * aux = (node*)temp;
    if (temp2 == NULL){
      (h->first)+=(current->next);
      (aux->previous)=0;
    }
    else{
      node * aux2 =(node*) temp2;
      (aux2->next)+=(current->next);
      (aux->previous)+=(current->previous);
    }
  }
  else{
    if (temp2 == NULL){
      (h->first)=0;
      (h->last)=0;
    }
    else{
      node * pre =(node*) temp2;
      (pre->next)=0;
      (h->last)+=(current->previous);
    }
    
  }
  entete_tranche t= {.suivant = 0,.nb_blocs = (current->len)};
  entete_tranche * var= (entete_tranche *)n;
  *var=t;
  entete_tranche * tranche = (h->libre);
  while ((tranche->suivant)!=0){
    tranche=(entete_tranche*)(void*)((align_data*)(void*)tranche + (tranche->suivant));
  }
  (tranche->suivant) = ((char*)var - (char*)tranche)/(ptrdiff_t)(int)sizeof(align_data);
  return liste;
}



  
  
    


//gestion de la mémoire
size_t  ld_total_free_memory(void*liste){
  head *h= (head*)liste;
  size_t acc= (h->len);
  node * n =(node*) ld_first(liste);
  if (n==NULL){
    goto out;
  }
  while ((n->next)!=0){
    acc-= (n->len);
    n=(node*) ld_next(liste, (void*)n);
  }
  acc-=(n->len);
 out:
  return acc;
}
      

  
size_t  ld_total_useful_memory(void*liste){
  head *h= (head*)liste;
  entete_tranche *t = (h->libre);
  size_t acc = 0;
  while ((t->suivant) != 0){
    if (t->nb_blocs>sizeof(node))
      acc+= (t->nb_blocs);
    t= (entete_tranche *)(void *)((align_data *)(void *)t+(t->suivant));
  }
  if (t->nb_blocs>sizeof(node))
      acc+= (t->nb_blocs);
  return acc;
}

  
 void*ld_add_memory(void*liste, size_t nboctets){
   size_t aux = nb_blocs(nboctets);
   void * temp = malloc(sizeof(align_data[aux]));
   if (temp==NULL){
     return NULL;
   }
   head *h= (head *) liste;
   h->ajout=temp;
   (h->len) += aux; 
   entete_tranche t={0, aux};
   entete_tranche * tra =(entete_tranche *) temp;
   tra= &t;
   entete_tranche * tranche = (h->libre);
   while ((tranche->suivant)!=0){
     tranche=(entete_tranche*)(void*)((align_data*)(void*)tranche + (tranche->suivant));
   }
   (tranche->suivant)=tra - tranche;
   return liste;   
 }

 
 void*ld_compactify(void*liste){
   head * h1= (head *)liste;
   void * new = ld_create((h1->len)*sizeof(align_data));
   if (new==NULL){
     return NULL;
   }
   node * n1 = ld_first(liste);
   while ((n1->next)!=0){
     ld_insert_last(new,(n1->len)-nb_blocs(sizeof(node)),(void*)(n1->data));
     n1= ld_next(liste, (void*)n1);
   }
   ld_insert_last(new,(n1->len)-nb_blocs(sizeof(node)),(void*)(n1->data));
   ld_destroy(liste);
   return new;
     
 }

