#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <time.h>
#include "projet2019.c"



void create_insertfirst(){
  printf("test ld_create \n\n");
  void * liste=ld_create(1000);
  if(liste == NULL){
    printf("Echec Malloc");
    exit(1);
  }
  head * h = (head *)liste;
  entete_tranche * t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));

  printf("\n\n\n test ld_insert_first \n\n");
  int n=6;
  printf("Sur une liste vide\n");
  void * temp = ld_insert_first(liste, sizeof(n), &n);
  node * noeud = (node*) temp;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len)); 
  printf("Sur une liste avec deja au moins un noeud\n");
  void * temp2 = ld_insert_first(liste, sizeof(n), &n);
  node * noeud2 = (node*) temp2;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));
  printf("noeud précédent \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));
  int k = 100;
  printf("%zu \n",ld_get(liste, temp2, sizeof(int), &k));
  printf("k= %i",k);
  ld_destroy(liste);
  
}






void create_insertlast(){
  printf("test ld_create \n\n");
  void * liste=ld_create(1000);
  if(liste == NULL){
    printf("Echec Malloc");
    exit(1);
  }
  head * h = (head *)liste;
  entete_tranche * t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));

  printf("\n\n\n test ld_insert_last \n\n");
  int n=5;
  printf("Sur une liste vide\n");
  void * temp = ld_insert_last(liste, sizeof(n), &n);
  node * noeud = (node*) temp;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len)); 
  printf("Sur une liste avec deja au moins un noeud\n");
  void * temp2 = ld_insert_last(liste, sizeof(n), &n);
  node * noeud2 = (node*) temp2;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));
  printf("noeud précédent \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));
  ld_destroy(liste);
  
}


void create_insertbefore(){
  printf("test ld_create \n\n");
  void * liste=ld_create(1000);
  if(liste == NULL){
    printf("Echec Malloc");
    exit(1);
  }
  head * h = (head *)liste;
  entete_tranche * t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));

  printf("\n\n\n test ld_insert_before \n\n Si insertion avant le premier element, alors il 'agit de la meme branche qu'un insert first sur une liste non vide donc deja vérifié. \n\n");
  int n=5;
  printf("Sur une liste vide\n");
  void * temp = ld_insert_before(liste,NULL, sizeof(n), &n);
  node * noeud = (node*) temp;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len)); 
  printf("Sur une liste avec deja au moins un noeud\n");
  void * temp2 = ld_insert_before(liste, temp, sizeof(n), &n);
  node * noeud2 = (node*) temp2;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));
  printf("noeud après \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));

  void * temp3 = ld_insert_before(liste, temp, sizeof(n), &n);
  node * noeud3 = (node*) temp3;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud3->next),(noeud3->previous),(noeud3->len));
  printf("noeud avant \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));
  printf("noeud après \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));

  ld_destroy(liste);
  
}



void create_insertafter(){
  printf("test ld_create \n\n");
  void * liste=ld_create(1000);
  if(liste == NULL){
    printf("Echec Malloc");
    exit(1);
  }
  head * h = (head *)liste;
  entete_tranche * t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));

  printf("\n\n\n test ld_insert_after \n\n\n");
  int n=5;
  printf("Sur une liste vide\n");
  void * temp = ld_insert_after(liste,NULL, sizeof(n), &n);
  node * noeud = (node*) temp;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len)); 
  printf("Sur une liste avec deja au moins un noeud\n");
  void * temp2 = ld_insert_after(liste, temp, sizeof(n), &n);
  node * noeud2 = (node*) temp2;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));
  printf("noeud précédent \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));

  void * temp3 = ld_insert_after(liste, temp, sizeof(n), &n);
  node * noeud3 = (node*) temp3;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud inséré \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud3->next),(noeud3->previous),(noeud3->len));
  printf("noeud après \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));
  printf("noeud avant \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));

  ld_destroy(liste);
  
}


// montrer le bon fonctionnement d'entete_tranche
void delete_after(){
  void * liste=ld_create(1000);
  if(liste == NULL){
    printf("Echec Malloc");
    exit(1);
  }
  head * h = (head *)liste;
  entete_tranche * t = (h->libre);
  int n=5;
  void * temp = ld_insert_after(liste,NULL, sizeof(n), &n);
  node * noeud = (node*) temp;
  t = (h->libre);
  void * temp2 = ld_insert_after(liste, temp, sizeof(n), &n);
  node * noeud2 = (node*) temp2;
  t = (h->libre);
  void * temp3 = ld_insert_after(liste, temp, sizeof(n), &n);
  node * noeud3 = (node*) temp3;
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud 1: \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));
  printf("noeud 2: \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud3->next),(noeud3->previous),(noeud3->len));
  printf("noeud 3: \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));

  printf("Delete sur noeud central\n");
  ld_delete_node(liste, temp3);
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud 1: \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud->next),(noeud->previous),(noeud->len));
  printf("noeud 3: \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));

  printf("Delete sur extremite de liste\n");
  ld_delete_node(liste, temp);
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud 3: \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud2->next),(noeud2->previous),(noeud2->len));

  printf("Delete sur noeud unique\n");
  ld_delete_node(liste, temp2);
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));


  
  ld_destroy(liste);
  
}

void compact(){
  void * liste=ld_create(1000);
  if(liste == NULL){
    printf("Echec Malloc");
    exit(1);
  }
  head * h = (head *)liste;
  entete_tranche * t = (h->libre);
  int n=5;
  void * temp = ld_insert_after(liste,NULL, sizeof(n), &n);
  t = (h->libre);
  void * temp2 = ld_insert_after(liste, temp, sizeof(n), &n);
 
  t = (h->libre);
  void * temp3 = ld_insert_after(liste, temp, sizeof(n), &n);
  node * noeud3 = (node*) temp3;
 
  ld_delete_node(liste, temp2);
 
  ld_delete_node(liste, temp);
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("noeud 3: \n suivant = %ld \n précédent = %ld \n len = %zu \n \n",(noeud3->next),(noeud3->previous),(noeud3->len));

  liste = ld_compactify(liste);
  h = (head *)liste;
  t=(h->libre);
  printf("Apres compaction \n memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));


  
  ld_destroy(liste);
  
}




void ajout(){
  void * liste=ld_create(32);
  if(liste == NULL){
    printf("Echec Malloc");
    exit(1);
  }
  head * h = (head *)liste;
  entete_tranche * t = (h->libre);
  int n=5;
  void * temp = ld_insert_after(liste,NULL, sizeof(n), &n);
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf(" Memoire libre totale : %zu \n", ld_total_free_memory(liste));
  printf("Mémoire utile : %zu  \n", ld_total_useful_memory(liste));
  printf("adresse d'insertion   %p \n", temp);
  ld_add_memory(liste, 968);
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  temp = ld_insert_after(liste,NULL, sizeof(n), &n);
  t = (h->libre);
  printf("memory = %p \n first = %ld \n last = %ld \n libre = %p \n len = %zu \n taille de la premiere tranche = %zu\n" ,(h->memory),(h->first),(h->last),(h->libre),(h->len),(t->nb_blocs));
  printf("adresse d'insertion   %p", temp);


  
  ld_destroy(liste);
  
}










int main(){
  //ajout();
  //compact();
  create_insertfirst();
  //create_insertlast();
  //create_insertafter();
  //create_insertbefore();
  //delete_after();
  
}

