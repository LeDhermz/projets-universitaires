PROJET L3 MATH-INFO 2020
============================================

**Etude et Modélisation de mélanges de cartes**

Ceci est le readme relatif à un code qui modélise et analyse l'efficacité de différents mélange de cartes.



Bcp de tests dans ce fichier. Celui qui aboutit est test_riffle2, qui devient riffle_shuffle dans le fichier principal. Il y avait deux façons de simuler un riffle_shuffle, une fois le paquet 
 coupé en deux sous paquets m1 et m2:

1. Force brute : On crée  toutes les permu qui verifient que m1 et m2 sont des ss suites croissantes, puis on en tire une au hasard.
2. Simulation de melange : On injecte les cartes de m1 dans m2, avec des probabilités telles qu'in fine, toutes les permus possibles ont les même chances d'apparaître.

Ne voyant pas de moyen peu couteux de faire la 1) on s'est tournés vers la 2)
 La fonction test_riffle2 repose sur le calcul de la probabilité qu'une carte soit envoyée en k,
	sachant la position de la carte précédente. Ci-dessous, nous détaillons l'élaboration de la fonction Loi_condi
 
* Départ : Placement de la première carte (nommée '1') de m1 dans m2.
On devine qu'il faut placer '1' en fonction du nombre de permutation que l'on pourra encore atteindre une fois placée. Ce nombre dépend du nombre de cartes derrière '1' dans m1, mais aussi du nombre de positions
 encore disponibles. 

	Exemple : m1= [1,2,3] m2=[4,5,6] ; on peut mettre '1' en 0, en 1, en 2 ou en 3
			'1'-> 2 amène à m2=[4,5,1,6] et on peut encore atteindre [4,5,1,2,3,6],[4,5,1,2,6,3] et [4,5,1,6,2,3]
		Si on note P_1(i) le nb  de permus encore atteignables/Possibles en plaçant '1' en i, alors on a :
			P_1(0)= 10, P_1(1)=6, P_1(2)=3 P_1(3)=1.

On comprends dès lors que pour atteindre uniformément chaque permu, il faut qu'on place '1' en 0 10x plus souvent qu'en 3. Mais d'ou viennent ces valeurs ?
On note cbin(k,n) le coefficient binomial 'k parmi n' On constate que :
 		 P_1(0) =  cbin(1,4) + cbin(2,4) ; P_1(1) = cbin(1,3) + cbin(2,3) P_1(2) = cbin(1,2) + cbin(2,2)

On comprend cette compositon comme : Une fois '1' placé, on peut choisir de mettre les deux cartes restantes
 		'collées' en 1 groupe comme en [1,2,3,4,5,6] ou [1,4,2,3,5,6], ou bien de séparer les cartes restante en 
 		2 groupes comme en [1,2,4,5,3,6] ou [1,4,5,2,6,3].
		On doit donc sommer le nombre de façons de mettre 1 groupe, cbin(1,4), et le nb de façons de mettre 2 groupes.

## Placement des cartes suivantes
 On a placé '1' en pos. On doit encore placer '2' entre pos et la fin de m2 : il reste len(m2)-pos +1 positions
 Comme précédemment, on place '2' selon les P_2(i), qui dépendent du nombre de cartes à placer après '2', et du nombre de positions où on pourra encore les placer, ie de la position prise par '1'
 A la troisième carte, on comprend l'essentiel : connaissant le nb total de carte, il suffit de connaître la position
 prise par la carte précédente pour placer la suivante, en utilisant la formule explicitée ci dessous :

 		Sachant qu'il y n cartes dans m2, m cartes dans m1, et que la carte précédente est placé en pos, La probabilité P_c(p) que la c-ème carte de m1  aille en position p est :
			P_c(p) = Somme_des_Coeff(m-c,n-pos+1) 
Avec Somme_des_Coeff(k,n) = cbin(1,n) + cbin(2,n) +.... + cbin(k,n)
En effet m-c est le nombre de carte de m1 qu'il restera à placer, et n-pos+1 est le nb de positions restantes

## Cas remarquables :
1. La dernière carte de m1 se distribue uniformément en probabilité sur les places qui lui reste.
2. Si une carte s'est placée au bout de m2, alors il n'y a plus qu'a rajouter les autre cartes de m1 derrière elle.


 Le programme test_riffle2 est écrit selon le raisonnement précédent. Plus précisément : Le calcul de la proba conditionelle est effectué dans Loi_Condi(m-c,n-pos+1). Loi_condi est appelé lorsque l'on insère une carte. Il renvoie un entier, correspondant à "l'avancée" de la carte insérée. Si loi_condi =0, on insère juste après la carte précédente, donc en pos+1. Si loi condi = n-pos+1, on insère en fin de paquet.
