# -*- coding: utf8 -*-


from __future__ import division
import itertools as its

import random
import math
import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sp

def S_n(n) : #Renvoie le groupe symétrique n. 
    L= range(1,n+1)
    S=[]
    for s in its.permutations(L) : #its.permu renvoie des itérables
        S.append(list(s)) #il faut donc les convertir en list
    return(S)

def lcartes_create(r): # On crée un jeu de cartes triées de 1 à r
    l=range(1,r+1)
    return l

def bernoulli(p) : #On simule une loi de bernoulli
	rea=random.random()
	if (rea<p) : return 1
	else : return 0

def binomiale(n,p) : # On simule une loi binomiale
	res = 0
	for k in range(n) :
		res = res + bernoulli(p)
	return res

#print(binomiale(5,1/2))

def nb_combi(k,n) : # valeurs de k parmi n
	return sp.comb(n, k,exact=True) #Besoin de 'exact' pour les TRES grands nombres



#print(somme_combi(2,2))
def Somme_partielle(tab) :
	for i in range(1,len(tab)) :
		tab[i]=tab[i]+tab[i-1]
	return(tab)

#print(Somme_partielle([1,2,3,4]))

def loi_Condi(a,b) : #Renvoie une réalisation d'avancée, sachant qu'il y a cartes à distribuer et encore b cartes 'actives'
	tab=(b-a+1)*[0]
	i=0
	for k in range(0,b-a+1) :
		tab[k]=nb_combi(a-1,b-1-k)
	rea=random.randint(1,sum(tab))
	#print(tab)
	tab=Somme_partielle(tab)
	#print(rea)
	for k in range(len(tab)) :
		if (rea<=tab[k]) :
			#print(tab)
			return(k)
#print(loi_Condi(2,4))

def MinMax(m1,m2) : #renvoie (Min(m1,m2),Max(m1,m2)) en terme de longueur.
	if(len(m1)>len(m2)) :
		return m2,m1
	else : return m1,m2

def previous_RS(deck) :
	r=len(deck)
	M=binomiale(r,1/2)
	poss=nb_combi(M,r)
	
	M1=deck[0:M]
	M2=deck[M:]
	m1,m2 = MinMax(M1,M2)
	i=0 #nombre de cartes distribuées
	pos=0 # place de la dernière carte distribuée
	while(i<len(m1) and pos<=len(m2)) :
		pos=pos+loi_Condi(len(m1)-i,r-pos)
		m2.insert(pos,m1[i])
		pos+=1
		i+=1
	if(i<len(m1) and pos>len(m2)) :
		m2.extend(m1[i:])
	return m2

def tirage(M,r):
	E=range(r)#=[0, 1, .. , r-1]
	for k in range(0,M) :
		i=random.randint(k,r-1)
		aux=E[k]
		E[k]=E[i]
		E[i]=aux
	T=sorted(E[:M]) #renvoie une copie triée de E[:M]
	U=sorted(E[M:]) #renvoie une copie triée de E[M:]
	return T,U

def riffle_shuffle(deck):
	r=len(deck)
	M=binomiale(r,1/2)
	r=len(deck)
	T,U=tirage(M,r)
	new_deck=range(r)
	for i in range(M):
		new_deck[T[i]]=deck[i]
	for i in range(M,r):
		new_deck[U[i-M]]=deck[i]
	return new_deck

def X_n(deck,n) : ##Renvoie une réal de n-ieme terme de la chaine X
	for k in range(n) :
		deck=riffle_shuffle(deck)
	return deck

#print(X_n(range(1,54),5))


def P_X(r,n) : ##Renvoie un tab tel que tab[k] = P(X_n=k) si r cartes. On suppose que k est un numero de permutation
	rfac = math.factorial(r)
	support=S_n(r)
	freq=rfac*[0]
	compt=0
	for k in range(100000) :
		deck=lcartes_create(r)
		deck=X_n(deck,n)
		freq[support.index(deck)]+=1
			
	return [i/100000 for i in freq]

#print(P_X(10,5),1/math.factorial(6))

def inv(deck) : #Effectue le mélange propre à Y
	r=len(deck)
	ans= []
	compt =0
	for i in range(r) :
		if not(bernoulli(1/2)) : #Si on tire 0
			ans.append(deck[i-compt])
			deck.pop(i-compt)
			compt=compt+1
	ans.extend(deck)
	return ans

#print(inv(range(1,7)))

def Y_n(deck,n) : #Renvoie une réal de n-ieme terme de la chaine Y

	for k in range(n) :
		deck=inv(deck)
	return(deck)

#print(Y_n(range(1,54),10))


def P_Y(r,n) : #Renvoie un tab tel que tab[k] = P(Y_n=k) si r cartes On suppose que k est un numero de permutation
	rfac = math.factorial(r)
	rfac = math.factorial(r)
	support=S_n(r)
	freq=rfac*[0]
	compt=0
	for k in range(1000000) :
		deck=lcartes_create(r)
		deck=Y_n(deck,n)
		freq[support.index(deck)]+=1
			
	return [i/1000000 for i in freq]
#print(P_Y(4,6),1/math.factorial(4))

def E(r,k) : #D'après wikipédia, le calcul du nombre eulérien est bon

	if(k>=r or r==0) : 
		return 0
	elif(k==0) : 
		return 1
	else : 
		return ((r-k)*E(r-1,k-1) +(k+1)*E(r-1,k))

#print(E(45,15))

#On ne se sert pas des deux fonctions suivantes

def A(n,r) : #Valeur du nombre Eulérien A(n,r) de B. Mann
	if(r==1) :
		return 1
	else :
		res=0
		for k in range(1,r) :
			res+= nb_combi(n,n+r-k)*A(n,k)
		return (r**n) - res

#print(A(10,6))

#Ci dessous, pour éviter de refaire tous les appels récursifs à calcul de A(r,k), 
#on calcule la valeur des A(r,k) petit à petit, en stockant d'une part le valeurs
#des A(r,k) dans un tableau, et les valeurs de cbin(r,r+k) dans un autre. 

def DEL_appro(r,n) : #Valeur de la DEL en utilisant les A(r,k).

	rfac=math.factorial(r)
	tab_combi = r*[0] #tab_combi contiendra tous les nb_combi(r,r+k), 0<k<r
	for k in range(0,r) :
		tab_combi[k]=nb_combi(r,r+k+1)
	#tab_combi.reverse()
	tab_A= r*[0] #tab_A contiendra tous les A(r,k), 0 <= k <= r
	tab_A[0]=1
	for k in range(1,r) :
		res=0
		for i in range(1,k+1) : 
			res+=tab_combi[k-i]*tab_A[i-1]
		tab_A[k]=(k+1)**r - res
	res=0
	for i in range(1,r+1) :
		coeff=nb_combi(r,(2**n)+r-i)
		div=2**(n*r)
		res += tab_A[i-1]*abs((coeff/div)-(1/rfac))
	return res/2

#print(DEL_appro(165,20))


def dist_en_loi(Var,r,n) : #Calcule la distance en loi de 'Var'_n et de son invariante, quand r cartes 
	rfac=math.factorial(r)
	if(Var=='X') :
		print("On calcule la DEL de X_{} pour {} cartes".format(n,r))
		Emp=P_X(r,n)

	elif(Var=='Y') :
		print("On calcule la DEL de Y_{} pour {} cartes".format(n,r))
		Emp=P_Y(r,n)

	elif(Var=='Z') :
		print("On calcule la DEL de Z_{} pour {} cartes".format(n,r))#Z s'obtient en utilisant la formule avec A(r,k)
		return DEL_appro(r,n)
	
	res = 0
	for k in range (rfac) :
		val_emp=Emp[k]
		Inv=1/rfac
		#print("Empiriquement on trouve {} alors que l'invariante est {}".format(val_emp,The))
		res= res+ abs(val_emp-Inv)
	return res/2

#print(dist_en_loi('X',7,6))

def graphe_DEL(Var,r,n) : #affiche le graphe de la distance en loi de 'Var'
	absc=[i+1 for  i in range(n)]
	ordo=n*[0]
	for k in absc :
		ordo[k-1]=dist_en_loi(Var,r,k)
	plt.title("Graphe DEL avec {} cartes et {} riffle shuffles".format(r,n))
	plt.plot(absc,ordo,label='Graphe DEL de P_{} et de la distrib uniforme'.format(Var))
	plt.xlabel('Nombre de melanges')
	plt.ylabel('Distance en Loi')
	plt.legend()
	plt.ylim(0,1.3)
	#plt.show()

#graphe_DEL('X',6,5) #Pensez à décommenter le plt.show

def deux_graphes(r,n) : #Affiche DEL de X et de Y. Meme vitesse de convergence. Peu lisible.
	#graphe_DEL('X',r,n)
	graphe_DEL('Z',r,n)
	plt.show()

#deux_graphes(10,5)



