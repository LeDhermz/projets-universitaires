# -*- coding: utf8 -*-

from __future__ import division
import random
import math
import numpy as np
import matplotlib.pyplot as plt
import itertools as its

def Lcartes_create(r):
    l=range(1,r+1)
    return l
    
def k_insert(l,k):
    x=l[0]
    for n in range (k):
        l[n]=l[n+1]
    l[k]=x
    return l

def Melange_N(r,n) : # on realise n k_insertions avec un deck a r cartes
    l=Lcartes_create(r)
    for i in range(n) :
        k=random.randint(0,r-1)
        k_insert(l,k)
    return l 

def deroulejeu(r, x): #Realise  x melanges, et renvoie la valeur de T, puis un tableau avec les T_i successifs  
    y=0 #compte du nombre de remontees de la carte r
    T=[0]
    k=0
    l=Lcartes_create(r)
    for n in range (x):
        k = random.randint(0,r-1)
        l=k_insert(l,k)
        #print(l)
        if(y<=r-1):
            if (l[r-y-1] != r):
                T.append(n+1-sum(T))
                y=y+1
                #print("La remontee {} a eu lieu au melange {}".format(y,n+1))
                #print(T)
    return sum(T),T

def Calcul_T_i(r,i,deck) : #Renvoie la valeur de T_i
    cartes=deck
    t_i=0
    while(cartes[r-i] != r): # on attend la réalisation de T_i-1
        k=random.randint(0,r-1)
        k_insert(cartes,k)
    while(cartes[r-i-1] != r) : #Une fois T_i -1 atteint, il n'y plus qu'à compter le nb de passage dans la boucle
        k=random.randint(0,r-1)
        k_insert(cartes,k)
        t_i=t_i +1
    return t_i



def histo(r,i,rangemax) : #Renvoie l'histogramme des valeurs de T_i
    data=[]
    for k in range(5000) :
        deck=Lcartes_create(r)  
        Ti_rea = Calcul_T_i(r,i,deck)
        data.append(Ti_rea) #data contient toute les realisations de T_i
    plt.hist(data, range = (0,rangemax), bins = 50, color = 'yellow',edgecolor = 'red')
    plt.xlabel("Valeurs de T_{}".format(i))
    plt.ylabel("Frequence d'apparition")
    plt.title("Histogramme de T_{} pour {} cartes".format(i,r))
    plt.show()

def Esp(nbcartes) : # Espérance Empirique de T
    r=nbcartes
    m=0
    for k in range(100000) :
        rea=deroulejeu(r,int(2*r*math.log(r)))[0] #deroulejeu[0] = T
        m+=rea
    return m/100000.

# Somme partielle de la suite Harmonique, utile pour calculer E(T),
# car l equivalence E(t)~r*ln(r) est 'lente' 
def H_n(n) : 
    res=0
    for k in range(1,n+1):
        res+= 1/k
    return res

def distrib_a_T_plusN(r,n) : #Retourne la permutation qui vient n+1 k-insertions apres T

    deck=Lcartes_create(r)
    while(deck[0] != r) : #on va jusqu'a T
        k=random.randint(0,r-1)
        k_insert(deck,k)
    k=random.randint(0,r-1)
    k_insert(deck,k) # On est a T+1
    for i in range (n) :
        k=random.randint(0,r-1)
        k_insert(deck,k)
    return deck

def distrib_a_T_i_plus1(r,i) : #Retourne la permutation qui vient juste après T_i

    deck=Lcartes_create(r)
    while(deck[r-i-1] != r) : #on va jusqu'a T_i
        k=random.randint(0,r-1)
        k_insert(deck,k)
    k=random.randint(0,r-1)
    k_insert(deck,k) # On est a T_i + 1
    return deck
    

def Equals(a,b) : #Renvoie un booleen après comparaison de tableau, sert dans Graphe_DistribT
    t=True
    k=0
    while(t and k<len(a)):
        t=(a[k]==b[k])
        k=k+1
    return t

def S_n(n) : #Renvoie les éléments du groupe symétrique
    L= range(1,n+1)
    S=[]
    for s in its.permutations(L) :
        S.append(list(s))
    return(S)

def Graphe_Distrib_T_i(r,j): #Simule 100000 fois X_(T_i) et affiche les frequences de sortie des permutations
    classes= S_n(r) 
    rfac=math.factorial(r)
    freq=rfac*[0]
    for k in range(1000000):
        real=distrib_a_T_i_plus1(r,j)
        for i in range(rfac) :
            if(real==classes[i]):
                freq[i]=freq[i]+1
                break
    plt.bar(range(1,rfac+1), [i*1./1000000 for i in freq], 1, color='b' )
    plt.plot([0,rfac+1],[1/rfac,1/rfac],color='r',label="Distribution Uniforme")
    plt.title("Graphe de la distribution de X a T_{} + 1 ".format(j))
    plt.legend()
    plt.show()
    return classes

#Graphe_Distrib_T_i(6,5)

def Graphe_DistribT(r,n): #Simule 100000 fois X_T+1+n et affiche les frequences de sortie des permutations
    classes= S_n(r) 
    rfac=math.factorial(r)
    freq=rfac*[0]
    for k in range(10000000):
        real=distrib_a_T_plusN(r,n)
        for i in range(rfac) :
            if(real==classes[i]):
                freq[i]=freq[i]+1
                break
    plt.bar(range(1,rfac+1), [i*1./10000000 for i in freq], 1, color='b' )
    plt.plot([0,rfac+1],[1/rfac,1/rfac],color='r',label="Distribution Uniforme")
    plt.title("Graphe de la distribution de X a T + {} +1 ".format(n))
    plt.ylim(0,1/rfac*4/3)
    plt.legend()
    plt.show()
    return classes


# ~~~~~~~~Ci-dessous des commandes a décommenter une  à une pour tester les fonctions.~~~~~


#print(Melange_N(10,5)) 
#print(deroulejeu(10,50))
print(Esp(10),10*H_n(9))
#print(distrib_a_T_plusN(8,0))
#Graphe_DistribT(5,0)
#histo(32,10,20)
