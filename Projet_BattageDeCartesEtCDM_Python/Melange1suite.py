# -*- coding: utf8 -*-

from __future__ import division
import Melange1 as M1
import random
import math
import itertools as its
import matplotlib.pyplot as plt

#Pour distinguer les permutations, on suppose qu'elles sont numérotées.

def Pi(r,sigma) : #loi uniforme sur S_n
	rfac=math.factorial(r)
	return 1/rfac

def S_n(n) :
	L= range(1,n+1)
	S=[]
	for s in its.permutations(L) :
		S.append(list(s))
	return(S)

def P_X(r,n) : #Renvoie tab tel que tab[k] = P(X=k)
	rfac = math.factorial(r)
	support=S_n(r)
	freq=rfac*[0]
	for k in range(100000) :
		deck=M1.Melange_N(r,n) #On simule n mélanges d'un paquet
		freq[support.index(deck)]+=1 #On incrémente le compte d'apparition
			
	return [i/100000 for i in freq]

def dist_en_loi(r,n) : #Calcule la DEL entre X et Pi
	rfac=math.factorial(r)
	res = 0
	Emp=P_X(r,n)
	for k in range (rfac) :
		Inv=Pi(r,k)
		#print("Empiriquement on trouve {} alors que l'invariante est {}".format(Emp[k],The))
		res= res+ abs(Emp[k]-Inv)
	return res/2

def graphe_DEL(r,n) : #affiche le graphe de la distance en loi
	absc=[i+1 for  i in range(n)]
	ordo=n*[0]
	for k in absc :
		ordo[k-1]=dist_en_loi(r,k)
	plt.plot(absc,ordo)
	plt.xlabel('valeurs de n')
	plt.ylabel('Distance en loi')
	plt.title("Graphe Distance en loi de X avec {} cartes".format(r))
	plt.ylim(0,1)
	plt.legend()
	plt.show()

def P_Y(r,n,i,pos) : #Estimation de la Probabilité que la carte initalement en i se trouve en pos apres n mélanges
	freq=0
	for k in range(5000) : # Faire une FOIS puis estimer pour tous les pos
		deck=M1.Melange_N(r,n)
		if (deck[pos-1]==i) : freq=freq+1
	return freq/5000

#La fonction suivante permet, avec presque autant d'opérations que la précédente,
# de calculer les probas de réalisation de TOUTES les positions, au lieu d'une seule. 

def P_Y_tab(r,n,i) : # P_Y_tab(r,n,i)[k] la probabilité que la carte initialement i se retrouve en k+1 après n mélanges
	freq = r*[0]
	for k in range(50000) : 
		deck=M1.Melange_N(r,n)
		pos = deck.index(i)
		freq[pos]= freq[pos] +1
	return [k/50000 for k in freq]

def Pi_tilde(r,pos) : #invariante pour Y
	return 1/r

def dist_en_loi_tilde(r,n,i) : 
	res =0
	Emp=P_Y_tab(r,n,i)
	for k in range(0,r) : 
		Inv=Pi_tilde(r,k)
		res=res + abs(Emp[k]-Inv)
	return res/2

def graphe_DEL_tilde(r,n,i) : #On affiche la DEL entre P_Y^i_k  et Pi_tilde pour k allant de 1 à n
	absc= range(1,n+1)
	ordo=n*[0]
	for k in absc :
		ordo[k-1]=dist_en_loi_tilde(r,k,i)
	plt.plot(absc,ordo,label="i={}".format(i))
	plt.xlabel('valeurs de n')
	plt.ylabel('Distance en loi')
	plt.title("Graphe Distance en loi quand Y_0 = {}, avec {} cartes ".format(i,r))
	plt.ylim(0,1)
	plt.legend()
	#plt.show()

def graphes_DEL_tilde(r,n) : #Chaque courbe correspond au graphe de la DEL(P_Y_n,Pi_tilde) pour un i de départ different
	for i in range(1,r+1) :
		graphe_DEL_tilde(r,n,i)
	plt.xlabel('valeurs de n')
	plt.ylabel('Distance en loi')
	plt.title('Graphes des DEL de P_Y^i_k et Pi')
	plt.show()

# ~~~~~~~~Ci-dessous des commandes a décommenter une  à une pour tester les fonctions.~~~~~

#print(P_X(5,100,2))
#print(dist_en_loi(5,10))
#graphe_DEL(5,15)
#print(P_Y(5,10,3,3))
#print(dist_en_loi_tilde(5,10,2))
#graphe_DEL_tilde(10,15,5)
graphes_DEL_tilde(10,30)

