# -*- coding: utf8 -*-

from __future__ import division
import itertools as its

import random
import math
import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sp

# Bcp de tests dans ce fichier. Celui qui aboutit est test_riffle2, qui devient riffle_shuffle dans le fichier principal.
# Il y avait deux façons de simuler un riffle_shuffle, une fois le paquet coupé en deux sous-paquets m1 et m2:

#		1) Force brute : On crée  toutes les permus qui verifient que m1 et m2 sont des ss suites croissantes, 
#	puis on en tire une au hasard.
# 		2) Simulation de melange : On injecte les cartes de m1 dans m2, avec des probabilités telles qu'in fine,
#	toutes les permus possibles ont les même chances d'apparaître.

#Ne voyant pas de moyen peu couteux de faire la 1) on s'est tournés vers la 2)
# La fonction test_riffle2 repose sur le calcul de la probabilité qu'une carte soit envoyée en k,
#	sachant la position de la carte précédente. Ci-dessous, nous détaillons l'élaboration de la fonction Loi_condi
# 
#1) Départ : Placement de la première carte (nommée '1') de m1 dans m2.
#On devine qu'il faut placer '1' en fonction du nombre de permutations que l'on pourra encore atteindre
# une fois placée. Ce nombre dépend du nombre de cartes derrière '1' dans m1, mais aussi du nombre de positions
# encore disponibles dans m2. 

#	Exemple : m1= [1,2,3] m2= [4,5,6] ; on peut mettre '1' en 0, en 1, en 2 ou en 3
#			'1'-> 2 amène à m2=[4,5,1,6] et on peut encore atteindre [4,5,1,2,3,6],[4,5,1,2,6,3] et [4,5,1,6,2,3]
#		Si on note P_1(i) le nb  de permus encore atteignables/Possibles en plaçant '1' en i, alors on a :

#			P_1(0)= 10, P_1(1)=6, P_1(2)=3 P_1(3)=1.

# 		On comprends dès lors que pour atteindre uniformément chaque permu, il faut qu'on place '1' en 0 10x
#		plus souvent qu'en 3. Mais d'ou viennent ces valeurs ?
#		On note cbin(k,n) le coefficient binomial 'k parmi n'
#		On constate que :
# 		 P_1(0) =  cbin(2,5) ; P_1(1) = cbin(2,4) P_1(2) = cbin(2,3) P_1(3)=cbin(2,2)


#2) Placement des cartes suivantes
# On a placé '1' en pos. On doit encore placer '2' entre pos et la fin de m2 : il reste len(m2)-pos +1 positions
# Comme précédemment, on place '2' selon les P_2(i), qui dépendent du nombre de cartes à placer après '2',
# et du nombre de positions où on pourra encore les placer, ie de la position prise par '1'
# A la troisième carte, on comprend l'essentiel : connaissant le nb total de carte, il suffit de connaître la position
# prise par la carte précédente pour placer la suivante, en utilisant la formule explicitée ci dessous :
#
# 		Sachant qu'il y n cartes dans m2, m cartes dans m1, et que la carte précédente est placé en pos,
#		La probabilité P_c(p) que la c-ème carte de m1  aille en position p est :
#			P_c(p) = cbin(m-c-1,r-pos-p)/cbin(m-c,r-pos) 
#		En effet m-c est le nombre de carte de m1 qu'il restera à placer, et n-pos+1 est le nb de positions restantes

#3) Cas remarquables :
# 	i) La dernière carte de m1 se distribue uniformément en probabilité sur les places qu'il lui reste.
#	ii) Si une carte s'est placée au bout de m2, alors il n'y a plus qu'a rajouter les autre cartes de m1 derrière elle.


# Le programme  test_riffle2 est écrit selon le raisonnement précédent. Plus précisément :
# Le calcul de la proba conditionelle est effectué dans Loi_Condi(). Loi_condi est appelé lorsque
# l'on insère une carte. Il renvoie un entier, correspondant à "l'avancée" de la carte insérée.
#  Si loi_condi =0, on insère juste après la carte insérée précédemement, donc en pos+1. Si loi condi = n-pos+1,
# on insère en fin de paquet.
#		
#		Loi_condi() fonctionne comme suit : Elle prend en argmument le nb de cartes restant à distribuer après 
#		celle qu'on insère, et le nombre p de cartes actives, ie restent à distribuer et qui ne sont pas dépassées.
#		Loi_condi crée ensuite un tableau tab tel tab[k] = "nb de permu atteignables si on insère en avançant de k"
# 		En reprenant l'exemple précédent, en insérant 1 on aurait dans Loi_condi le tableau tab =[10,6,3,1]
#		On calcule ensuite les sommes partielles tel que tab=[10,16,19,20], et on tire unif. un entier k dans [1,20] 
#		Si 1<=k<=10 loi_condi renvoie 0; ie on insère juste après la carte précédente. Si 11<=k<=16, on insère 1
#		carte après la précédente etc.. 

# Ce qui précède est suffisant pour comprendre l'implémentation de test_riffle2.
# Le fichier comporte aussi d'autres tentatives non abouties, dont la plus naive est certainement FAKE_riffleshuffle()
#
#Pour vérifier que qu'un riffleShuffle fonctionne bien, on utilise controle_Uni(deck,M), qui projette les fréquences d'apparition
# des permutations possibles à M fixé. Actuellement, il n'est utilisable que pour FAKE_riffleshuffle(), et pour Test_riffle2()



def MinMax(m1,m2) : # Pour eviter de calculer des factorielles négatives, on injecte le plus petit dans le plus grand
	if(len(m1)>len(m2)) :
		return m2,m1
	else : return m1,m2

def Lcartes_create(r):
    l=[]
    for n in range (r):
        l.append(n+1)
    return l

def nb_combi(k,n) : # valeurs de k parmi n
	return sp.comb(n, k,exact=True)

def bernoulli(p) :
	rea=random.random()
	if (rea<p) : return 1
	else : return 0

def binomiale(n,p) :
	res = 0
	for k in range(n) :
		res = res + bernoulli(p)
	return res

def FAKE_riffleShuffle(deck,M) : #Une fois M choisi, toutes les permu n'ont PAS les même chances de se réaliser
	r=len(deck)
	#M=binomiale(r,1/2)
	m1=deck[0:M]
	m2=deck[M:]
	i=0
	for c in m1 :
		k=random.randint(i,len(m2))
		m2.insert(k,c)
		i=k+1
	return(m2)

#print(FAKE_riffleShuffle(range(1,5),2))

def somme_combi(k,n) : # renvoie la somme des k parmi i, pour i allant de k à n
	#if(n<k): return somme_combi(n,k) #/ Utile si len(m1)>len(m2)
	res = 0
	for i in range(k,n) :
		res = res + nb_combi(k,i)
	return(res)

#print(somme_combi(2,2))
def Somme_partielle(tab) : #renvoie un tableau contenant les sommes partielles de tab
	for i in range(1,len(tab)) :
		tab[i]=tab[i]+tab[i-1]
	return(tab)

#print(Somme_partielle([1,2,3,4]))

def loi_Condi(a,b) : #Renvoie une réalisation d'avancée, sachant qu'il y a cartes à distribuer et encore b cartes 'actives'
	tab=(b-a+1)*[0]
	i=0
	for k in range(0,b-a+1) :
		tab[k]=nb_combi(a-1,b-1-k)
	#print(tab)
	rea=random.randint(1,sum(tab))
	#print(tab)
	tab=Somme_partielle(tab)
	#print(rea)
	for k in range(len(tab)) :
		if (rea<=tab[k]) :
			#print(tab)
			return(k)

#print(loi_Condi(4,8))

def Graphe_loi_Condi(): #test pour voir si le rand est correct
	n=100000
	X=[0,1,2,3]
	Y=[0,0,0,0]
	for k in range(n):
		Y[loi_Condi(3,5)] += 1
	plt.bar(X,Y)
	plt.plot([0,1,2,3],[n*15/26,n*7/26,n*3/26,n/26],color='r',label='Distribution attendue')
	plt.legend()
	plt.show()

#Graphe_loi_Condi()

def Test_riffle2(deck,M) :

	r=len(deck)
	#M=binomiale(r,1/2)
	poss=nb_combi(M,r)
	
	M1=deck[0:M]
	M2=deck[M:]
	m1,m2 = MinMax(M1,M2)
	i=0 #nombre de cartes distribuées
	pos=0 # place de la dernière carte distribuée
	while(i<len(m1) and pos<=len(m2)) :
		pos=pos+loi_Condi(len(m1)-i,r-pos)
		m2.insert(pos,m1[i])
		pos+=1
		i+=1
	if(i<len(m1) and pos>len(m2)) :
		m2.extend(m1[i:])
	return m2

#print(Test_riffle2(range(1,54),25))

def TEST_riffleShuffle(deck) : #Un essai de plus, où l'on tentait d'approcher les probas conditionelles
	
	r=len(deck)
	M=binomiale(r,1/2)
	poss=nb_combi(M,r)
	
	m1=deck[0:M]
	m2=deck[M:]

	i=0
	#print(m1,m2)
	for j in range(0,len(m1)) :
		while(i<len(m2) and len(m1)>1) :
			prob=somme_combi(len(m1)-j-1,len(m2)-i+1)/poss
			#print("prob que {} aille en {} est {} / {}".format(m1[j],i,somme_combi(len(m1)-j-1,len(m2)-i+1),poss))
			
			if(random.random()<prob) :
				#print("On a inseré {} à la position {} ".format(m1[j],i))
				m2.insert(i,m1[j])
				i=i+1
				#print(m1,m2,i)
				break
			i=i+1
	if (i>=len(m2)):
		m2.extend(m1[j:])
		#print("On met {} à la fin de de m2".format(m1))
		return(m2)
	if (len(m1)==1) :
		prob= 1/(len(m2)+1-i)
		for k in range(i,len(m2)) :
			#print("prob que {} aille en {} est {}".format(m1[0],k,prob))
			if(random.random()<prob) :
				m2.insert(k,m1[0])
				m1.remove(m1[0])
				break
		m2.append(m1[0])
		m1.remove(m1[0])
		return(m2)

	return(m2)

#print(TEST_riffleShuffle(range(1,6)))

def Test_riffle(deck,M) :
	r=len(deck)
	#M=binomiale(r,1/2)
	poss=nb_combi(M,r)
	
	M1=deck[0:M]
	M2=deck[M:]
	m1,m2 = MinMax(M1,M2)
	i=0
	j=0

	prec=1

	while(j<len(m1)-1) :
		while( i< len(m2)) :
			prob=loi_Condi(len(m1)-j-1,len(m2)-i+1)
			#print("La proba que {} aille en {} sachant {} est en {} est {}".format(m1[j],i,m1[j-1],i-1,prob))
			i=i+prob
			m2.insert(i,m1[j-1])
			#print(i,j)
			i=i+1
			j=j+1

	if(j==len(m1)-1 and i<len(m2)) : #On insère la dernière carte
		k=random.randint(i,len(m2))
		m2.insert(k,m1[j-1])
		return(m2[:len(deck)])

	if(i>= len(m2)) :
		m2.extend(m1[j:])
		return(m2[:len(deck)])

#print(Test_riffle([1,2,3,4,5,6],3))

def ord_annexe(a,b,T):#Test si a et b qui sont dans la liste sont tels que a a un index plus petit que b : Ok
    
    return T.index(a)<T.index(b)

def ordm(T,M):#On suppose M inclu dans T et tous les elements de T differents 
    indexM=0;
    for n in range(len(T)):
        if T[n] in M:
            for i in range(n+1,len(T)):
                if T[i] in M:
                    if (ord_annexe(T[n],T[i],M)==False):
                        return False
    return True
                    
              
            
    
def permu_p(m1,m2): #Créé le groupe symétrique, et renvoie les permus qui vérifient ce que l'on souhaite
    T=list(its.permutations(Lcartes_create(len(m1)+len(m2))))
    T1=[] #tableau des index a supprimer
    for n in range(len(T)):
        if (ordm(T[n],m1)==False or ordm(T[n],m2)==False) :
            T1.append(n)
    y=0
    for i in range (len(T1)):
        del T[T1[i]-y]
        y=y+1
    return [list(i) for i in T] 

#Les deux fonctions ci dessous ont été proposées par 
#Cedric Barret, enseignant en CPGE. Elles aboutissent à
#un mélange bien plus efficace que riffleShuffle2

def tirage(M,r):
	E=range(r)#=[0, 1, .. , r-1]
	for k in range(0,M) :
		i=random.randint(k,r-1)
		aux=E[k]
		E[k]=E[i]
		E[i]=aux
	T=sorted(E[:M]) #renvoie une copie triée de E[:M]
	U=sorted(E[M:]) #renvoie une copie triée de E[M:]
	return T,U

def Shuffle_Barret(deck,M):
	r=len(deck)
	T,U=tirage(M,r)
	new_deck=range(r)
	for i in range(M):
		new_deck[T[i]]=deck[i]
	for i in range(M,r):
		new_deck[U[i-M]]=deck[i]
	return new_deck

#print(Shuffle_Barret([1,2,3,4,5,6,7,8,9,10],3))


def controle_Uni(deck,M) : #On affiche les frequences de réal des M parmi r permu possibles
	m1=deck[0:M]
	m2=deck[M:]
	S=permu_p(m1,m2)
	print(S)
	print(len(S))
	freq=len(S)*[0]

	for k in range(1000000) :
		#rea = Test_riffle2(deck,M)
		rea=Shuffle_Barret(deck,M)
		freq[S.index(rea)]=freq[S.index(rea)] +1
	freq=[i/1000000 for i in freq ]
	print(freq)
	plt.title("Frequence des permu possibles avec deck = {} et M ={}".format(deck,M))
	plt.bar(range(1,len(S)+1),freq)
	plt.plot(range(len(S)+1),(len(S)+1)*[1/len(S)],color='r',label='Distribution uniforme')
	plt.legend()
	plt.ylim(0,1.5/(len(S)))
	plt.show()

controle_Uni([1,2,3,4,5,6],3) # Ne pas mettre len(deck)>10, car permu_p est TRES TRES GOURMANDE
#Attendre  suffisament, car on fait 10^6 tests