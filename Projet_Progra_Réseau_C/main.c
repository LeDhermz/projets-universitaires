//#include <zconf.h>
#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <time.h>
#include <assert.h>
#include <sys/time.h>
#include <netdb.h>
#include <stdbool.h>
//#include "rfc6234/sha.h"


/*typedef struct Pair{//Pas encore sur que l'on est besoin de l'id ni si la timeval est le plus judicieux
  //char id[8];
  struct in6_addr ip;
  //On appelleadresse de socketune paire(IP,port); on peut voir une adresse de socket comme uneadresse de 48 ou 144 bits dont les premiers 32 ou 128 bits contiennent une adresse IP. Une adressede socket identifie unesocket, une abstraction représentant l’extrémité d’une communication decouche transport.
  short port;
  //des nombres de 16 bits appelésnumérosde port
  bool perma;//Permanent V ou temporaire F dans la liste de voisin 
  struct timeval tv;
}Pair;

typedef struct Donnee{
  unsigned long long id[8];
  unsigned short  sq;
  char donnee[192];
  }Donnee;*/


/*int h(char *suite_octets ,int len, unsigned char *res){ // La fonction h décrite dans le poly du projet
    SHA256Context ctx; // Elle retourne-1 en cas d'echec et 0 en cas de succès
    unsigned char hash[32];
    int status;

    status=SHA256Reset(&ctx);
    if(status< 0){
        perror("Erreur lors de la fonction SHA256Reset");
        return -1;
    }
    status=SHA256Input(&ctx,suite_octets,len);
    if(status< 0){
        perror("Erreur lors de la fonction SHA256Input");
        return -1;
    }
    status=SHA256Result(&ctx,hash);
    if(status!= 0){
        perror("Erreur lors de la fonction SHA256Result");
        return -1;
    }
    memmove(res,&hash,16);
    return 0;

    }*/

/*const unsigned long long create_id() { //Unsigned long long est un entier de 8 octets
    const unsigned long long max= 0xFFFFFFFFFFFFFFFF; //2^64 -1 la valeur max que peut contenir un unsigned long long
    return (unsigned long long)((double)rand()/RAND_MAX*max);
}


int comparSeq(int s1,int s2){// 1 si s1>s2 , 0 si egaux , -1 si s2>s1
  if ((s2-s1)%65535<32768){
    return -1;
  }else if ((s1-s2)%65535<32768){
    return 1;
  }else{ return 0;}
}*/

//DONNEES


//VOISINS


//TLV PAQUET// A CUSTOM C A GREGOUZ

/*bool paquet_accept(char *paquet,int taille){
  if (taille<5){return false;} //Pas de contenu dans le paquet
  unsigned char magic=paquet[0];
  unsigned char version=paquet[1];
  short length; memmove(&length,paquet+2,2);length=ntohl(length);
  return (length>0 && magic ==95 && version==1);
}
short body_paquet_length(char *paquet,int taille){
  if (paquet_accept(paquet,taille)){
    short length;
        memmove(&length,paquet+2,2);
        length=ntohl(length);
        if (length>1020){
            return -1;
        }
        return length;
  }
  return -1;
}
int TLVtype(char *paquet,int taille,int debut){//taille=taille du paquet , debut=nbr d'octets avant le debut de la TLV
  if (!(debut>taille)){
    unsigned char type=paquet[debut];
    if(type>=0 && type<=9){
      return type;
    }
  }
  return -1;
}*/
/*int taille_TLV(char *paquet, int taille, int debut){// en octet
  int type=TLVtype(paquet,taille,debut);
  if (type ==0){ return 1;}
  if (type ==1 || type==8){
    unsigned char length;
    memmove(&length,paquet+debut+1,1);
    return 2 +length;
    if (type==2 || type==5){
      return 2;
    }
    if (type==3){
      return 20;
    }
    if (type==4){
      return 18;
    }
    if (type==6){
      return 28;
    }
    if (type==7){
      return 10;
    }
   
    return -1;
    }*/
  // a refaire, il y a pleins de cas ou ya pas de next
/*int octet_next_TLV(char *paquet,int taille, int debut){ //debut=debut de la TLV precedente
  return debut+taille_TLV(paquet,taille,debut);
  }*/
 void creation_paquet(char *paquet,int bodylength){
   memset(paquet,95,1);
   memset(paquet+1,1,1);
   memmove(paquet+2,&bodylength,2);
 }
/*int TLV0(char * paquet, int octets){ //retourne le nombre d'octets occupé dans le paquet a envoyé, sinon -1 : paquet =adresse du paquet / Octets = nombre d'octets deja occupé dans le paquet 
  if (octets<1024){//Sinon cela veut dire que le paquet est deja rempli
    memset(paquet+octets,0,1);
    return octets+1;
  }
  return -1; //la TLV0 n'a pas été ajouté au paquet
}
int TLV1(char * paquet, int octets,int length){//length = taille du body
  if (octets+1+length<1024){
    memset(paquet+octets,1,1);
    memset(paquet+octets+1,length,1);
    memset(paquet+octets+2,0,length);
    return octets+2+length;
  }
  return -1;   
}*/

int TLV2(char *paquet, int octets){
  if (octets+1<1024){
    memset(paquet+octets,2,1);
    memset(paquet+octets+1,0,1);
    return octets+2;
  }
  return -1;
}

/*int TLV3(char *paquet, int octets, Pair voisin){
  if (octets+19<1024){
    memset(paquet+octets,3,1);
    memset(paquet+octets+1,18,1);
    memmove(paquet+octets+2,&voisin.ip,16);
    memmove(paquet+octets+18,&voisin.port,2);
    return octets+20;
  }
  return -1;
}

int TLV4(char *paquet,int octets,char *h){
  if (octets+17<1024){
    memset(paquet+octets,4,1);
    memset(paquet+octets+1,16,1);
    memmove(paquet+octets+2,h,16);
    return octets+18;
  }
  return -1;
}
int TLV5(char *paquet,int octets){
  if (octets+1<1024){
    memset(paquet+octets,5,1);
    memset(paquet+octets+1,0,1);
    return octets+2;
  }
  return -1;
}
/*int TLV6(char *paquet,int octets,Donnee d){
  if (octets+27<1024){
    memset(paquet+octets,6,1);
    memset(paquet+octets+1,26,1);
    memmove(paquet+octets+2,&d.id,8);
    memmove(paquet+octets+10,&d.sq,2);
    memset(paquet+octets+12,HASH NOEUD,16);
    return octets+28;
  }
  return -1;
  }*/
int TLV7(char *paquet, int octets,unsigned long long id ){
  if (octets+9<1024){
    memset(paquet+octets,7,1);
    memset(paquet+octets+1,8,1);
    memset(paquet+octets+18,id,8);
    return octets+10;
  }
  return -1;
}/*
/*int make_TLV8(char *paquet,int octets,Donnee d){//a faire
    if ((octets+27+strlen(d.donnee))<1024 && strlen(d.donnee)<193) {
        memset(paquet+octets,8,1); 
        memset(paquet+octets+1,26+strlen(d.donnee),1);
        memmove(paquet+octets+2,d.id,8);
        memmove(paquet+octets+10,d.sq,2); 
        unsigned char res[16];
        if (h_noeud(data,res)==0){
            memmove(paquet+octets_occupes+12,res,16);//On ajoute le hachage du noeud
            memmove(paquet+octets_occupes+28,data.chaine,strlen(data.chaine));// On ajoute les data du noeud
            return octets+28+strlen(d.donnee);
        }
        else{
            perror("Erreur lors de h_noeud");
            return -1;
        }
    }
    return -1;
    }*/


int main(int argc, char *argv[]) {
  char buff_reponse[1024];
  char buff_envoie[1024];
  memset(buff_reponse,0,1024);
  memset(buff_envoie,0,1024);
  printf("setup_co begin");
  //Mise en place struct addrinfo pour utiliser getaddrinfo
  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET6;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_V4MAPPED | AI_ALL;

  //creation de la socket
  int s =socket(AF_INET6, SOCK_DGRAM, 0);
  if(s < 0){
    perror("Erreur lors de la création de la socket ");
    exit(-123);
  }
  int rc;
  //socket polymorphe
  int val=0;
  rc = setsockopt(s, IPPROTO_IPV6, IPV6_V6ONLY, &val, sizeof(val));
  if (rc<0){
    perror("Erreur polymorphisme");
  }

  //getaddrinfo
  int status;
  struct addrinfo *res;
  status = getaddrinfo("jch.irif.fr", NULL, &hints, &res);
  if(status != 0) {
    perror("Erreur getaddrinfo");
    exit(-122);
  }
  struct addrinfo *t_adresse;
  struct sockaddr_in6 *adr_pro;
  for (t_adresse=res;t_adresse!=NULL;t_adresse=t_adresse->ai_next){
  }    adr_pro=(struct sockaddr_in6*)t_adresse->ai_addr;
  freeaddrinfo(res);
  creation_paquet(buff_envoie,2);
  TLV2(buff_envoie,2);
  status=sendto(s,buff_envoie,6,0,adr_pro,sizeof(struct sockaddr_in6));
  printf("On a envoyé %d octets\n",status);
  status=recvfrom(s,buff_reponse,1024,0,NULL,NULL);
  printf("On a recu %d octets\n",status);
}

