//#include <zconf.h>
#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <time.h>
#include <assert.h>
#include <sys/time.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "rfc6234/sha.h"
#include <stdbool.h>
#define ID "ebf254ab4586a80e"

//VOISIN

typedef struct Pair{
  struct in6_addr ip;
  in_port_t port;
  bool perma;
  struct timeval date;
}Pair;

typedef struct t_voisin{
  Pair table [15];
  int nbr_voisins;
}t_voisin;


t_voisin *creat_t_voisin(){
  t_voisin *ret=malloc(sizeof(t_voisin));
  Pair a;
  for (int i=0;i<15;i++){
    ret->table[i]=a;
  }
  ret->nbr_voisins=0;
  return ret;
}

void maj_tvoisin(t_voisin* tvoisin, struct sockaddr_in6 ajout){//Modifier : allouer de la memoire pour la pair qu'on creer avec la sockadress
  if (tvoisin->nbr_voisins==15){ return ;}
  struct timeval tvtemp;
  for (int i=0;i<tvoisin->nbr_voisins;i++){ //Si deja dans la table on update sa date
    if (memcmp(&tvoisin->table[i].ip, &ajout.sin6_addr, sizeof(struct in6_addr))==0){
      //printf("same adress que le voisin %d\n",i);
      gettimeofday(&tvtemp,NULL);
      memcpy(&tvoisin->table[i].date.tv_sec,&tvtemp.tv_sec,4);
      memcpy(&tvoisin->table[i].date.tv_usec,&tvtemp.tv_usec,4);
      return ; }    
  }
 
  int index_add=tvoisin->nbr_voisins;
  //Pas la meme adresse donc on l'ajoute a la table en tant que membre non permanent
  memset(&tvoisin->table[index_add],0,sizeof(tvoisin));
  tvoisin->table[index_add].perma=false;
  gettimeofday(&tvtemp,NULL);
  memcpy(&tvoisin->table[index_add].date.tv_sec,&tvtemp.tv_sec,4);
  memcpy(&tvoisin->table[index_add].date.tv_usec,&tvtemp.tv_usec,4);
  memcpy(&tvoisin->table[index_add].ip,&ajout.sin6_addr,sizeof(struct in6_addr));
  tvoisin->table[index_add].port=ajout.sin6_port;
  tvoisin->nbr_voisins++;
  printf("\n NOUVEAU VOISIN--Nombre de voisins : %d \n\n",tvoisin->nbr_voisins);
  if (tvoisin->nbr_voisins==0 || tvoisin->nbr_voisins==1){//Le premier voisin est permanent
    tvoisin->table[index_add].perma=true;
    return ;
  }
  return ;
}
void suppr_voisins(t_voisin *t , int index){//On ne supprime jamais l'index 0(cf enonce)
  for (int i=index;i<t->nbr_voisins-1;i++){
    t->table[i]=t->table[i+1];
  }
  t->nbr_voisins--;
  }
void tablev_70s_maj(t_voisin* tvoisin){
  struct timeval tvtemp;
   gettimeofday(&tvtemp,NULL);
   for (int i=0;i<tvoisin->nbr_voisins;i++){
     if (tvoisin->table[i].perma==false && (tvoisin->table[i].date.tv_sec - tvtemp.tv_sec > 70)){
       suppr_voisins(tvoisin,i);
     }
   }
}


//DONNEE

typedef struct Donnee{
  unsigned char id [16] ;
  short  sq;
  char donnee[192];
}Donnee;
struct tab_do{
  Donnee *debut;
  int nbr_donnee;
  int nbr_elm_max;
};
typedef struct tab_do *tabdo;

tabdo creer_td(){
#define CAPACITE 1000
  tabdo p = malloc(sizeof(struct tab_do));
  if( p == NULL)
    return NULL;
  p->debut = malloc( sizeof(Donnee) * CAPACITE );
  if(p->debut == NULL){
    free(p);
    return NULL;
  }
  p->nbr_donnee=0;
  p->nbr_elm_max=CAPACITE;
  return p;
}
int cmp_seq(short s1,short s2){//-1 si s1<= s2 1 sinon
  if ((s2-s1)%65535<32768){
    return -1;
  }else{
    return 1;
  }
}
int ajout_Do(tabdo t,Donnee d){
  if (t->nbr_donnee+1==t->nbr_elm_max){//on depasse la capacité
    int n = sizeof(Donnee)*2*t->nbr_elm_max;
    void *ntab = realloc(t->debut, n );
    if(ntab == NULL){
      return -1000;
    }
    t->debut = ntab;
  }
  if (t->nbr_donnee==0){//table vide
    memcpy(&t->debut[0].id,&d.id,8);
    memcpy(&t->debut[0].sq,&d.sq,2);
    memcpy(&t->debut[0].donnee,d.donnee,strlen(d.donnee));
    t->nbr_donnee=1;
    return 1;
  }
  int tem=memcmp(t->debut[t->nbr_donnee-1].id,d.id,8);
  if (tem==0){return 0;}
  if(tem<0){
        memcpy(&t->debut[t->nbr_donnee].id,d.id,8);
        memcpy(&t->debut[t->nbr_donnee].sq,&d.sq,2);
        memcpy(&t->debut[t->nbr_donnee].donnee,d.donnee,strlen(d.donnee));
	t->nbr_donnee++;
        return  1;
  }
  char res [192];
  memset(res,0,192);
  int i;
  
  for (i=t->nbr_donnee; (i>0)&& (tem<0) ;i--){
    memcpy(&t->debut[i].id,&t->debut[i-1].id,8);
    memcpy(&t->debut[i].sq,&t->debut[i-1].sq,2);
    //memset(res,t->debut[i-1].donnee,strlen(t->debut[i-1].donnee));
    memcpy(&t->debut[i].donnee,t->debut[i-1].donnee,strlen(t->debut[i-1].donnee));
    memset(res,0,192);
    tem=memcmp(d.id,t->debut[i-1].id,8);
    if (tem==0){return 0;}
  }
  if (tem==0){return 0;}
  memcpy(&t->debut[i].id,&d.id,8);
  memcpy(&t->debut[i].sq,&d.sq,2);
  memcpy(&t->debut[i].donnee,d.donnee,strlen(d.donnee));
  t->nbr_donnee++;
  return 1;
}
void print_donnee(Donnee *d){//affiche les champs d'une donnee
}
void afficher_td(tabdo t){//affiche le tableau de donnee
  int i;
  Donnee *courant=t->debut;
  for(i=0;i<t->nbr_donnee; i++){
    print_donnee(courant);
    courant+=sizeof(Donnee);
  }
}
void suppr_Do(tabdo t , int index){
  if (index==0){t->debut+=sizeof(Donnee);return;}
  
  for (int i=index;i<t->nbr_donnee-1;i++){
    t->debut[i]=t->debut[i+1];
  }
  t->nbr_donnee--;
  }
void detruire(tabdo t){//detruit le tableau
  free(t->debut);
  free(t);
}


//HASHAGE
int h(char *buf ,int buflen, unsigned char *res){ 
    SHA256Context ctx; // Elle retourne-1 en cas d'echec et 0 en cas de succès
    unsigned char hash[32];
    int status;
    status=SHA256Reset(&ctx);
    if(status< 0){
        perror("Erreur lors de la fonction SHA256Reset");
        return -1;
    }
    status=SHA256Input(&ctx,buf,buflen);
    if(status< 0){
        perror("Erreur lors de la fonction SHA256Input");
        return -1;
    }
    status=SHA256Result(&ctx,hash);
    if(status!= 0){
        perror("Erreur lors de la fonction SHA256Result");
        return -1;
    }
    memmove(res,&hash,16); //On tronque les 16 premiers octets
    return 0;

}
 size_t taille_do(Donnee d){
  return 10+strlen(d.donnee);
}
int hash_n(Donnee d,unsigned char *res){// mettre toutes les données de la Donnee dans une seule et meme char * et appliquer h dessus
  int taille_d=taille_do(d);
  int t_dchaine=taille_d-10;//Taille du message contenu dans la struct
  unsigned char bloc[taille_d];
  memcpy(bloc,&d.id,8);
  memcpy(bloc+8,&d.sq,2);
  memcpy(bloc+10,&d.donnee,t_dchaine); // On concatene les octets
  int status=h(bloc,taille_d,res);//le hachage va etre copie dans res
  if (status==0){
    return 0;
    }
  return -1;
  }

int hash_r(tabdo td,unsigned char *res){
  int taille_d;
  int taille_total;
  int index=0;
  int ite=td->nbr_donnee;
  for (int j=0;j<td->nbr_donnee;j++){
    taille_total=taille_total+taille_do(td->debut[j]);
  }
  unsigned char bloc[taille_total];
  for (int j=0;j<td->nbr_donnee;j++){
    memcpy(bloc+index,&td->debut[j].id,8);
    index+=8;
    memcpy(bloc+index,&td->debut[j].sq,2);
    index+=2;
    taille_d=taille_do(td->debut[j])-10;
    memcpy(bloc+index,&td->debut[j].sq,taille_d);
    index+=taille_d;
  }
  index=h(bloc,taille_total,res);
  if (index!=0){ return -1;}else{ return 0;}
    }
bool CMP_r(unsigned char *hash1,unsigned char *hash2){
  return memcmp(hash1,hash2,16);
}
//ENVOIE
void creation_paquet(char *paquet,unsigned short bodylength){
  paquet[0]=95;
  paquet[1]=1;
  bodylength=htons(bodylength);
  memcpy(paquet+2,&bodylength,2);
 }
int TLV0(char * paquet, int octets){ //retourne le nombre d'octets occupé dans le paquet a envoyé, sinon -1 : paquet =adresse du paquet / Octets = nombre d'octets deja occupé dans le paquet 
  if (octets<1024){//Sinon cela veut dire que le paquet est deja rempli
    memset(paquet+octets,0,1);
    return octets+1;
  }
  return -1; //la TLV0 n'a pas été ajouté au paquet
}

int TLV1(char * paquet, int octets,int length){//length = taille du body
  if (octets+1+length<1024){
    memset(paquet+octets,1,1);
    memset(paquet+octets+1,length,1);
    memset(paquet+octets+2,0,length);
    return 2+length;
  }
  return -1;   
}
int TLV2(char *paquet, int octets){
  if (octets+1<1024){
    memset(paquet+octets,2,1);
    memset(paquet+octets+1,0,1);
    return 2;
  }
  return -1;
}
int TLV3(char *paquet, int octets, Pair voisin){
  if (octets+19<1024){
    memset(paquet+octets,3,1);
    memset(paquet+octets+1,18,1);
    memcpy(paquet+octets+2,&voisin.ip.__in6_u,16);
    voisin.port=htons(voisin.port);
    memcpy(paquet+octets+18,&voisin.port,2);
    return 20;
  }
  return -1;
}

int TLV4(char *paquet,int octets,char *h){
  if (octets+17<1024){
    memset(paquet+octets,4,1);
    memset(paquet+octets+1,16,1);
    memcpy(paquet+octets+2,h,16);
    return 18;
  }
  return -1;
}
int TLV5(char *paquet,int octets){
  if (octets+1<1024){
    memset(paquet+octets,5,1);
    memset(paquet+octets+1,0,1);
    return 2;
  }
  return -1;
}
int TLV6(char *paquet,int octets,Donnee d){
  if (octets+27<1024){
    //printf("check\n");
    char res[16];
    int x=hash_n(d,res);
    memset(paquet+octets,6,1);
    memset(paquet+octets+1,26,1);
    memcpy(paquet+octets+2,&d.id,8);
    memcpy(paquet+octets+10,&d.sq,2);
    if (x==0){
    memset(paquet+octets+12,res,16);
    return 28;
    }else{
      perror ("erreur hashage\n");
      return -1;
  }
     printf("check6\n");
  return -1;
  }
  return -1;
}
int TLV7(char *paquet, int octets,Donnee d ){
  if (octets+9<1024){
    memset(paquet+octets,7,1);
    memset(paquet+octets+1,8,1);
    memcpy(paquet+octets+2,&d.id,8);
    return 10;
  }
  return -1;
}
int TLV8(char *paquet,int octets_envoie,Donnee d){//a faire
  int taille_c=strlen(d.donnee);
  if (taille_c<193){
    if ((octets_envoie+27+strlen(d.donnee))<1024 ) {
        memset(paquet+octets_envoie,8,1); 
        memset(paquet+octets_envoie+1,26+strlen(d.donnee),1);
        memcpy(paquet+octets_envoie+2,d.id,8);
        memcpy(paquet+octets_envoie+10,&d.sq,2);
        unsigned char res[16];
	int x=hash_n(d,res);
        if (x==0){
            memcpy(paquet+octets_envoie+12,res,16);
            memcpy(paquet+octets_envoie+28,d.donnee,taille_c);
            return 28+taille_c;
        }
        else{
            perror("Erreur hashage");
            return -1;
        }
    }
  }
    return -1;
    }
//RECEPTION
bool HEADER_accept(char *paquet,int taille){//Okay
  if (taille<5){return false;} //Pas de contenu dans le paquet
  unsigned char magic=paquet[0];
  unsigned char version=paquet[1];
  return (magic ==95 && version==1);
}

int TLVtype(char *paquet,int taille,int debut){//taille=taille du paquet , debut=nbr d'octets avant le debut de la TLV
  if (!(debut>taille)){
    unsigned char type=paquet[debut];
    if(type>=0 && type<=9){
      return type;
    }
  }
  return -1;
}
size_t taille_TLV(char *paquet, int taille, int debut){// en octet
  int type=TLVtype(paquet,taille,debut);
  if (type ==0){ return 1;}
  if (type ==1 || type==8 ||type==9){
    unsigned char length;
    memmove(&length,paquet+debut+1,1);
    return 2 +length;}
    if (type==2 || type==5){
      return 2;
    }
    if (type==3){
      return 20;
    }
    if (type==4){
      return 18;
    }
    if (type==6){
      return 28;
    }
    if (type==7){
      return 10;
    }
    
    return -1;
    }
int octet_next_TLV(char *paquet,int taille, int debut){ //debut=debut de la TLV precedente

    if (debut>taille){ 
        return 0;
    }
    unsigned char taille_tlv=taille_TLV(paquet,taille,debut);
    if(debut+taille_tlv<taille){
        return debut+taille_tlv;
    }
    return 0;
}


int main(int argc, char *argv[]){
  assert(argc == 3);
  printf("Debut dazibao :\n");
  //Mise en place
  t_voisin *tablev=creat_t_voisin();  //table des voisins
  tabdo td=creer_td(); //table des données
  int octets_envoie=0;
  int octets_recu=0;
  int status;
  int val = 0;
  int rc;
  char buff_rece[1024]; 
  char buff_envoie[1024];
  char buff[1024]; 
  unsigned char buff_hash[16];
  unsigned char node_h_annexe[16];
  unsigned char id_annexe [8];
  memset(buff_rece,0,1024);
  memset(buff_envoie,0,1024);
  memset(buff,0,1024);
  memset(buff_hash,0,16);
  struct sockaddr_in6 from;//destinataire des paquets
  socklen_t len6 = sizeof(from);
  memset(&from,0,len6);
  struct sockaddr_in6 annexe;
  memset(&annexe,0,len6);
  struct timeval tv1,tv2;
  gettimeofday(&tv1,NULL);
  gettimeofday(&tv2,NULL);
  short taille_TLVs;
  Donnee tempd;//Dans le cas ou on recoit TLV 8
  Donnee first;
  memset(&first,0,sizeof(Donnee));
  first.sq=0;
  memset(&first.id,&ID,8);
  short us=1234;
  memcpy(&first.sq,&us,2);
  strcpy(first.donnee, "miss u macdo");
  printf("ma donnee : %s\n",first.donnee);
  ajout_Do(td,first);
  srand(time(NULL));

  //Co_____debut du Dazibao avec une Neighboor request
  int s =socket(AF_INET6, SOCK_DGRAM, 0);
  if(s < 0){
    perror("erreur creation socket ");
    exit(-11);
  }
  struct sockaddr_in6 server;
  memset(&server, 0, sizeof(server));
    server.sin6_family = AF_INET6;
  server.sin6_port = htons(argv[2]);
 
  rc=setsockopt(s, IPPROTO_IPV6, IPV6_V6ONLY, &val, sizeof(val));
  if (rc<0){
    perror("erreur polymorphe");
    exit(-12);
  }
  val=1;
  rc = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
  if (rc<0){
    perror("erreur reus");
    exit(-12);
  }
  rc = bind(s, (struct sockaddr*)&server, sizeof(server));
  if(rc < 0){
    perror("");
    exit(-12);
  }
  struct addrinfo hints,*t;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET6;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_V4MAPPED | AI_ALL;
  hints.ai_protocol = 0;  
  if ((status = getaddrinfo(argv[1], argv[2], &hints, &t))!= 0) {
    perror("erreur getaddrinfo");
    exit(-3);
  }
  
  struct addrinfo *p;
  struct sockaddr_in6 *adrr;
  for (p=t;p!=NULL;p=p->ai_next){
    adrr=(struct sockaddr_in6 *)p->ai_addr;
    maj_tvoisin(tablev,*adrr);
    creation_paquet(buff_envoie,2);
    TLV2(buff_envoie,4);
    status=sendto(s,buff_envoie,6,0,adrr,sizeof(struct sockaddr_in6));
    printf("J'envoie %d octets : TLV 2 pour demarrer le dazibao \n",status);
    memset(buff_envoie,0,1024);
    if (status<1){
      perror("erreur sendto");
      exit(-123);
    }
  }
  freeaddrinfo(t);
  len6 = sizeof(from);
  //le projet debute :
  int octets_TLV_next;
  int type;
  int taille;
  while (1){
    printf("...........................................\n");
        fd_set readfs;
        FD_ZERO (&readfs);
        FD_SET (s, &readfs);
        struct timeval tv = {1, 0};
        status= select(s + 1, &readfs, NULL, NULL, &tv);
        if (status==-1){
            perror("Erreur lors de l'appel select");
            exit(-1);
        }
	
        if (FD_ISSET (s, &readfs)){// On a recu un paquet , on doit le parser puis y répondre
	  memset(buff_rece,0,1024);
	  octets_recu=recvfrom(s,buff_rece,1024,0,(struct sockaddr *) &from,&len6);
	  memset(&taille_TLVs,0,2);
	  printf("RECEPTION : paquet de %d octets\n",octets_recu);
	  if(HEADER_accept(buff_rece,octets_recu)){
	    memcpy(&taille_TLVs,buff_rece+2,2);
	    taille_TLVs=ntohs(taille_TLVs);
	     if (taille_TLVs==octets_recu-4){ //la length du paquet est bonne
	    printf("HEADER_accepted..\n\n");
	    maj_tvoisin(tablev,from);
             	octets_TLV_next=4;
		type=TLVtype(buff_rece,octets_recu,octets_TLV_next);
		taille=taille_TLV(buff_rece,status,octets_TLV_next);
	      while(octets_TLV_next!=0){
		switch(type){
		case 2:
		  memset(buff_envoie,0,1024);
		  octets_envoie=4;
		  int r=rand()%tablev->nbr_voisins;
		  octets_envoie=TLV3(buff_envoie,octets_envoie,tablev->table[r]);
		  creation_paquet(buff_envoie,octets_envoie);
		  status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&from,len6);
		  if(status<0){ printf("Recu :TLV Neighbour Request - Echec Reponse\n");break; }
		  printf("Recu : TLV Neighbour Request Envoie: TLV  Neighbour\n");
		  break;
		case 3:
		  //verifier que le hash est bon
		  memset(buff_envoie,0,1024);
		  octets_envoie=4;
		  memset(&annexe,0,sizeof(annexe));
		  len6=sizeof(annexe);
		  memcpy(&annexe.sin6_addr,buff_rece+octets_TLV_next+2,16);
		  memcpy(&annexe.sin6_port,buff_rece+octets_TLV_next+18,2);
		  annexe.sin6_family=AF_INET6;
		  //annexe.sin6_port=ntohs( annexe.sin6_port); // Boutisme
		  hash_r(td,buff_hash);
		  octets_envoie=TLV4(buff_envoie,octets_envoie,buff_hash);
		  creation_paquet(buff_envoie,octets_envoie);
		  status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&annexe,len6);
		  if(status<0){
		    perror("erreur");
		    printf("Recu : TLV Neighbour Echec Reponse\n");break; }
		  printf("Recu : TLV Neighbour Envoie: TLV Network Hash\n");
		  break;
		case 4:
		  // printf("Recu : TLV network hash\n");
		  memset(buff_hash,0,16);
		  hash_r(td,buff_hash);
		  rc=CMP_r(buff_hash,buff_rece+octets_TLV_next+2);
		  if (rc!=0){
		    octets_envoie=4;
		    memset(buff_envoie,0,1024);
		    octets_envoie=TLV5(buff_envoie,octets_envoie);
		    creation_paquet(buff_envoie,octets_envoie);
		    len6=sizeof(from);
		    status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&from,len6);
		   
		    if(status<0){
		       perror("erreur");
		      printf("Recu : TLV Network hash : les hash ne correspondent pas : Echec Reponse\n");break; }
		    printf("Recu : TLV network hash : les hash ne correspondent pas---- Envoie: TLV Network State Request\n");break;
		  }
		  printf("Recu : TLV network hash : les hash correspondent \n");
		    

		  break;
		case 5:
		  //printf("Recu : TLV network state Request\n");
		  octets_envoie=4;
		  len6=sizeof(from);
		   memset(buff_envoie,0,1024);
		   rc=0;//On recycle rc
		  for (int j=0;j<td->nbr_donnee;j++){
		    octets_envoie=TLV6(buff_envoie,octets_envoie,td->debut[j]);
		    creation_paquet(buff_envoie,octets_envoie);
		    status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&from,len6);
		    octets_envoie=4;
		    memset(buff_envoie,0,1024);
		    rc++;
		     if(status<0){
		       printf("Recu : TLV Network state request : Envoie : TLV Node hash pour chaque donnee : Echec Reponse");break;
		     }
		     
		    
		  }
		  printf("NBR DONNEES : %d NODE HASH ENVOIE : %d\n",td->nbr_donnee,rc);
		  break;
		  
		
		case 6:
		  // printf("TLV Node hash\n");
		  len6=sizeof(from);
		  memset(buff_envoie,0,1024);
		  octets_envoie=4;
		  memset(buff_hash,0,16);
		  memset(id_annexe,0,8);
		  memset(node_h_annexe,0,16);
		  memcpy(node_h_annexe,buff_rece+octets_TLV_next+12,16);

		  memcpy(id_annexe,buff_rece+octets_TLV_next+2,8);
		  rc=0;//rc=1 sera notre test, si il change de valeur cela veut dire que la donnee ainsi que son hash sont deja present dans notre table de donnee
		  for (int j=0;j<td->nbr_donnee;j++){
		    if (memcmp(td->debut[j].id,id_annexe,8)==0){
		      hash_n(td->debut[j],buff_hash);
		      if (memcmp(node_h_annexe,buff_hash,16)==0){
			rc=2;
		      }else{
			rc=1;
		      };
		    }
		  }
		  if (rc!=2){  
		    Donnee d_ann;
		    memset(d_ann.id,0,16);
		    memcpy(d_ann.id,id_annexe,16);
		    octets_envoie=TLV7(buff_envoie,octets_envoie,d_ann);
		    creation_paquet(buff_envoie,octets_envoie);
		    status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&from,len6);
		    if (status<0){
		      printf("Recu: TLV node Hash  Envoie: TLV Node State Request Echec reponse\n");break;
		    }
		    if (rc==1){
		       printf("Recu: TLV node Hash ,les hash sont differents Envoie: TLV Node State Request\n");break;
		    }
		    if (rc==0){
		      printf("Recu: TLV node Hash ,la donnee n'appartient pas la table Envoie: TLV Node State Request\n");break;

		    }
		  }
		  printf("Recu: TLV node Hash ,les hash sont egaux\n");
		    break;
		case 7:
		  memset(buff_envoie,0,1024);
		  len6=sizeof(from);
		  octets_envoie=4;
		  unsigned char id_annexe1 [8];
		  memcpy(&id_annexe1,buff_rece+octets_TLV_next+2,8);
		  rc=1;//Sert de test comme TLV6
		  for (int j=0;j<td->nbr_donnee;j++){
		    if (memcmp(id_annexe1,td->debut[j].id,8)==0){
		      octets_envoie=TLV8(buff_rece,octets_envoie,td->debut[j]);
		      creation_paquet(buff_envoie,octets_envoie);
		      rc=0;
		    }
		  }
		  if (rc!=1){
		    status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&from,len6);
		    if (status<0){
		      printf("Recu: TLV node state request  Envoie: TLV Node State Echec reponse\n");break;
		    }
		    printf("Recu: TLV node state request  Envoie: TLV Node State \n");break;
		  }
		  if (rc==1){
		    printf("Recu: TLV node state request : Donnee pas dans la table\n");break;
		  }
		  break;
		case 8://A FIXER CORE DUMPED
		  //reinitialisation
		  len6=sizeof(from);
		  memset(buff_envoie,0,1024);
		  octets_envoie=4;
		  memset(id_annexe,0,8);
		  memset(node_h_annexe,0,16);
		  memset(buff_hash,0,16);
		  
		  //copie des champ de la node state
		  memcpy(node_h_annexe,buff_rece+octets_TLV_next+12,16);//Node_h contient le t de l'enoncé
		  memcpy(id_annexe,buff_rece+octets_TLV_next+2,8);
		  short sq_annexe;
		  memcpy(&sq_annexe,buff_rece+octets_TLV_next+10,2);
		  //Sq annexe contient le s de l'enonce
		  
		  rc=1;//rc=1 sera notre test, si il change de valeur cela veut dire que la donnee ainsi que son hash sont deja present dans notre table de donnee
		  status=-1;//On recycle status
		  for (int j=0;j<td->nbr_donnee;j++){
		    if (memcmp(td->debut[j].id,id_annexe,8)==0){
		      status=j;//On recycle status
		      hash_n(td->debut[j],buff_hash);
		      if (memcmp(node_h_annexe,buff_hash,16)==0){
			rc=0;//Les hashs sont identiques
			printf("Les hash sont egaux, rien a faire\n");
		      }
		    }
		  }
		  //cas ou les hashs sont differents
		  if (rc==1){
		    
		    //C'est mon ID
		    if (memcmp(&first.id,id_annexe,8)==0){
		       printf("C'est mon id  \n");
		      val=cmp_seq(td->debut[status].sq,sq_annexe);//on recycle val
		      if (val==-1){
			memset(&td->debut[status].sq,((td->debut[status].sq)+1)%65536,2);
			printf("RECU : NODE STATE :SEQUENCE (MY ID) maj \n");
			break;
		      }else{
			printf("RECU : NODE STATE :SEQUENCE (MY ID) > \n");
		      }
		      break; 
		      //ID d'un autre noeud
		    }else{
		      memcpy(tempd.id,id_annexe,8);
		      memcpy(&tempd.sq,&sq_annexe,2);
		      if ((buff_rece[octets_TLV_next+1]-26)<0){printf("erreur longueur chaine de données\n");break;}
		      memcpy(tempd.donnee,buff_rece+octets_TLV_next+28,buff_rece[octets_TLV_next+1]-26);
		      if (status==-1 ){
	       
			ajout_Do(td,tempd);
			printf("RECU : NODE STATE : NOUVELLE DONNE -- NBR DONNEES : %d\n",td->nbr_donnee);
			break;
			
		      }else{
			if (cmp_seq(sq_annexe,td->debut[status].sq) == -1){
			   printf("RECE : NODE STATE : SEQUENCE >= \n");break;
			}else{
			  suppr_Do(td,status);
			  ajout_Do(td,tempd);
			    printf("MISE A JOUR D'UNE DONNEE \n");
			  break;
			
			}
		      }//fin if status
		    }
		  }
		   
		    break;
		case 9: printf("TLV WARNING ERROR : %s\n",buff_rece+octets_TLV_next+6);break;
		}//fin switch
		octets_TLV_next=octet_next_TLV(buff_rece,octets_recu,octets_TLV_next);
	      
	      }//fin while octets_next
	       if(tv1.tv_sec-tv2.tv_sec>20){
		 printf("\nCela fait 20 secondes : Envoies automatiques\n\n");
		tablev_70s_maj(tablev);
		printf("[MAJ 70 SEC] NBR VOISINS-- %d \n",tablev->nbr_voisins);
		memset(buff_envoie,0,1024);
		octets_envoie=4;
		memset(&annexe,0,sizeof(annexe));
		len6=sizeof(annexe);
		if (tablev->nbr_voisins<5){ 
		  int random=rand()%tablev->nbr_voisins;
		  octets_envoie=TLV2(buff_envoie,octets_envoie);
		  creation_paquet(buff_envoie,octets_envoie);
		  memcpy(&annexe.sin6_addr,&tablev->table[random].ip,16);
		  memcpy(&annexe.sin6_port,&tablev->table[random].port,2);
		  annexe.sin6_family=AF_INET6;
		  status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&annexe,len6);
		  if(status<0){
		    perror("erreur");
		    printf("Moins de 5 voisins : Echec envoie TLV neighboor request\n");
		    exit(-12);
		  } 
		  printf("Moins de 5 voisins : Envoie de TLV2\n");
		}
		printf("..................\n");
    
	
		//Envoie TLV Network hash a tous les voisins
		memset(buff_hash,0,16);
		hash_r(td,buff_hash);
		for (int j=0;j<tablev->nbr_voisins;j++){
		  memset(buff_envoie,0,1024);
		  octets_envoie=4;
		  memset(&annexe,0,sizeof(annexe));
		  octets_envoie=TLV4(buff_envoie,octets_envoie,buff_hash);
		  creation_paquet(buff_envoie,octets_envoie);
		  memcpy(&annexe.sin6_addr,&tablev->table[j].ip,16);
		  memcpy(&annexe.sin6_port,&tablev->table[j].port,2);
		  annexe.sin6_family=AF_INET6;
		  status=sendto(s,buff_envoie,octets_envoie+4,0,(struct sockaddr*)&annexe,len6);
		  if(status<0){
		    perror("erreur");
		    printf("Echec envoie TLV network hash automatique\n");
		    exit(-12);
		  } 
		}
		printf("Envoie de %d Network hash automatique\n",tablev->nbr_voisins);	
		gettimeofday(&tv2,NULL);
		}
	        }
	  }//fin if HEADER
	}//fin fd ISSET
	memset(buff_rece,0,1024);
	memset(buff_envoie,0,1024);
	memset(&from,0,len6);
	memset(&annexe,0,len6);
	memset(buff_hash,0,16);
	octets_envoie=0;
	octets_recu=0;
	gettimeofday(&tv1,NULL);
  }//fin while
  return 0;
}//fin main

