//TLV/PAQUET 
#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
bool paquet_accept(char *paquet,int taille){
  if (taille<5){return false;} //Pas de contenu dans le paquet
  unsigned char magic=paquet[0];
  unsigned char version=paquet[1];
  short length; memmove(&length,paquet+2,2);length=ntohl(length);
  return (length>0 && magic ==95 && version==1);
}
short body_paquet_length(char *paquet,int taille){
  if (paquet_accept(paquet,taille)){
    short length;
        memmove(&length,paquet+2,2);
        length=ntohl(length);
        if (length>1020){
            return -1;
        }
        return length;
  }
  return -1;
}
int TLVtype(char *paquet,int taille,int debut){//taille=taille du paquet , debut=nbr d'octets avant le debut de la TLV
  if (!(debut>taille)){
    unsigned char type=paquet[debut];
    if(type>=0 && type<=9){
      return type;
    }
  }
  return -1;
}
int taille_TLV(char *paquet, int taille, int debut){// en octet
  int type=TLVtype(paquet,taille,debut);
  if (type ==0){ return 1;}
  if (type ==1 || type==8){
    unsigned char length;
    memmove(&length,paquet+debut+1,1);
    return 2 +length;
    if (type==2 || type==5){
      return 2;
    }
    if (type==3){
      return 20;
    }
    if (type==4){
      return 18;
    }
    if (type==6){
      return 28;
    }
    if (type==7){
      return 10;
    }
   
    return -1;
  }
  // a refaire, il y a pleins de cas ou ya pas de next
int octet_next_TLV(char *paquet,int taille, int debut){ //debut=debut de la TLV precedente
  return debut+taille_TLV(paquet,taille,debut);
}
int TLV0(char * paquet, int octets){ //retourne le nombre d'octets occupé dans le paquet a envoyé, sinon -1 : paquet =adresse du paquet / Octets = nombre d'octets deja occupé dans le paquet 
  if (octets<1024){//Sinon cela veut dire que le paquet est deja rempli
    memset(paquet+octets,0,1);
    return octets+1;
  }
  return -1; //la TLV0 n'a pas été ajouté au paquet
}

int TLV1(char * paquet, int octets,int length){//length = taille du body
  if (octets+1+length<1024){
    memset(paquet+octets,1,1);
    memset(paquet+octets+1,length,1);
    memset(paquet+octets+2,0,length);
    return octets+2+length;
  }
  return -1;   
}

int TLV2(char *paquet, int octets){
  if (octets+1<1024){
    memset(paquet+octets,2,1);
    memset(paquet+octets+1,0,1);
    return octets+2;
  }
  return -1;
}

int TLV3(char *paquet, int octets, Pair voisin){
  if (octets+19<1024){
    memset(paquet+octets,3,1);
    memset(paquet+octets+1,18,1);
    memmove(paquet+octets+2,&voisin.ip,16);
    memmove(paquet+octets+18,&voisin.port,2);
    return octets+20;
  }
  return -1;
}

int TLV4(char *paquet,int octets,char *h){
  if (octets+17<1024){
    memset(paquet+octets,4,1);
    memset(paquet+octets+1,16,1);
    memmove(paquet+octets+2,h,16);
    return octets+18;
  }
  return -1;
}
int TLV5(char *paquet,int octets){
  if (octets+1<1024){
    memset(paquet+octets,5,1);
    memset(paquet+octets+1,0,1);
    return octets+2;
  }
  return -1;
}
/*int TLV6(char *paquet,int octets,Donnee d){
  if (octets+27<1024){
    memset(paquet+octets,6,1);
    memset(paquet+octets+1,26,1);
    memmove(paquet+octets+2,&d.id,8);
    memmove(paquet+octets+10,&d.sq,2);
    memset(paquet+octets+12,HASH NOEUD,16);
    return octets+28;
  }
  return -1;
  }*/
int TLV7(char *paquet, int octets,unsigned long long id ){
  if (octets+9<1024){
    memset(paquet+octets,7,1);
    memset(paquet+octets+1,8,1);
    memset(paquet+octets+18,id,8);
    return octets+10;
  }
  return -1;
}
/*int make_TLV8(char *paquet,int octets,Donnee d){//a faire
    if ((octets+27+strlen(d.donnee))<1024 && strlen(d.donnee)<193) {
        memset(paquet+octets,8,1); 
        memset(paquet+octets+1,26+strlen(d.donnee),1);
        memmove(paquet+octets+2,d.id,8);
        memmove(paquet+octets+10,d.sq,2); 
        unsigned char res[16];
        if (h_noeud(data,res)==0){
            memmove(paquet+octets_occupes+12,res,16);//On ajoute le hachage du noeud
            memmove(paquet+octets_occupes+28,data.chaine,strlen(data.chaine));// On ajoute les data du noeud
            return octets+28+strlen(d.donnee);
        }
        else{
            perror("Erreur lors de h_noeud");
            return -1;
        }
    }
    return -1;
    }*/

int main(){
 
}
