//#include <zconf.h>
#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <time.h>
#include <assert.h>
#include <sys/time.h>
#include <netdb.h>
#include <stdbool.h>
typedef struct Pair{
  struct in6_addr ip;
  in_port_t port;
  bool perma;
  time_t date;
}Pair;

typedef struct t_voisin{
  Pair table [15];
  int nbr_voisins;
}t_voisin;
//Voisin


t_voisin *creat_t_voisin(){
  t_voisin *ret=malloc(sizeof(t_voisin));
  for (int i=0;i<15;i++){
    Pair a={NULL,0,false,NULL};
    ret->table[i]=a;
  }
  ret->nbr_voisins=0;
  return ret;
}




bool CMP(a, b) {return (a==b);}
int sockaddr_cmp(struct sockaddr *x, struct sockaddr *y)//Pas tester encore
{


    CMP(x->sa_family, y->sa_family);

    if (x->sa_family == AF_INET) {
        struct sockaddr_in *xin = (void*)x, *yin = (void*)y;
	bool r1= CMP(ntohl(xin->sin_addr.s_addr), ntohl(yin->sin_addr.s_addr)) & CMP(ntohs(xin->sin_port), ntohs(yin->sin_port));
	if (r1) {return true;}
    } else if (x->sa_family == AF_INET6) {
        struct sockaddr_in6 *xin6 = (void*)x, *yin6 = (void*)y;
        int r = memcmp(xin6->sin6_addr.s6_addr, yin6->sin6_addr.s6_addr, sizeof(xin6->sin6_addr.s6_addr));
        if (r != 0){
            return r;
	}
	bool r2=CMP(ntohs(xin6->sin6_port), ntohs(yin6->sin6_port)) & CMP(xin6->sin6_flowinfo, yin6->sin6_flowinfo) &  CMP(xin6->sin6_scope_id, yin6->sin6_scope_id);
	if (r2) {return true;}
    } else {
        assert(!"unknown sa_family");
    }
    return false;
}
bool same_adress(Pair p, struct sockaddr_in6 *a){
  struct in6_addr p_ip=p.ip;
  in_port_t p_port=p.port;
  struct sockaddr_in6 *b1;
  struct sockaddr_in6 *b2;
  b1->sin6_family=AF_INET6;
  b1->sin6_port=p.port;
  memcpy(&p.ip,&b1->sin6_addr,sizeof(struct in6_addr));
  b2->sin6_family=AF_INET;
  b2->sin6_port=p.port;
  memcpy(&p.ip,&b2->sin6_addr,sizeof(struct in6_addr));
  return ( sockaddr_cmp(a,b1) ||  sockaddr_cmp(a,b2));
}

void maj_tvoisin(t_voisin* tvoisin, struct sockaddr_in6 ajout, socklen_t taille){//Ip_ajout IP a tester recu par le client / Taille taille recu par le client
  if (tvoisin->nbr_voisins==15){ return;}
  for (int i=0;i<tvoisin->nbr_voisins;i++){ //Si deja dans la table on update sa date 
    if (same_adress(tvoisin->table[i],&ajout)){
      tvoisin->table[i].date=time (NULL);
      return;    }    
  }  //Pas la meme adresse donc on l'ajoute a la table en tant que membre non permanent
  int index_add=tvoisin->nbr_voisins;
  memset(&tvoisin->table[index_add],0,sizeof(tvoisin));
  tvoisin->table[index_add].perma=false;
  tvoisin->table[index_add].date=time (NULL);
  memcpy(&tvoisin->table[index_add].ip,&ajout,sizeof(struct in6_addr));
  tvoisin->table[index_add].port=ajout.sin6_port;
  tvoisin->nbr_voisins++;
}
void suppr_voisins(t_voisin *t , int index){
  Pair res[15];
  for (int i=index;i<t->nbr_voisins-1;i++){
    t->table[i]=t->table[i+1];
  }
  Pair a={NULL,0,false,NULL};
  t->table[t->nbr_voisins]=a;
}
typedef struct Donnee{
  unsigned long long id;
  unsigned short  sq;
  char donnee[192];
}Donnee;
struct tab_do{
  Donnee *debut;
  int nbr_donnee;
  int nbr_elm_max;
};
typedef struct tab_do *tabdo;

tabdo creer_td(){
#define CAPACITE 1000
  tabdo p = malloc(sizeof(struct tab_do));
  if( p == NULL)
    return NULL;
  p->debut = malloc( sizeof(Donnee) * CAPACITE );
  if(p->debut == NULL){
    free(p);
    return NULL;
  }
  p->nbr_donnee=0;
  p->nbr_elm_max=CAPACITE;
  return p;
}
int CMP_id(Donnee d,unsigned long long id_ajout){//A faire : Comparaison pour le tri selon l'id
  return 0;
}
int ajout_Do(tabdo t,unsigned long long id,unsigned short  s,char donnee[192]){//A faire : ajout d'un element dans la table des donnees / Agrandir si depasse taille avec realloc
  return 0;
}
void print_donnee(Donnee *d){//affiche les champs d'une donnee
}
void afficher_td(tabdo t){//affiche le tableau de donnee
  int i;
  Donnee *courant=t->debut;
  for(i=0;i<t->nbr_donnee; i++){
    print_donnee(courant);
    courant+=sizeof(Donnee);
  }
}
void detruire(tabdo t){//detruit le tableau
  free(t->debut);
  free(t);
}

int main(){
  
}
