PROJET JOUONS DANS LES GRAPHES 2019
===================================
Autheurs :
* Antoine DHERMAIN
* Nicolas JEULAND
* Grégoire REMY
* Massinissa YEKBA

Ce projet implémente en java plusieurs jeux utilisant des graphes, ainsi qu'un générateur de tels graphes.

Pour lancer le programme:
 -Sous linux:
	faire:1) chmod +x run.sh
	      2) ./run.sh
 -sous windows:
	double click sur run.bat
