package Model;
import java.util.LinkedList;


public class Noeud {
    private Coordonnee coordonnee;
    private final int  sommet;
    private String etiquette;
    private boolean special;

    transient private LinkedList<Noeud> listAdja;

    public String getEtiquette() {
        return etiquette;
    }

    public Coordonnee getCoordonnee() {
        return coordonnee;
    }

    public void setCoordonnee(Coordonnee coordonnee) {
        this.coordonnee = coordonnee;
    }

    public Noeud (){
        sommet=1;
        this.listAdja=new LinkedList<Noeud>();
        coordonnee=new Coordonnee();
    }

    public void setEtiquette(String etiquette) {
        this.etiquette = etiquette;
    }

    public Noeud(String etiq){
        this();
        etiquette=etiq;
    }

    public void setListAdja(LinkedList<Noeud> listAdja) {
        this.listAdja = listAdja;
    }

    public int getSommet(){
		return sommet;
	}
	public LinkedList<Noeud> getListAdja(){
		return listAdja;
	}

    public boolean isSpecial() {
        return special;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

}
