package Model;

public class Coordonnee {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Coordonnee(double abscisse, double ordonee){
        x=abscisse;
        y=ordonee;
    }
    public Coordonnee(){
        x=0;
        y=0;
    }
    @Override
    public String toString(){
        return ( "("+x+", "+y+")" );
    }
}
