package Model;

public abstract class Jeu {
    protected Graphe graphe;
    protected Joueur[] joueurs;



    public abstract boolean grapheIsOk(Graphe g);
    /**
     * methode qui return si le jeu permet le deplacement des noeuds
     * @return true si le jeu permet le deplacement, False sinon
     */
    public abstract boolean deplacementPermis();

    /**
     * methode qui return si le jeu permet la suppression des noeuds et des arretes
     * @return True si le jeu permet la suppression, False sinon
     */
    public abstract boolean suppressionPermise();
    /**
     * getter pour recuperer les joueurs
     * @return
     */
    public Joueur[] getJoueurs() {
        return joueurs;
    }

    /**
     * methode setter qui permet d'affecter au jeu un graphe compatible
     * @param graphe
     */
    public void setGraphe(Graphe graphe) {
        this.graphe = graphe;
    }
    /**
     * Cette methode détermine si le joueur j peut jouer jouer sur le noeud n avec la couleur
     * seléctionée , si cette méthode retun true alors la methode playing node sera appelée
     * @param j le joueur
     * @param n le noeud sur lequel le joueur veut jouer
     * @param selectedColor couleur du joueur qui a été selectioné
     * @return True si le joueur peut jouer, False sinon
     */
    public abstract boolean peutJouer(Joueur j, Noeud n, String selectedColor);
    public abstract void playingNode(Joueur j , Noeud selectedNode, String selectedColor);
    public abstract void consequence(Joueur j ,Noeud playedNode);
    /**
     * methode qui determin si la partie est termine car il y a un gagnat ou que la partie est bloquéé
     * @return True si la partie est terminée false sinon
     */
    public abstract boolean partieTerminee();

    /**
     * cette methode est appelé une fois la partie terminée et retourne le joueur gagnat
     * @return le joueurs qui a gagné
     */
    public abstract Joueur determinerGagnant();
    public abstract Joueur getCurrentPlayer();


}
