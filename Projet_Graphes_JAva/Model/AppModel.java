package Model;

import java.util.LinkedList;

public class AppModel {
    private LinkedList<Jeu> listeJeux;
    private LinkedList<Graphe> listeGraphes;

    public LinkedList<Jeu> getListeJeux() {
        return listeJeux;
    }

    public void setListeJeux(LinkedList<Jeu> listeJeux) {
        this.listeJeux = listeJeux;
    }

    public LinkedList<Graphe> getListeGraphes() {
        return listeGraphes;
    }

    public void setListeGraphes(LinkedList<Graphe> listeGraphes) {
        this.listeGraphes = listeGraphes;
    }

    public AppModel(){
        listeJeux =new LinkedList<>();
        listeGraphes =new LinkedList<>();
    }
    public void ajouterGraphe(Graphe g ){
        this.listeGraphes.add(g);
    }
}
