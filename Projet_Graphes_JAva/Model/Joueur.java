package Model;

public class Joueur {
    private String nom;
    private String [] couleurs;

    public Joueur(String n, String [] c){
        nom=n;
        couleurs=c;
    }
    public void setNom(String nom) {
    	this.nom=nom;
    }
    public String getNom(){
        return nom;
    }
    public String [] getColor(){
        return couleurs;
    }
    @Override
    public String toString(){
        return nom;
    }
}
