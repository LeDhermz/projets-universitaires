package Model;

import java.util.LinkedList;
import java.util.Stack;


public class Graphe{

	private LinkedList<Noeud> listNoeud;
    private int nbNoeud;
    private int nbArrete;
    private static int compteur=0;
    private String nom;

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public Graphe(String nom){
		this.listNoeud=new LinkedList<Noeud>();
		this.nbNoeud=0;
		this.nbArrete=0;
		this.nom = nom;
		compteur++;
	}
    public Graphe(){// Crée une liste de noeud qui representeront les sommets
    	this.listNoeud=new LinkedList<Noeud>();
    	this.nbNoeud=0;
    	this.nbArrete=0;
    	nom ="graphe"+Integer.toString(compteur);
    	compteur++;
    }
    public void ajouterNoeud(){// Ajoute un Noeud
    	Noeud n=new Noeud();
    	n.setEtiquette("blanc");
    	listNoeud.add(n);
    	nbNoeud+=1;
    }
	public void ajouterNoeud(boolean special){// Ajoute un Noeud
		Noeud n=new Noeud();
		n.setSpecial(special);
		if(special){
			n.setEtiquette("special");
		}else{
			n.setEtiquette("blanc");
		}
		listNoeud.add(n);
		nbNoeud+=1;
	}
    public void retirerNoeud(int sommet){// Retire un noeud
    	if(sommet-1>-1&&sommet-1<listNoeud.size()) {
    		listNoeud.remove(sommet-1);
    		nbNoeud-=1;
    	}
    }
    public void ajouterNoeud(String etiqNoeud){
    	listNoeud.add(new Noeud(etiqNoeud));
	}
    public void ajouterArete(int sommet1, int sommet2){ //Relie deux arretes
        if(sommet1==sommet2&&sommet1-1>-1&&sommet1-1<listNoeud.size()) {
        	listNoeud.get(sommet1-1).getListAdja().add(listNoeud.get(sommet2-1));
        	nbArrete+=1;
        	return;
        }
    	if(sommet1-1>-1&&sommet1-1<listNoeud.size()&&sommet2-1>-1&&sommet2-1<listNoeud.size()) {
        	listNoeud.get(sommet1-1).getListAdja().add(listNoeud.get(sommet2-1));
        	listNoeud.get(sommet2-1).getListAdja().add(listNoeud.get(sommet1-1));
        	nbArrete+=1;
        }
    }

	public void setListNoeud(LinkedList<Noeud> listNoeud) {
		this.listNoeud = listNoeud;
	}

	public int getNbArrete() {
		return nbArrete;
	}

	public void setNbArrete(int nbArrete) {
		this.nbArrete = nbArrete;
	}

	public void setNbNoeud(int nbNoeud) {
		this.nbNoeud = nbNoeud;
	}

	public void retirerArete(int sommet1, int sommet2){// Retire le lien entre deux arretes
    	if(sommet1==sommet2&&sommet1-1>-1&&sommet1-1<listNoeud.size()) {
        	listNoeud.get(sommet1-1).getListAdja().remove(listNoeud.get(sommet2-1));
        	nbArrete-=1;
        	return;
        }
    	if(sommet1-1>-1&&sommet1-1<listNoeud.size()&&sommet2-1>-1&&sommet2-1<listNoeud.size()) {
        	listNoeud.get(sommet1-1).getListAdja().remove(listNoeud.get(sommet2-1));
        	listNoeud.get(sommet2-1).getListAdja().remove(listNoeud.get(sommet1-1));
        	nbArrete-=1;
        }
    }
    public void ajouterArete(Noeud a , Noeud b){
    	a.getListAdja().add(b);
    	b.getListAdja().add(a);
    	nbArrete+=1;
	}
    public boolean estConnexe() {//Renvoie si un graphe est connexe grace a un parcours de profondeur
    	LinkedList<Noeud> sommetvisi = new LinkedList<Noeud>();
    	for(Noeud a:listNoeud) {
    		sommetvisi=parcoursProfondeur(this,a,sommetvisi);
    		if(sommetvisi.size()!=listNoeud.size()) {
        		return false;
        		}
    		}
    	return true;
    }
    public LinkedList<Noeud> parcoursProfondeur(Graphe g,Noeud a,LinkedList<Noeud> sommetvisi) {//Parcours en DFS un graphe
    	if(!sommetvisi.contains(a)) {
    		sommetvisi.add(a);
    	}
    	for(int i=0;i<a.getListAdja().size();i++) {
    		if(!sommetvisi.contains(a.getListAdja().get(i))) {
    			parcoursProfondeur(g,(Noeud) a.getListAdja().get(i),sommetvisi);
    		}
    	}
    	return sommetvisi;
    }
    public int[][] matAdja(){// Renvoie la matrice d'adja du graphe
    	int [][] mat=new int[listNoeud.size()][listNoeud.size()];
    	for(int i=0;i<this.listNoeud.size();i++) {
    		Noeud a=this.listNoeud.get(i);
    		for(int j=0;j<this.listNoeud.size();j++) {
    			if(a.getListAdja().contains(listNoeud.get(j))) {
    				mat[i][j]=1;
    			}
    			else{
    				mat[i][j]=0;
				}
    		}
    	}
    	return mat;
    }
    public Graphe AdjaToGraph(int[][] mat) {//Renvoie le graphe correspondant a matrice d'adja pris en argument
    	Graphe graph =new Graphe();
    	for(int i=0;i<mat.length;i++) {
    		graph.ajouterNoeud();
    	}
    	for(int i=0;i<mat.length;i++) {
    		for(int j=0;j<mat.length;j++) {
    			if(mat[i][j]==1) {
    				graph.ajouterArete(i+1,j+1);
    				((Noeud) graph.listNoeud.get(i)).getListAdja().add((Noeud) graph.listNoeud.get(j));
    				((Noeud) graph.listNoeud.get(j)).getListAdja().add((Noeud) graph.listNoeud.get(i));
    			}
    			if(mat[i][j]==2){
    			    graph.ajouterArete(i+1,j+1);
    			    ((Noeud) graph.listNoeud.get(i)).getListAdja().add((Noeud) graph.listNoeud.get(j));
    			    ((Noeud) graph.listNoeud.get(j)).getListAdja().add((Noeud) graph.listNoeud.get(i));
    			     graph.listNoeud.get(j).setSpecial(true);
                }
    		}
    	}
    	return graph;
    }
    public boolean verfiMatAdja(String entered){
        int a = entered.length();
        String [] tab = new String[a];

        for(int i = 0; i < a;i++){
            tab[i] = entered.valueOf(entered.charAt(i));
        }
        if(!tab[0].equals("[")||!tab[1].equals("[")||!tab[entered.length()-1].equals("]")||!tab[entered.length()-2].equals("]")){
            return false;
        }
        a=0;
        int b=0;
        for(int i= 0;i<tab.length;i++){
            if (tab[i].equals("[")){
                a+=1;
            }
            if (tab[i].equals("]")){
                b+=1;
            }
        }
        if(a!=b) {
            return false;
        }
        for(int i=1;i<tab.length-1;i++){
            if(tab[i].equals("[")) {
                b=-1;
                while(!tab[i].equals("]")&&i<tab.length-1) {
                    b+=1;
                    i+=1;
                    if(tab[i].equals("[")) {
                        return false;
                    }
                }
                if(a-1!=b) {
                    return false;
                }
            }
        }
        for(int i=1;i<tab.length-1;i++){
            if(!tab[i].equals("[")&&!tab[i].equals("]")&&!tab[i].equals("0")&&!tab[i].equals("1")&&!tab[i].equals("2")){
                return false;
            }
        }
        return true;
    }
	public int[][] convertStringMat(String entered){
        int a = entered.length();
        String [] tab = new String[a];

        for(int i = 0; i < a;i++){
            tab[i] = entered.valueOf(entered.charAt(i));
        }
        a=0;
        int b=0;
        for(int i= 0;i<tab.length;i++){
            if (tab[i].equals("[")){
                a+=1;
            }
            if (tab[i].equals("]")){
                b+=1;
            }
        }
        int[][] mat=new int[a-1][a-1];
        a=-1;
        for(int i=1;i<tab.length-1;i++){
            if(tab[i].equals("[")) {
                a+=1;
                b=0;
            }
            if(!tab[i].equals("[")&&!tab[i].equals("]")) {
                mat[a][b]=Integer.parseInt(tab[i]);
                b+=1;
            }
        }
        return mat;
	}

    public int getNbNoeud(){
    	return nbNoeud;
	}
	public boolean aConnectedToB(Noeud a,Noeud b){//Renvoie si des sommets sont connectés
		if(!this.listNoeud.contains(a) || !this.listNoeud.contains(b)) {
			return false;
		}
		Stack<Noeud> pile=new Stack<>();
		LinkedList<Noeud> visited=new LinkedList<>();
		pile.push(a);
		visited.add(a);
		while(!pile.empty()){
			Noeud poped=pile.pop();
			if(poped.equals(b))
				return true;
			for(Noeud n : poped.getListAdja()){
				if (n.equals(b)){
					return true;
				}
				if(!pile.contains(n) && !visited.contains(n)){
					pile.push(n);
					visited.add(n);
				}
			}
		}

		return false;
	}
	public void supprimerNoeud(Noeud n){
    	this.listNoeud.remove(n);
    	for (Noeud adjacents: n.getListAdja()){
    		adjacents.getListAdja().remove(n);
		}
	}
	public LinkedList<Noeud> getListNoeud() {
		return listNoeud;
	}
	public Noeud getNoeud(int indice){
    	return this.listNoeud.get(indice);
	}
	public void retirerNoeud(Noeud noeud){
    	listNoeud.remove(noeud);
    	for (Noeud node : listNoeud){
    		node.getListAdja().remove(noeud);
		}
	}
	public void retirerArete(Noeud a, Noeud b){
    	a.getListAdja().remove(b);
    	b.getListAdja().remove(a);
	}
	@Override
	public String toString(){
    	return nom;
	}

}
