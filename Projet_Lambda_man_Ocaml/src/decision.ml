(**

   Chers programmeuses et programmeurs de λman, votre mission consiste
   à compléter ce module pour faire de vos λmen les meilleurs robots
   de la galaxie. C'est d'ailleurs le seul module qui vous pouvez
   modifier pour mener à bien votre mission.

   La fonction à programmer est

         [decide : memory -> observation -> action * memory]
e
   Elle est appelée à chaque unité de temps par le Λserver avec de
   nouvelles observations sur son environnement. En réponse à ces
   observations, cette fonction décide quelle action doit effectuer le
   robot.

   L'état du robot est représenté par une valeur de type [memory].  La
   fonction [decide] l'attend en argument et renvoie une nouvelle
   version de cette mémoire. Cette nouvelle version sera passée en
   argument à [decide] lors du prochain appel.

*)

open World
open Space

(** Le Λserver transmet les observations suivantes au λman: *)
type observation = World.observation

(** Votre λman peut se déplacer : il a une direction D et une vitesse V.

    La direction est en radian dans le repère trigonométrique standard :

    - si D = 0. alors le robot pointe vers l'est.
    - si D = Float.pi /. 2.  alors le robot pointe vers le nord.
    - si D = Float.pi alors le robot pointe vers l'ouest.
    - si D = 3 * Float.pi / 2 alors le robot pointe vers le sud.
    (Bien entendu, ces égalités sont à lire "modulo 2 * Float.pi".)

    Son déplacement en abscisse est donc V * cos D * dt et en ordonnée
   V * sin D * dt.

    Votre λman peut communiquer : il peut laisser du microcode sur sa
   position courante pour que d'autres λmen puissent le lire.  Un
   microcode s'autodétruit au bout d'un certain nombre d'unités de
   temps mais si un microcode est laissé près d'un autre microcode
   identique, ils fusionnent en un unique microcode dont la durée de
   vie est le somme des durées de vie des deux microcodes initiaux.
   Construire un microcode demande de l'énergie au robot : chaque
   atome lui coûte 1 point d'énergie. Heureusement, l'énergie augmente
   d'1 point toutes les unités de temps.

    Pour terminer, votre λman peut couper des arbres de Böhm. Les
   arbres de Böhm ont un nombre de branches variables. Couper une
   branche prend une unité de temps et augmente le score de 1
   point. Si on ramène cette branche au vaisseau, un second point est
   accordé.

    Pour finir, le monde est malheureusement très dangereux : on y
   trouve des bouches de l'enfer dans lesquelles il ne faut pas tomber
   ainsi que des champs de souffrances où la vitesse de votre robot
   est modifiée (de -50% à +50%).

*)

type action =
  | Move of Space.angle * Space.speed
  (** [Move (a, v)] est l'angle et la vitesse souhaités pour la
     prochaine unité de temps. La vitesse ne peut pas être négative et
     elle ne peut excéder la vitesse communiquée par le serveur. *)

  | Put of microcode * Space.duration
  (** [Put (microcode, duration)] pose un [microcode] à la position courante
      du robot. Ce microcode s'autodétruira au bout de [duration] unité de
      temps. Par contre, s'il se trouve à une distance inférieure à
      [Space.small_distance] d'un microcode similaire, il est fusionné
      avec ce dernier et la durée de vie du microcode résultant est
      la somme des durées de vide des deux microcodes. *)

  | ChopTree
  (** [ChopTree] coupe une branche d'un arbre de Böhm situé une distance
      inférieure à [Space.small_distance] du robot. Cela augmente le score
      de 1 point. *)

  | Wait
  (** [Wait] ne change rien jusqu'au prochain appel. *)

  | Die of string
  (** [Die] est produit par les robots dont on a perdu le signal. *)

[@@deriving yojson]

(**

   Le problème principal de ce projet est le calcul de chemin.

   On se dote donc d'un type pour décrire un chemin : c'est une
   liste de positions dont la première est la source du chemin
   et la dernière est sa cible.

*)
type path = Space.position list

(** Version lisible des chemins. *)
let string_of_path path =
  String.concat " " (List.map string_of_position path)

(**

   Nous vous proposons de structurer le comportement du robot
   à l'aide d'objectifs décrits par le type suivant :

*)
type objective =
  | Initializing            (** Le robot doit s'initialiser.       *)
  | Chopping                (** Le robot doit couper des branches. *)
  | GoingTo of path * path
  (** Le robot suit un chemin. Le premier chemin est la liste des
      positions restantes tandis que le second est le chemin initial.
      On a donc que le premier chemin est un suffixe du second. *)

(** Version affichable des objectifs. *)
let string_of_objective = function
  | Initializing -> "initializing"
  | Chopping -> "chopping"
  | GoingTo (path, _) ->
     Printf.sprintf
       "going to %s" (String.concat " " (List.map string_of_position path))

(**

  Comme dit en introduction, le robot a une mémoire qui lui permet de
   stocker des informations sur le monde et ses actions courantes.

  On vous propose de structurer la mémoire comme suit:

*)
type memory = {
    known_world : World.t option;      (** Le monde connu par le robot.     *)
    graph       : Graph.t;             (** Un graphe qui sert de carte.     *)
    objective   : objective;           (** L'objectif courant du robot.     *)
    targets     : Space.position list; (** Les points où il doit se rendre. *)
}

(**

   Initialement, le robot ne sait rien sur le monde, n'a aucune cible
   et doit s'initialiser.

*)
let initial_memory = {
    known_world = None;
    graph       = Graph.empty;
    objective   = Initializing;
    targets     = [];
}

(**

   Traditionnellement, la fonction de prise de décision d'un robot
   est la composée de trois fonctions :

   1. "discover" qui agrège les observations avec les observations
      déjà faites dans le passé.

   2. "plan" qui met à jour le plan courant du robot en réaction
      aux nouvelles observations.

   3. "next_action" qui décide qu'elle est l'action à effectuer
       immédiatement pour suivre le plan.

*)

(** [discover] met à jour [memory] en prenant en compte les nouvelles
    observations. *)
let discover visualize observation memory =
  let seen_world = World.world_of_observation observation in
  let known_world =
    match memory.known_world with
    | None -> seen_world
    | Some known_world -> World.extends_world known_world seen_world
  in
  if visualize then Visualizer.show ~force:true known_world;
  { memory with known_world = Some known_world }


(**

   Pour naviguer dans le monde, le robot construit une carte sous la
   forme d'un graphe.

   Les noeuds de ce graphe sont des positions clées
   du monde. Pour commencer, vous pouvez y mettre les sommets des
   polygones de l'enfer, le vaisseau, le robot et les arbres.

   Deux noeuds sont reliés par une arête si le segment dont ils
   sont les extremités ne croisent pas une bouche de l'enfer.

 *)
let sq x = x *. x

let dist1 (x, y) (x', y') = sqrt (sq (x' -. x) +. sq (y' -. y))

let calcul_angle_from_to (p1:position) (p2:position) = Space.angle_of_float ( atan2 (Space.y_ p2 -. Space.y_ p1) (Space.x_ p2 -. Space.x_ p1))

let inv_angle angle =
  angle_of_float(float_of_angle angle +. Float.pi)

let enlarger_rectangle polygon = (* renvoie une liste de sommets correspondants aux coordonnées d'un rectangle légèrement + grand que l'input*)
  let vertices = Space.vertices polygon in
  let aux (v:position list) =
    match v with
    |[] -> failwith"polygone sans sommet"
    |x::y::z::[t] -> let angle_d1 = calcul_angle_from_to x z in
                   let angle_d2 = calcul_angle_from_to y t in
                   (Space.move x (inv_angle angle_d1) (Space.speed_of_float 2.)) :: (Space.move y (inv_angle angle_d2) (Space.speed_of_float 2.)) :: (Space.move z angle_d1 (Space.speed_of_float 2.)) :: [(Space.move t angle_d2 (Space.speed_of_float 2.))]
    |_-> failwith"Mauvais match"
  in
  aux vertices

let is_Hell a =
  match a with
  |Hell -> true
  |_ ->false
let add_node_hell observation memory graph =
  let aux around graph =
    let vertices_of_hell = List.flatten(List.map enlarger_rectangle (Space.polygons around is_Hell)) in
    let rec aux2 graph vertices =
      match vertices with
      |[] -> graph
      |p::q -> aux2 (Graph.add_node graph p) q
    in
    aux2 graph vertices_of_hell
  in
  aux observation.around graph

let rec link_with node list graph around =
  let segments_of_hell = List.flatten (List.map Space.polygon_segments (Space.polygons around is_Hell)) in
  match list with
  |[] -> graph
  |x::l -> let check segment = Space.segment_intersects (node,x) segment in
           if List.for_all (fun x -> x==false) (List.map check segments_of_hell) then link_with node l (Graph.add_edge graph (node,x,dist1 node x) ) around
           else link_with node l graph around

let create_edges  observation memory graph =
  let rec aux list graph around =
    match list with
    |[] -> graph
    |n::q -> aux q (link_with n q graph around) around
  in
  aux (Graph.nodes graph) graph observation.around 

let put_nodes observation memory =
  let rec aux graph trees =
    match trees with
    |[] -> graph
    |x::xs -> aux (Graph.add_node graph x.tree_position) xs
  in
  let avec_arbre = aux Graph.empty observation.trees in
  let avec_position = Graph.add_node avec_arbre observation.position in
  let avec_vaisseau = Graph.add_node avec_position observation.spaceship in
  
  add_node_hell observation memory avec_vaisseau

let visibility_graph observation memory =
  let graph_with_nodes= put_nodes observation memory in
  create_edges observation memory graph_with_nodes


(**

   Il nous suffit maintenant de trouver le chemin le plus rapide pour
   aller d'une source à une cible dans le graphe.

*)
module Noeud_Queue = PriorityQueue.Make
                       (
                         struct
                           type t = position
                           let compare = compare
                         end
                       )
                       (
                         struct
                           type t = distance
                           let compare = compare
                           let infinity = Distance Float.infinity
                           let to_string (Distance dist) = string_of_float dist
                         end
                       )

module MapAncetre = Map.Make
                      (
                        struct
                          type t = position
                          let compare = compare
                        end
                      )

module MapDistance = Map.Make
                       (
                         struct
                           type t = position
                           let infinity = Distance Float.infinity
                           let compare = compare
                         end
                       )

let init graph source target =
  let f l =
    let rec aux l2=
      match l2 with
      | [] -> []
      | x::xs  when x == source -> Distance 0. :: aux xs 
      | x::xs -> Distance Float.infinity :: aux xs
    in aux l
  in
  let rec fold_add l1 l2 map =
    match l1, l2 with
    |[],_ -> map
    |x::xs,y::ys -> fold_add xs ys (MapDistance.add x y map)
    | _, _-> map
  in

  let mapD  =
    fold_add  (Graph.nodes graph)  (f (Graph.nodes graph))  MapDistance.empty 
  in
  
  let pQ =
    let rec fold_insert l1 l2 map =
      match l1, l2 with
      |[],_ -> map
      |x::xs,y::ys -> fold_insert xs ys (Noeud_Queue.insert map x y )
      | _, _-> map
    in
    fold_insert  (Graph.nodes graph)  (f (Graph.nodes graph))  Noeud_Queue.empty 
  in
  
  let mapA  =
    let ajout map s =
      MapAncetre.add s target map
    in
    graph |> Graph.nodes |> List.fold_left ajout MapAncetre.empty
  in (pQ, mapD, mapA)
 
   
let maj_distances s1 s2 (pQ, mapD, mapA) =
  let distToFloat d  =
    match d with
    |Distance x -> x
  in 
  let edgeConv a =
    match a with
    |(x,y,z) when x == s1 -> (y , z)
    |(x,y,z) -> (x , z)
  in
  let (s2, poids) = edgeConv s2 in 
  if MapDistance.find s2 mapD > Distance (distToFloat (MapDistance.find s1 mapD) +.  poids) 
  then
    Noeud_Queue.decrease pQ s2 (Distance(distToFloat(MapDistance.find s1 mapD) +.poids)),
    MapDistance.add s2 (Distance(distToFloat(MapDistance.find s1 mapD) +. poids)) mapD ,
    MapAncetre.add s2 s1 mapA
  else (pQ, mapD, mapA)
  
let dijkstra g s0 (pQ, mapD, mapA) =
  let rec fold_maj s l (x,y,z) =
    match l with
      [] -> (x,y,z)
    | u :: v -> fold_maj s v (maj_distances s u (x, y, z))
  in
  let noeudToposition s =
    match s with
    | None-> (0. , 0.)
    |Some (_, k) -> k
  in
    let rec aux g so (pQ, mapD, mapA) =
      match Noeud_Queue.length pQ with
      | 0 -> mapA
      | _ -> let s1 = Noeud_Queue.get_min pQ in
             let pQ = Noeud_Queue.remove_min pQ in
             aux g so (fold_maj(noeudToposition s1) (Graph.out g (noeudToposition s1)) (pQ, mapD, mapA))
    in aux g s0 (pQ, mapD, mapA)
     
  
let shortest_path graph source target : path =
  let return_path s0 sn g =
    let rec last l mapUtile sn s0 =
      match sn with
        x when x==s0 -> l
      | _ ->  last (sn::l) mapUtile (MapAncetre.find sn mapUtile) s0
    in last [] (dijkstra g s0 (init graph source target)) sn s0
  in return_path source target graph


(**

   [plan] doit mettre à jour la mémoire en fonction de l'objectif
   courant du robot.

   Si le robot est en train de récolter du bois, il n'y a rien à faire
   qu'attendre qu'il est fini.

   Si le robot est en train de suivre un chemin, il faut vérifier que
   ce chemin est encore valide compte tenu des nouvelles observations
   faites, et le recalculer si jamais ce n'est pas le cas.

   Si le robot est en phase d'initialisation, il faut fixer ses cibles
   et le faire suivre un premier chemin.
 *)


(*met en target les arbres avec des branches, peut être qu'on rajoutera aussi les microcodes*)
let refresh_target (observation:World.observation)  =
  let compare  p1 p2 =
    let robot_pos = observation.position in
                      if dist1 p1 robot_pos > dist1 p2 robot_pos then 1
                      else if  dist1 p1 robot_pos < dist1 p2 robot_pos then -1
                      else 0
    in 
  let rec aux list acc =
    match list with
    |[] -> acc
    |x::xs -> match x.branches with
              | 0 -> aux xs acc
              | _ -> aux xs (x.tree_position :: acc)
  in
  let tri = List.sort compare (aux observation.trees [] ) in
  tri @ [ observation.spaceship ] 

let close_to_target observation memory =
  let target = match memory.objective with
    |GoingTo(l1,l2) -> List.hd l1
    |_ -> failwith"On s'attendait a un objectif GoingTo" in
  Space.close observation.position target 1.

let path_still_good (path :path) observation memory =
  let liste_edges = Graph.edges memory.graph in
  List.exists (fun x -> x=(observation.position, List.hd path, dist1 (observation.position)( List.hd path)) ) liste_edges

let plan visualize (observation :World.observation)  memory =
  let memory= {memory with graph = visibility_graph observation memory} in
  match memory.objective with
  |Chopping -> memory
  |Initializing -> let newtarget=refresh_target observation in
                   {memory with objective = GoingTo(newtarget,newtarget);targets=newtarget}
  |GoingTo(p,pp) -> if path_still_good p observation memory then memory
                    else if (close_to_target observation memory) then
                      (*let sp= shortest_path (memory.graph) (observation.position) (List.hd memory.targets ) in 
                      let () = Printf.eprintf " %s\n%! " (string_of_path sp) in *)
                      {memory with objective = match memory.objective with
                                               |GoingTo(x::xs,m)-> GoingTo(xs,m)
                                               |_-> failwith"erreur404" }
                    else
                      let sp= shortest_path (memory.graph) (observation.position) (List.hd memory.targets) in
                      {memory with objective = GoingTo(sp,sp) }
                              

(**

   Next action doit choisir quelle action effectuer immédiatement en
   fonction de l'objectif courant.

   Si l'objectif est de s'initialiser, la plannification a mal fait
   son travail car c'est son rôle d'initialiser le robot et de lui
   donner un nouvel objectif.

   Si l'objectif est de couper du bois, coupons du bois! Si on vient
   de couper la dernière branche, alors il faut changer d'objectif
   pour se déplacer vers une autre cible.

   Si l'objectif est de suivre un chemin, il faut s'assurer que
   la vitesse et la direction du robot sont correctes.

 *)
let tree_is_close observation =
  match World.tree_at observation.trees observation.position with
  |None->false
  |Some _ ->true



let calcul_speed observation = observation.max_speed

let get_closed_tree observation =
  match World.tree_at observation.trees observation.position with
  |None -> failwith"On s'attendait à un arbre ici"
  |Some tree-> tree
  

let next_action visualize observation memory =
  match memory.objective with
  |Initializing -> failwith"Erreur à l'initialisation"
  |GoingTo([],_) -> failwith"Le robot n'a plus de target"
  |GoingTo(x::xs,m) -> if tree_is_close observation then
                         Move(Space.angle_of_float 0. , Space.speed_of_float 0.),{memory with objective=Chopping }
                       else if (close_to_target observation memory) then Move(calcul_angle_from_to observation.position (List.hd xs),calcul_speed observation) ,{memory with objective = GoingTo(xs,m) }
                       else Move(calcul_angle_from_to observation.position x,calcul_speed observation),memory
                               
  |Chopping -> if (get_closed_tree observation).branches <> 0 then ChopTree,memory
               else let new_target = refresh_target observation in
                    let first_target = match new_target with
                      |[]-> failwith"Y'a plus d'objectif"
                      |x::xs->x in
                    Move(calcul_angle_from_to observation.position first_target,calcul_speed observation),{memory with objective=GoingTo(new_target,new_target);targets=new_target}
   

(**

   Comme promis, la fonction de décision est la composition
   des trois fonctions du dessus.

*)
let decide visualize observation memory : action * memory =
  let memory = discover visualize observation memory in
  let memory = plan visualize observation memory in
  let () = Visualizer.show_graph memory.graph in
  next_action visualize observation memory
