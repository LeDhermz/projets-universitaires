PROJET LAMBDA-MAN 2019
============================================
*Projet de programmation fonctionnelle 2019*

Un robot doit récolter un maximum de ressource sur une carte en un temps minimum, tout en évitant des obstacles.
Nous programmons ici l'algorithme qui cherche le plus court chemin passant par toutes les ressources, et qui s'adapte à la découverte de nouveaux obstacles.

Auteurs : 
* Antoine DHERMAIN
* Etienne LAFOND


Les tâches 1 et 2 ont été intégralement traitées.
Le robot peut également évaluer dans une carte munie de champs de souffrances, mais son parcours peut ne pas être optimisé.
