# -*- coding: utf-8 -*-
"""
Created on Mon Jan  3 21:34:13 2022

@author: antoi
"""

class Compteur:
    '''
    Classe compteur, on s en sert pour pouvoir utiliser un compteur 
    plusieurs fois en argument de fonction, et le modifier DANS la fonction.
    En effet, lorsqu'on appelle une fonction sur un int, la fonction prend en fait en argument
    une COPIE de cet int. Dès lors, modifier l'int dans la fonction n'a pas d'effet sur l'int
    donné en argument. 
    Pour remédier au problème, on donne en argument un OBJET compteur à la place d'un int.
    On pourra donc modifier efficacement cet objet compteur dans notre fonction.
    
    Attribut : value 
    methods : add(x),sub(x)
    '''
    def __init__(self,val=0):
        self.value=val
    
    def set_val(self,val):
        self.value=val
    
    def add(self, val):
        self.value+=val
    
    def get_val(self):
        return(self.value)
    
    def sub(self,val):
        self.value-=val
   
    def print(self):
        print(self.value)