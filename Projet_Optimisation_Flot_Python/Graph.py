# -*- coding: utf-8 -*-

################################  Graphs  ################################## 
import List
from random import randint as rd
from scipy.special import binom 



class Adj_list(List.Linked_list): 
    '''
    class for a adjacency list
    
    @attributes (inherited from Linked_list) :
    - head : Frame1 or None
    
    @methods :
    methods inherited from Linked_list :
    - is_empty() : return true is the queue is empty, false otherwise
    - append(x) : add the data x at the head 
    - pop() : if the linked list is not empty, pop the head, else do nothing and return None
    
    
    @new method
    - print() : print the list of node name
    
    @requires : class Linked_list
'''

    def print(self):  # surcharge la methode print des listes pour afficher les noms des noeuds
        print("Liste d'adjacence = ",end='')
        current=self.head
        while current:
            edge=current.data
            print(edge.head.name,',',edge.w, end=' ; ')
            current=current.next
        print()    


class Node: 
    '''
    Class for nodes of a graph
    
    @attributes :
    - index : index of the node in the graph list
    - name : string (default '')
    - adj : Adj_list of Edge
    - visited : True or False

    @requires : class Adj_list
    '''
    def __init__(self,index=0,name=''):
        self.index = index
        self.name = name
        self.adj = Adj_list()
        self.visited=False
    def print(self):
        print(self.index)


class Edge: 
    '''
    Class for edges of a graph
        
    @attributes :
        - head : Node   # dans l'arc (x,y), y est nommé head en anglais (tete de la fleche), et x tail (queue de la fleche)
        - w : weight
    
    @requires : None
    '''
    def __init__(self,n,w=1,ret=False):
        self.head = n  
        self.w = w


class Graph:
    '''
    Class for graph
    
    @attributes :
    - nodes : list (instance of list class) of Node  (default empty list)
    - source : node
    - puit : node
    
    @methods :
    - add_edge(i,j,w=1) : if i and j are not out of range, add an edge between i and j with the weight w (default edge = 1), do nothing otherwise
    - set_non_visited(self): #mettre tous les noeuds à non visité
    - get_edge(self,u,v): #renvoie le edge de u à v
    - set_source(self,index):
    - set_puits(self,index):
    - add_edge(self,i,j,w=1):
    - print_adj_list(self):
    - add_edge_list(self,E):
    - are_linked(self,i_u,i_v): renvoie true si il existe un edge entre u et v ou v et u
    - to_txt(self, name="MonGraphe.txt"): tranforme un graphe en fichier txt
    
    @requires : classes Node, Edge and Adj_list
    '''
    def __init__(self,l=[],i_source=0,i_puits=-1):  # l est optionnel : liste ne noms de noeuds
        self.nodes=list()
        for i in range(len(l)):
            n=Node(i,l[i])
            self.nodes.append(n)
        self.source=self.nodes[i_source]
        self.puit=self.nodes[i_puits]
    
    def set_non_visited(self): #mettre tous les noeuds à non visité
        for e in self.nodes:
            e.visited=False
    
    def get_edge(self,u,v): #renvoie le edge de u à v
        frame_edge=u.adj.head
        while frame_edge:
            if frame_edge.data.head.index==v.index:
                return frame_edge.data
            else : 
                frame_edge=frame_edge.next
        return None
    
    
    def set_source(self,index):
        self.source=self.nodes[index]
        
    def set_puits(self,index):
        self.puits=self.nodes[index]
    
    def add_edge(self,i,j,w=1):
        if i<len(self.nodes) and j<len(self.nodes):
            e=Edge(self.nodes[j],w)
            self.nodes[i].adj.append(e)
    
    def suppr_edge(self,u,v):
        nod=self.nodes[u.index]
        nod.adj.suppr(v)
        nod=self.nodes[v.index]
        nod.adj.suppr(u)
        
    
    def print_adj_list(self):
        for n in self.nodes:
            print('Noeud :', n.index,n.name )
            n.adj.print()
    
    def add_edge_list(self,E):
        for e in E:
            if len(e)==2:
                self.add_edge(e[0],e[1])
            if len(e)>2 and e[2]>0:
                self.add_edge(e[0],e[1],e[2])
                self.add_edge(e[1],e[0],0) #On ajoute l'arete retour en l'initialisant à 0
    
    def add_edge_list_bis(self,E):
        for e in E:
            if len(e)==2:
                self.add_edge(e[0],e[1])
            if len(e)>2 and e[2]>0:
                if self.get_edge(self.nodes[e[1]],self.nodes[e[0]]) is not None:
                    '''Si il y a une arete qui relie e[1] à e[0], alors on introduit un noeud
                    auxiliaire qui servira de transition entre e[0] et e[1].
                    Ainsi, dans le graphe donné à l'algo, le cas (u,v) et (v,u) N'EXISTE PAS
                    '''
                    node_aux=Node(len(self.nodes))
                    self.nodes.append(node_aux)
                    self.add_edge(e[0], len(self.nodes)-1,e[2])
                    self.add_edge(len(self.nodes)-1,e[1],e[2])
                    self.add_edge(e[1], len(self.nodes)-1, 0) #on rajoute les aretes retour
                    self.add_edge(len(self.nodes)-1, e[0], 0)
                     
                else:
                    self.add_edge(e[0],e[1],e[2])
                    self.add_edge(e[1],e[0],0) #On ajoute l'arete retour en l'initialisant à 0
        
    def are_linked(self,i_u,i_v):
        return((self.get_edge(self.nodes[i_u], self.nodes[i_v]) is not None or (self.get_edge(self.nodes[i_v], self.nodes[i_u]) is not None)))
    
    #Crée une donnée .txt depuis un objet Graph
    def to_txt(self, name="MonGraphe.txt"):
        with open(name,mode='w', encoding='utf-8-sig') as file:
            file.write(str(len(self.nodes))+"\n") 
            for n in self.nodes :
                frame_edge=n.adj.head
                while frame_edge :
                    i=n.index
                    j=frame_edge.data.head.index
                    w=frame_edge.data.w
                    file.write(str(i)+";"+str(j)+";"+str(w)+"\n")
                    frame_edge=frame_edge.next

'''
    La fonction ci dessous est essentielle lorsqu'on souhaite calculer le flot max
    d'un fichier.txt, car c'est elle qui va l'instancier en un graph, en ajoutant les aretes
    retour et en ajoutant des noeuds pour supprimer le cas (u,v) et (v,u)
    #Crée un objet Graph depuis une donnée de graph en .txt
    #On rajoute les 'arètes retour' initialisées à 0
'''

def txtToGrp(fichier):
    E=list()
    with open(fichier, mode='r', encoding='utf-8-sig') as fp:
        # ouverture en lecture d'un fichier codé en utf-8 avec BOM
        for i,line in enumerate(fp): # line est un string qui contient une ligne du fichier
            line.strip() # la méthode strip() retire les caractères non imprimable
            if i==0:
                if line:  # teste si line est vide
                            n=int(line) # int() convertit un string en entier --> le nb de sommets
                else:
                    raise SystemExit(1)  # provoque la sortie du programme  
            else:        
                        L=line.split(';') # decoupe line selon le séparateur ';'
                        if len(L)==3:  # elimine les lignes mal formees
                            E.append([int(L[0]), int(L[1]),float(L[2])])
    graph=Graph(range(n))
    graph.add_edge_list_bis(E)
    #graph.print_adj_list()
    return(graph)

#txtToGrp("exemple3_graphe.txt").print_adj_list()

'''
    Crée un objet Graph, les arètes sont distribuées aléatoirement avec
    un 1 <= poids <= max_weight. Le graphe créé n'aura pas (u,v) et (v,u),
    donc il faut borner le nb d'arete par binom(nb_node,2) ie 2 parmi nb_node.
    Le grahe n'aura pas non plus deux fois la meme arete.
'''
#            
def create_graph(nb_node,nb_arr,max_weight=15):
    if nb_arr>binom(nb_node,2) :
        print("Plus d'arète que de paires de noeuds")
        raise SystemExit(1)
    graph=Graph(range(nb_node))
    for k in range(nb_arr):
        arr=rd(0,nb_node-1)
        dep=rd(0,nb_node-1)
        while arr==dep or graph.are_linked(arr,dep):
            arr=rd(0,nb_node-1)
            dep=rd(0,nb_node-1)
        graph.add_edge(dep, arr,rd(1,max_weight))
        graph.add_edge(arr, dep,0)
    return graph
#create_graph(12000, 100000,max_weight=100).to_txt()

'''
    Créée une donnée .txt modélisant un graphe. Le graphe en question pourra avoir
    les aretes (u,v) et (v,u), mais il ne pourra pas avoir deux fois (u,v).
    Dans la premiere phase, on écrit dans tab[k] le degré sortant du sommet k.
    Dans une seconde phase, on crée les aretes en les ecrivant dans le fichier txt.
    Ce programme est bien moins couteux que le précedent.
'''
def create_gtxt(nb_node,nb_arr,max_weight=15,name="random_graph.txt"):
    with open(name,mode='w', encoding='utf-8-sig') as file:
        file.write(str(nb_node)+"\n")
        tab=[0]*nb_node
        for k in range(nb_arr):
            tab[rd(0,nb_node-1)]+=1
        for k in range(nb_node): #Pour chaque noeud
            prec=[] #Ce faisant, on s'assure de ne pas créer 2 fois la meme arete.
            for i in range(tab[k]): #tab[k] est le nombre d'arete qui parte de k qu'on doit ecrire
                arr=rd(0,nb_node-1)
                while arr==k or (arr in prec):
                    arr=rd(0,nb_node-1)
                prec.append(arr)
                file.write(str(k)+";"+str(arr)+";"+str(rd(1,max_weight))+"\n")
        
#create_gtxt(12000, 100000,max_weight=100)
