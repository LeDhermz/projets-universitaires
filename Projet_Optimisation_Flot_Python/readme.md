PROJET OPTIMISATION DE FLOT DANS UN GRAPHE 2021
===============================================

**Implementation de Ford-Fulkerson**

Ce projet propose une implémentation de l'algorithme d'optimisation de flot de Ford-Fulkerson en Python, et étudie sa complexité.
On passera par une programmation orientée Objet pour passer  à l'échelle facilement.

Deux grandes parties dans le projet :

1. Implémentation de l'algorithme
Essentiellement le contenu de Graph.py et algo.py , qui expliquent comment on s'y prend pour modéliser des graphes de grandes echelles

2. Etude de la complexite de l'algorithme 
Tout le travail est fait dans projet_main, en utilisant une génération aléatoire de graphes !