# -*- coding: utf-8 -*-
"""
Created on Sat Dec 18 09:14:33 2021

@author: antoi

Fichier contenant les algorithmes implémentant directement Ford-Fulkerson

/!\ On aura transformé les graphes en amont pour qu'ils ne possedent pas à la fois
(u,v) et (v,u) /!\
    
"""

import Graph
import stack_queue_with_linked_list as Sq
import Compteur


def printres(stack):
    print("On a atteint le puit")
    test=stack.top
    while test:
        print(test.data.index)
        test=test.next
        
def parcours_en_profondeur(g):
    stack=Sq.Stack()
    stack.push(g.source)
    g.source.visited=True
    while not stack.is_empty() :
        current=stack.top
        frame_edge=current.data.adj.head
        while frame_edge:
            if frame_edge.data.w > 0 and not(frame_edge.data.head.visited):
                stack.push(frame_edge.data.head)
                frame_edge.data.head.visited=True
                if frame_edge.data.head.index == g.puit.index:
                    #printres(stack) #sert pour voir le chemin améliorant trouvé
                    return True,stack
                frame_edge=frame_edge.data.head.adj.head
            else :
                frame_edge=frame_edge.next
        stack.pop()
    return False,Sq.Stack()

def ford_fulkerson(g):
    res=parcours_en_profondeur(g)
    flow_max=0
    while res[0]: #Tant qu'on a un chemin améliorant
        path_flow = float("Inf")
        frame_node=res[1].top
        while frame_node.next:
            e=g.get_edge(frame_node.next.data,frame_node.data)
            path_flow=min(path_flow,e.w)
            frame_node=frame_node.next
        flow_max+=path_flow
        frame_node=res[1].top
        while frame_node.next:
            g.get_edge(frame_node.data,frame_node.next.data).w += path_flow
            g.get_edge(frame_node.next.data,frame_node.data).w -= path_flow
            frame_node=frame_node.next
        g.set_non_visited()
        #g.print_adj_list() #Sert pour voir le graphe des résidus actualisé
        res=parcours_en_profondeur(g)
    return flow_max
        
####################Evaluer la complexité################

#En dessous, on ajoute à compt compte le nombre de fois où l'on visite une arête    
    
def parcours_en_profondeur_comp(g,compt):
    stack=Sq.Stack()
    stack.push(g.source)
    g.source.visited=True
    while not stack.is_empty() :
        current=stack.top
        frame_edge=current.data.adj.head
        while frame_edge:
            compt.add(1)
            if frame_edge.data.w > 0 and not(frame_edge.data.head.visited):
                stack.push(frame_edge.data.head)
                frame_edge.data.head.visited=True
                if frame_edge.data.head.index == g.puit.index:
                    #printres(stack) #sert pour voir le chemin améliorant trouvé
                    return True,stack
                frame_edge=frame_edge.data.head.adj.head
            else :
                frame_edge=frame_edge.next
        stack.pop()
    return False, Sq.Stack()

def ford_fulkerson_comp(g):
    compteur=Compteur.Compteur()
    res=parcours_en_profondeur_comp(g,compteur)
    flow_max=0
    while res[0]: #Tant qu'on a un chemin améliorant
        path_flow = float("Inf")
        frame_node=res[1].top
        while frame_node.next:
            e=g.get_edge(frame_node.next.data,frame_node.data)
            path_flow=min(path_flow,e.w)
            frame_node=frame_node.next
        flow_max+=path_flow
        frame_node=res[1].top
        while frame_node.next:
            g.get_edge(frame_node.data,frame_node.next.data).w += path_flow
            g.get_edge(frame_node.next.data,frame_node.data).w -= path_flow
            frame_node=frame_node.next
        g.set_non_visited()
        #g.print_adj_list()
        res=parcours_en_profondeur_comp(g,compteur)
    return flow_max,compteur.get_val()      
        
            
            


