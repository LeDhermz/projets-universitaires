# -*- coding: utf-8 -*-
#import sys #Utile lorsqu'on souhaite appeler directement le script en console

import Graph
import algo
from math import floor
from math import sqrt
from random import randint as rd
import matplotlib.pyplot as plt

############## Trouver le flow max ###############

#Depuis une donnée de graphe en .txt
def maxflowtxt(gtxt):
    return algo.ford_fulkerson(Graph.txtToGrp(gtxt))
#print(maxflowtxt("exemple3_graphe.txt"))

#Depuis une instance de Graphe 
def maxflowg(g):
    return algo.ford_fulkerson(g)


print(maxflowtxt("random_graph.txt"))

############## Evaluer la complexité ###############

'''
    Nos evaluations prennent 
'''

#Depuis une instance de graphe
#On renvoie le flow max et la valeur du compteur de complexité
def maxflowcomp(g):
    return algo.ford_fulkerson_comp(g)

#Depuis une donnée de graphe en .txt
#On renvoie le flow max et la valeur du compteur de complexité
def maxflowcomptxt(gtxt):
    return algo.ford_fulkerson_comp(Graph.txtToGrp(gtxt))

'''
    Créé un graphe de nb_arr arètes de poids au plus max_weight. Le nombre de noeuds
    est tiré aléatoirement entre le min pour avoir un graphe complet, et le max pour
    avoir une densité de 2/(nb_arr-1). On renvoit ensuite son flot max et le nombre 
    d'arêtes parcourues lors de nos parcours en profondeur. '
'''
def comp_empirique(nb_arr,max_weight):
    nb_noeud=rd(floor(sqrt(2*nb_arr))+2,nb_arr) # à la plus petite valeur, on a un graphe complet
    g=Graph.create_graph(nb_noeud, nb_arr,max_weight)
    #g.print_adj_list()
    f_max,comp=maxflowcomp(g)
    return nb_arr,f_max,comp

'''
    Pour chaque nb d'arete entre 10 et 10+n, on garde la complexité moyenne de
    notre algo sur 10 graphes de nb_arr de poids max = poids.
    On affiche ensuite chacune de ces valeurs moyennes.
    On constate bien un relation linéaire, mais il y a triche :
        Le flow max augmente aussi avec le nombre d'aretes, car bien que le poids
        de chaque arête soit borné, le puits peut recevoir de plus en plus d'aretes.
        Ainsi, rien ne nous confirme que la relation linéaire provienne bien de 
        l'augmentation du nombre d'arete, et pas seulement de l'augmentation du flot max.
    Pour faire mieux, il aurait fallu générer des graphes à flot max constants ou bornés,
    ou bien plus simplement faire une regression multivariée avec librairie adaptées ou en R.    
'''
def complexite_var_arete(n,poids):
    tab=[0]*n
    for k in range(n):
        for i in range(30):
            tab[k]+=comp_empirique(10+k,poids)[2]
        tab[k]=tab[k]*0.1
    plt.plot(range(10,n+10),tab)
    plt.title("Complexité en fonction du nombre d'arêtes")
    plt.xlabel("Nombre d'arêtes")
    plt.ylabel("Compléxité")
    plt.show()
'''
    Ici c'est plus simple : On repete comp_empirique(nb_arr, poids) n fois.
    On affiche ensuite les différents cout engendré par la recherche de chaque valeur
    de flot max. Comme le nombre d'arete est ici VRAIMENT fixé, on peut VRAIEMENT
    conclure à une complexité linéaire par rapport au flow max.
'''
def complexite_var_maxf(n,nb_arr,poids):
    X=[0]*n
    Y=[0]*n
    for k in range(n):
        res=comp_empirique(nb_arr, poids)
        X[k]=res[1]
        Y[k]=res[2]
    plt.plot(X,Y,"2")
    plt.xlim(0, max(X))
    plt.ylim(0, max(Y))
    plt.title("Complexité en fonction du flow max")
    plt.xlabel("Flow Max")
    plt.ylabel("Compléxité")
    plt.show()
    
#complexite_var_maxf(1000, 100, 50)
#complexite_var_arete(200, 10)