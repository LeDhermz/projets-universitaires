# -*- coding: utf-8 -*-

################################  Linked_list  ################################## 

class Frame:
    '''
    attributes :
    - data :  
    - next : next Frame or None
    - previous: preivous frame or None if first / Finalement ça n'a pas été utile'
    
    Dependencies : None
    '''
    def __init__(self,x):
        self.data=x
        self.next=None
        self.previous=None
        
class Linked_list:
    '''
    class for a linked list
    
    attributes :
    - head : Frame or None
    
    methods :
    - is_empty() : return true is the list is empty, false otherwise
    - append(x) : add the data x at the head 
    - pop() : if the linked list is not empty, pop the head, else do nothing and return None
    - print() : print the list
    - suppr(data): Si elle existe, on supprime la data de la liste chainée. Finalement, on
        a pas eu à s'en servir. 
    Dependencies : class Frame
    '''
    def __init__(self):
        self.head=None

    def is_empty(self):
        return self.head is None
    
    def suppr(self,dat):
        frame=self.head
        if frame.data==dat :
           if frame.next is not None :
              frame.next.previous=None
           self.head=frame.next
           return
        else:
            frame=frame.next
            while frame:
                if frame.data==dat :
                    #Fonctionne SEULEMENT SI la classe data implémente une méthode __eq__
                    #Sans méthode __eq__, on compare juste les adresses de data et dat
                    #ce qui aurait fonctionné dans notre cas
                    self.head=frame.next
                    frame.previous.next=frame.next
                    if frame.next is not None :
                        frame.next.previous=frame.previous
                    return
                frame=frame.next
                    
    
    def append(self,x):
        new_head = Frame(x)
        new_head.next = self.head
        self.head = new_head
        if self.head.next is not None:
            self.head.next.previous=self.head

    def pop(self):
        if self.head is None:
            return None
        x=self.head.data
        self.head=self.head.next
        return x

    def print(self):  # fonction d'affichage de la liste
        print('Liste chainees= ',end='')
        current=self.head
        while current:
            print(current.data,end=' ')
            current=current.next
        print()    
################################  Tests  ################################## 