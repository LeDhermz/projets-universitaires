# -*- coding: utf-8 -*-
"""
 * @version 1.0
 * @author Bertrand Gentou <bertrand.gentou@u-paris.fr>
 *
 * Correction de l'exercice 3.b du TP 3
 *
 * classes Stack et Queue avec listes chainées
 *
 * @requires : none
"""

################################  Frame  ################################# 

class Frame:
    '''
        class used only by Queue class
        attributes :
        - data :  
        - next : next Frame or None
        - prev : previous Frame or None
    '''
    def __init__(self,x):
        self.data=x
        self.next=None
        self.prev=None



################################  Stack  ################################## 

        
class Stack:
    '''
        class for a stack
        attributes :
        - top : Frame or None
        - size : size of stack
        methods :
        - is_empty() : return true is the stack is empty, false otherwise
        - push(x) : add the data x at the top 
        - pop() : if the stack is not empty, pop the top, else do nothing and return None
        - print() : print the list
    '''
    def __init__(self):
        self.size=0
        self.top=None

    def is_empty(self):
        return self.top is None
        
    def push(self,x):
        new_top = Frame(x)
        new_top.next = self.top
        self.top = new_top
        self.size+=1

    def pop(self):
        if self.top is None:
            return None
        x=self.top.data
        self.top=self.top.next
        self.size-=1
        return x

    def print(self):  # fonction d'affichage de la pile
        print('Pile= ',end='')
        current=self.top
        while current:
            print(current.data,end=' ')
            current=current.next
        print()    

################################  Queue  ################################## 


class Queue:
    '''
        class for Queue
        attributes :
        - head : Frame or None
        - tail : Frame or None
        - size : size of queue
        methods :
        - is_empty() : return true is the queue is empty, false otherwise
        - enqueue(x) : enqueue the data x  
        - dequeue() : if the queue is not empty, dequeue the head, else do nothing and return None
        - print() : print the queue
    '''
    def __init__(self):
        self.size=0
        self.head=None
        self.tail=None
        
    def is_empty(self):
        return self.head is None
    
    def enqueue(self,x):
        new_tail=Frame(x)
        # écriture du chainage aller : 2 cas
        new_tail.next=None
        if self.tail is None:
        # si la file est vide, self.head doit pointer sur new_tail
            self.head= new_tail
        else:
        # Sinon, self.tail.next est valide et doit pointer sur new_tail
            self.tail.next=new_tail
        # écriture du chainage retour : 1 cas
        new_tail.prev=self.tail 
        self.tail=new_tail
        self.size+=1
 

    def dequeue(self):
        #si la liste est vide, on ne fait rien
        if self.head is None:
            return None    
        # sinon sauvegarde de l'élément défiler
        x=self.head.data
        # écriture du chainage aller : 1 cas
        self.head=self.head.next 
        # écriture du chainage retour : 2 cas
        if self.head is None:
        # si la liste devient vide
            self.tail=None
        # sinon self.head.prev est valide
        else:
            self.head.prev=None
        self.size-=1
        return x
        
    def print(self):  # fonction d'affichage de la file
        print('File= ',end='')
        current=self.head
        while current:
            print(current.data,end=' ')
            current=current.next
        print()    

################################  Tests  ################################## 